//
//  ButtonCollectionViewCell.h
//  Gigster
//
//  Created by Rakitha Perera on 8/9/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Explore.h"

@class ButtonCell;
@protocol ButtonCellDelegate<NSObject>

- (void)ButtonCell:(ButtonCell *)cell
     didTapOnButton:(Explore *)item;

@end

@interface ButtonCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (nonatomic, weak) id <ButtonCellDelegate> delegate;
@property (strong, nonatomic) Explore *item;

- (void)configCellWithData:(Explore *)item;
@end
