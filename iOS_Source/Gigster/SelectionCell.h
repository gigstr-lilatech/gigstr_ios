//
//  SelectionCell.h
//  Gigster
//
//  Created by Rakitha Perera on 12/22/16.
//  Copyright © 2016 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *selectionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *selectionImageView;

@end
