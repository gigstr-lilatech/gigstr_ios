//
//  CarouselItem.m
//  Gigster
//
//  Created by Rakitha Perera on 8/8/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import "CarouselItem.h"

@implementation CarouselItem

-(id)initWithDic:(NSDictionary *)data
{
    self = [super init];
    if (self) {
        self.carousel_id = data[@"id"];
        self.item_description = data[@"description"];
        self.image = data[@"image"];
        self.title = data[@"title"];
        self.action = data[@"action"];
        self.url = data[@"url"];
        
    }
    return self;
}

@end
