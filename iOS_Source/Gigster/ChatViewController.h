//
//  ChatViewController.h
//  Gigster
//
//  Created by EFutures on 12/13/15.
//  Copyright © 2015 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatDTO.h"
#import "BaseViewController.h"
@interface ChatViewController : BaseViewController

@property(nonatomic,strong)ChatDTO *chatDTO;
@property(nonatomic,strong)NSString *userImageURL;

@end
