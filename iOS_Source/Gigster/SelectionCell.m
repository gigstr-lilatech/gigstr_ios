//
//  SelectionCell.m
//  Gigster
//
//  Created by Rakitha Perera on 12/22/16.
//  Copyright © 2016 EFutures. All rights reserved.
//

#import "SelectionCell.h"

@implementation SelectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
