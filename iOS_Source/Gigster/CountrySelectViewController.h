//
//  CountrySelectViewController.h
//  Gigster
//
//  Created by Rakitha Perera on 12/22/16.
//  Copyright © 2016 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CountrySelectViewController;
@protocol CountrySelectViewControllerDelegate<NSObject>

- (void)CountrySelectViewController:(CountrySelectViewController *)countrySelectViewController
           didSelecectCountry:(NSDictionary *)country;

@end

@interface CountrySelectViewController : UIViewController

@property (nonatomic, weak) id <CountrySelectViewControllerDelegate> delegate;

@end
