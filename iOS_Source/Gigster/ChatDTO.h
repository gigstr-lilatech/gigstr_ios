//
//  ChatDTO.h
//  Gigster
//
//  Created by EFutures on 12/10/15.
//  Copyright © 2015 EFutures. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ChatDTO : NSObject 

@property NSString *chatID;
@property NSString *gigID;
@property NSString *createdByID;
@property NSString *messageID;
@property NSString *message;
@property NSString *imageURL;
@property UIImage *chatImage;
@property NSString *title;
@property NSString *createdTime;
@property NSString *modified;
@property NSString *unreadCount;
@property (nonatomic) NSString *unreadMessage;
@property NSString *userImageURL;
@property UIImage *userImage;

+ (void) getChatLiostforUser:(NSString *)userID token:(NSString *)token completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion;

+ (void) getChatMessageForUser:(NSString *)userID token:(NSString *)token chatID:(NSString *)chatID completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion;

+ (void) sendMessage:(NSString *)userID token:(NSString *)token chatID:(NSString *)chatID message:(NSString *)message completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion;

+ (void) getUnreadMessages:(NSString *)userID token:(NSString *)token chatID:(NSString *)chatID completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion;


@end
