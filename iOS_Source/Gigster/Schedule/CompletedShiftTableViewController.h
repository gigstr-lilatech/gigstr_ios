//
//  CompletedShiftTableViewController.h
//  Gigster
//
//  Created by Rakitha Perera on 4/24/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Schedule.h"

@interface CompletedShiftTableViewController : UITableViewController

@property (strong, nonatomic) Schedule *schedule;

- (void)setupShiftDetails:(Schedule *)shift;

@end
