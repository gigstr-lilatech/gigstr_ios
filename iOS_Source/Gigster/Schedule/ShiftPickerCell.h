//
//  ShiftPickerCell.h
//  Gigster
//
//  Created by Rakitha Perera on 7/15/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShiftPickerCell : UIView
    @property (weak, nonatomic) IBOutlet UIButton *doneButton;
    @property (weak, nonatomic) IBOutlet UIPickerView *picker;

@end
