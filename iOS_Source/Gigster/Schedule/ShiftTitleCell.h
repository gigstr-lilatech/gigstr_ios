//
//  ShiftTitleCell.h
//  Gigster
//
//  Created by Rakitha Perera on 7/16/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShiftTitleCell : UITableViewCell
    
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
    @property (weak, nonatomic) IBOutlet NSLayoutConstraint *trailingConstraint;
    @property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingConstraint;
    
@end
