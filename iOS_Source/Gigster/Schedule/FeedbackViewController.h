//
//  FeedbackViewController.h
//  Gigster
//
//  Created by Rakitha Perera on 4/17/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Schedule.h"

@class FeedbackViewController;
@protocol FeedbackViewControllerDelegate<NSObject>

- (void)FeedbackViewController:(FeedbackViewController *)feedbackViewController
                 didDismissWithSuccess:(BOOL)success;

@end

@interface FeedbackViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) Schedule *schedule;
@property (nonatomic, weak) id <FeedbackViewControllerDelegate> delegate;

@end
