//
//  ReportViewController.h
//  Gigster
//
//  Created by Rakitha Perera on 7/25/16.
//  Copyright © 2016 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Schedule.h"

@class ReportViewController;
@protocol ReportViewControllerDelegate<NSObject>

- (void)ReportViewController:(ReportViewController *)reportViewController
         didDismissWithSuccess:(BOOL)success;

@end

@interface ReportViewController : UIViewController
@property (strong, nonatomic) Schedule *schedule;
@property (nonatomic, weak) id <ReportViewControllerDelegate> delegate;

@end
