//
//  ReportViewController.m
//  Gigster
//
//  Created by Rakitha Perera on 7/25/16.
//  Copyright © 2016 EFutures. All rights reserved.
//

#import "ReportViewController.h"
#import "ReportTableViewController.h"
#import "LocationManager.h"

@interface ReportViewController ()

@end

@implementation ReportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //    [self.navigationController.navigationBar setHidden:YES];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    UIImage* image = [UIImage imageNamed:@"close"];
    CGRect frame = CGRectMake(0, 0, 100, 50);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frame];
    [backButton setImage:image forState:UIControlStateNormal];
    [backButton setTitle:@"         " forState:UIControlStateNormal];
    CGFloat insetAmount = 4 / 2.0;
    backButton.imageEdgeInsets = UIEdgeInsetsMake(0, -insetAmount, 0, insetAmount);
    backButton.titleEdgeInsets = UIEdgeInsetsMake(0, insetAmount, 0, -insetAmount);
    backButton.contentEdgeInsets = UIEdgeInsetsMake(0, insetAmount - 50 , 0, insetAmount);
    [backButton addTarget:self action:@selector(closeReportView:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem* backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backButtonItem;
    self.navigationItem.title = NSLocalizedString(@"CHECK OUT", nil);
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationItem.title = NSLocalizedString(@"CHECK OUT", nil);
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
//                                                  forBarMetrics:UIBarMetricsDefault];
//    self.navigationController.navigationBar.shadowImage = [UIImage new];
//    self.navigationController.navigationBar.translucent = YES;
//    self.navigationController.view.backgroundColor = [UIColor clearColor];
//    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

}

-(void)viewWillDisappear:(BOOL)animated {

    [super viewWillDisappear:animated];
//    self.navigationController.navigationBar.userInteractionEnabled = YES;
}

- (IBAction)closeReportView:(id)sender {
    [[LocationManager sharedInstance] stopUpdatingLocation];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ReportVCSegue"]) {
        ReportTableViewController *embed = segue.destinationViewController;
        embed.schedule = self.schedule;
        embed.parentVC = self;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
