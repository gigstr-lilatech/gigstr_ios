//
//  ShiftCheckboxCell.h
//  Gigster
//
//  Created by Rakitha Perera on 7/16/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShiftData.h"

@class ShiftCheckboxCell;
@protocol ShiftCheckboxCellDelegate<NSObject>

- (void)shiftCheckboxCell:(ShiftCheckboxCell *)cell changeStatus:(BOOL)status;

@end

@interface ShiftCheckboxCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *fieldTitle;
@property (weak, nonatomic) IBOutlet UISwitch *fieldSwitch;
@property (nonatomic, strong) ShiftData *shiftData;
@property (nonatomic, strong) ShiftData *parentShiftData;
@property (nonatomic, weak) id <ShiftCheckboxCellDelegate> delegate;
    @property (weak, nonatomic) IBOutlet NSLayoutConstraint *trailingConstraint;
    @property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingConstraint;
    
@end
