//
//  ScheduleListCell.h
//  Gigster
//
//  Created by Rakitha Perera on 7/5/16.
//  Copyright © 2016 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DJWStarRatingView.h"
#import "Schedule.h"

@interface ScheduleListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *workingHoursLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;
@property (weak, nonatomic) IBOutlet UIImageView *statusImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *separatorHeight;
@property (weak, nonatomic) IBOutlet UIView *ratingContentView;
@property (nonatomic,strong) DJWStarRatingView *ratingView;

- (void)prepareForScheduleState:(int)state;
- (void)prepareForScheduleDate:(NSDate *)date andState:(int)state;
- (void)prepareForScheduleRating:(Schedule *)schedule;

@end
