//
//  EditShiftDataTableViewController.h
//  Gigster
//
//  Created by Rakitha Perera on 5/29/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Schedule.h"
#import "EditShiftDataViewController.h"

@interface EditShiftDataTableViewController : UITableViewController

@property (strong, nonatomic) NSMutableArray *shiftDataArr;
@property (strong, nonatomic) EditShiftDataViewController *parentVC;
- (BOOL)isValidatedAllFields;
@end
