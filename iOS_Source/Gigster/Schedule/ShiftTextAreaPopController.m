//
//  ShiftTextAreaPopController.m
//  Gigster
//
//  Created by Rakitha Perera on 7/28/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import "ShiftTextAreaPopController.h"
#import "Utility.h"
#import "IQKeyboardManager.h"

@interface ShiftTextAreaPopController ()<UITextViewDelegate>
//@property (nonatomic, strong) XCDFormInputAccessoryView *inputAccessoryView;
@property(nonatomic,weak)IBOutlet UITextView *txtPitch;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *botomConstraint;

@property (nonatomic, assign) CGRect oldRect;
@property (nonatomic, strong) NSTimer *caretVisibilityTimer;
@end

@implementation ShiftTextAreaPopController

@synthesize pitchDelegate;

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.inputAccessoryView = [XCDFormInputAccessoryView new];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    [self.navigationController.navigationBar setBackgroundImage:[Utility imageFromColor:[UIColor colorWithRed:131.0/255.0 green:213.0/255.0 blue:210.0/255.0 alpha:1.0]]
                                                  forBarMetrics:UIBarMetricsDefault];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardHide)
                                                 name:@"HideKeyboard" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_keyboardWillShowNotification:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    if ([_pitchText isEqualToString:@""]) {
        
        _txtPitch.textColor = [UIColor lightGrayColor];
        _txtPitch.textAlignment = NSTextAlignmentLeft;
        _txtPitch.text = _placeholder;
        [_txtPitch resignFirstResponder];
    }else{
        
        _txtPitch.textColor = [UIColor darkGrayColor];
        _txtPitch.textAlignment = NSTextAlignmentLeft;
        _txtPitch.text      = _pitchText;
    }
    
    self.txtPitch.delegate = self;
    
    [self.saveButton setHidden:self.viewModeOnly];
    [self.txtPitch setEditable:!self.viewModeOnly];
    self.txtPitch.userInteractionEnabled = !self.viewModeOnly;
    self.navigationItem.hidesBackButton = !self.viewModeOnly;
    [_txtPitch becomeFirstResponder];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [[IQKeyboardManager sharedManager] setEnable:NO];
}

-(void)viewWillDisappear:(BOOL)animated {
    [[IQKeyboardManager sharedManager] setEnable:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)addPitchText:(id)sender{
    
    NSString *textString = @"";
    if (![self.txtPitch.text isEqualToString:_placeholder]) {
        textString = _txtPitch.text;
    }
    
    [pitchDelegate setPitchText:textString];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    return YES;
}


- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:_placeholder]) {
        _txtPitch.textAlignment = NSTextAlignmentLeft;
        _txtPitch.text = @"";
        _txtPitch.textColor = [UIColor darkGrayColor];
        return YES;
    }
    return YES;
}

-(void)textViewDidChange:(UITextView *)textView
{
    
    if(_txtPitch.text.length == 0){
        
        _txtPitch.textAlignment = NSTextAlignmentLeft;
        _txtPitch.textColor = [UIColor lightGrayColor];
        _txtPitch.text = _placeholder;
         [_txtPitch resignFirstResponder];
    }
}

-(void)didTapOnTextView{
    
    if(_txtPitch.text.length == 0){
        _txtPitch.textColor = [UIColor colorWithRed:204.0f/255.0f green:204.0f/255.0f blue:204.0f/255.0f alpha:1.0];
        _txtPitch.text = _placeholder;
        [_txtPitch resignFirstResponder];
    }else {
        
        [_txtPitch resignFirstResponder];
    }
    
}

-(void)keyboardHide{
    
    if(_txtPitch.text.length == 0){
        _txtPitch.textColor = [UIColor colorWithRed:204.0f/255.0f green:204.0f/255.0f blue:204.0f/255.0f alpha:1.0];
        _txtPitch.text = _placeholder;
        [_txtPitch resignFirstResponder];
    }
    
}


- (void)_keyboardWillShowNotification:(NSNotification*)notification
{
//    UIEdgeInsets insets = self.txtPitch.contentInset;
//    insets.bottom += [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
//    self.txtPitch.contentInset = insets;
//    
//    insets = self.txtPitch.scrollIndicatorInsets;
//    insets.bottom += [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
//    self.txtPitch.scrollIndicatorInsets = insets;
    
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    self.botomConstraint.constant = keyboardFrameBeginRect.size.height + 25;
}

- (void)_keyboardWillHideNotification:(NSNotification*)notification
{
//    UIEdgeInsets insets = self.txtPitch.contentInset;
//    insets.bottom -= [notification.userInfo[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
//    self.txtPitch.contentInset = insets;
//    
//    insets = self.txtPitch.scrollIndicatorInsets;
//    insets.bottom -= [notification.userInfo[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
//    self.txtPitch.scrollIndicatorInsets = insets;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    _oldRect = [self.txtPitch caretRectForPosition:self.txtPitch.selectedTextRange.end];
    
    _caretVisibilityTimer = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(_scrollCaretToVisible) userInfo:nil repeats:YES];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [_caretVisibilityTimer invalidate];
    _caretVisibilityTimer = nil;
}

- (void)_scrollCaretToVisible
{
    //This is where the cursor is at.
    CGRect caretRect = [self.txtPitch caretRectForPosition:self.txtPitch.selectedTextRange.end];
    
    if(CGRectEqualToRect(caretRect, _oldRect))
        return;
    
    _oldRect = caretRect;
    
    //This is the visible rect of the textview.
    CGRect visibleRect = self.txtPitch.bounds;
    visibleRect.size.height -= (self.txtPitch.contentInset.top + self.txtPitch.contentInset.bottom);
    visibleRect.origin.y = self.txtPitch.contentOffset.y;
    
    //We will scroll only if the caret falls outside of the visible rect.
    if(!CGRectContainsRect(visibleRect, caretRect))
    {
        CGPoint newOffset = self.txtPitch.contentOffset;
        
        newOffset.y = MAX((caretRect.origin.y + caretRect.size.height) - visibleRect.size.height + 5, 0);
        
        [self.txtPitch setContentOffset:newOffset animated:YES];
    }
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
