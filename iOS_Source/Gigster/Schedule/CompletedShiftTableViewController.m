//
//  CompletedShiftTableViewController.m
//  Gigster
//
//  Created by Rakitha Perera on 4/24/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import "CompletedShiftTableViewController.h"
#import "ZSWTappableLabel.h"
#import "DJWStarRatingView.h"
#import "NSDateFormatter+Addtions.h"
#import "Utility.h"
#import "UIImage+StackBlur.h"
#import "EnterPitchViewController.h"
#import "WebViewController.h"
#import "UIImageView+AFNetworking.h"
#import "WebViewController.h"
#import "ShiftDataCell.h"
#import "ShiftData.h"
#import "EditShiftDataViewController.h"
#import "ShiftTitleCell.h"
#import "ShiftCheckboxCell.h"

@interface CompletedShiftTableViewController () <UIWebViewDelegate, UpdatePichTextProtocol, ShiftDataCellDelegate>


@property (weak, nonatomic) IBOutlet UIImageView *dayReportImageView;
@property (weak, nonatomic) IBOutlet UILabel *shiftNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *shiftTimeLabel;
@property (weak, nonatomic) IBOutlet UITableViewCell *adminChatCell;
@property (weak, nonatomic) IBOutlet ZSWTappableLabel *adminFeedbackLabel;
@property (weak, nonatomic) IBOutlet UIView *adminRatingContentView;
@property (weak, nonatomic) IBOutlet UILabel *adminNameLabel;
@property (nonatomic,strong) DJWStarRatingView *adminRatingView;
@property (nonatomic,strong) DJWStarRatingView *gigstrRatingView;
@property (weak, nonatomic) IBOutlet UIView *feedbackContentView;

@property (weak, nonatomic) IBOutlet UIImageView *gigstrImageView;
@property (weak, nonatomic) IBOutlet UIView *gigstrRatingContentView;
@property (weak, nonatomic) IBOutlet ZSWTappableLabel *gigstrFeedbackLabel;
@property (weak, nonatomic) IBOutlet UILabel *gigstrName;
@property (weak, nonatomic) IBOutlet UIView *gigstrFeedbackContentView;
@property (weak, nonatomic) IBOutlet UITableViewCell *gigstrChatCell;
    @property (weak, nonatomic) IBOutlet UIActivityIndicatorView *webViewLoading;


@property (weak, nonatomic) IBOutlet ZSWTappableLabel *shiftCommentLabel;
@property (strong, nonatomic) IBOutlet UIWebView *descriptionWebView;
@property (nonatomic) CGFloat webViewHeight;
@property (weak, nonatomic) IBOutlet UIButton *shifDataEditButton;
@property (weak, nonatomic) IBOutlet UILabel *gigImageCountLabel;

@end

@implementation CompletedShiftTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.webViewHeight = 100;
//    self.descriptionWebView.scalesPageToFit = YES;
    [self.webViewLoading stopAnimating];
    [self setupFeedbackView];
    [self setupRatingView];
    [self setupShiftDetails:self.schedule];
    [self.tableView registerNib:[UINib nibWithNibName:@"ShiftDataCell" bundle:nil] forCellReuseIdentifier:@"ShiftDataCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ShiftTitleCell" bundle:nil] forCellReuseIdentifier:@"ShiftTitleCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ShiftCheckboxCell" bundle:nil] forCellReuseIdentifier:@"ShiftCheckboxCell"];
}

- (void)setupShiftDetails:(Schedule *)shift {

    self.shiftNameLabel.text = shift.name;
    
    if (self.schedule.gigstr_start != nil) {
        NSDateComponents *components = [[NSCalendar currentCalendar]
                                        components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitTimeZone
                                        fromDate:self.schedule.gigstr_start];
        NSTimeZone *tempTimeZone = [components timeZone];
        
        if ([tempTimeZone isDaylightSavingTimeForDate:self.schedule.gigstr_start]) {
            [NSDateFormatter dafaultDateFormatter].timeZone = [NSTimeZone timeZoneForSecondsFromGMT:7200];
        } else {
            [NSDateFormatter dafaultDateFormatter].timeZone = [NSTimeZone timeZoneForSecondsFromGMT:3600];
        }
        
        [[NSDateFormatter dafaultDateFormatter] setDateFormat:@"d MMM"];
        NSString *day = [[[NSDateFormatter dafaultDateFormatter] stringFromDate:self.schedule.gigstr_start].uppercaseString stringByReplacingOccurrencesOfString:@"." withString:@""];;
        
        [[NSDateFormatter dafaultDateFormatter] setDateFormat:@"HH:mm"];
        NSString *startTime = [[NSDateFormatter dafaultDateFormatter] stringFromDate:self.schedule.gigstr_start];
        NSString *endTime = [[NSDateFormatter dafaultDateFormatter] stringFromDate:self.schedule.gigstr_stop];
        self.shiftTimeLabel.text = [NSString stringWithFormat:@"%@ %@-%@",day,startTime,endTime];
    }
    
    
    //Rating
    
    //Admin Rating
    if (self.schedule.internal_rating == 0) {
        self.adminChatCell.hidden = YES;
    } else {
        self.adminRatingView.rating = self.schedule.internal_rating;
        self.adminChatCell.hidden = NO;
        self.adminFeedbackLabel.text = self.schedule.internal_rating_comment;
        self.adminNameLabel.text = self.schedule.internal_user;
    }
    
    //Gigstr Rating
    if (self.schedule.gigstr_rating == 0) {
        self.gigstrChatCell.hidden = YES;
    } else {
        self.gigstrRatingView.rating = self.schedule.gigstr_rating;
        self.gigstrChatCell.hidden = NO;
        self.gigstrFeedbackLabel.text = self.schedule.gigstr_rating_comment;
        self.gigstrName.text = self.schedule.gigstr_name;
    }
    
    [self.gigstrImageView setImageWithURL:[NSURL URLWithString:self.schedule.gigstr_image] placeholderImage:[UIImage imageNamed:@"user_profile"]];
    
    self.shiftCommentLabel.text = self.schedule.comment;
    [self.tableView reloadData];
    [self.descriptionWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.schedule.details]]];
    
    
    //Shift Data
    [self.shifDataEditButton setHidden:!self.schedule.shift_data_editable];
    if (self.schedule.schedule_image_count > 0 && self.schedule.schedule_image_count == 1) {
        self.gigImageCountLabel.text = [NSString stringWithFormat:@"%lu %@",(unsigned long)self.schedule.schedule_image_count, NSLocalizedString(@"Image uploaded", @"")];
    } else if (self.schedule.schedule_image_count > 0) {
        self.gigImageCountLabel.text = [NSString stringWithFormat:@"%lu %@",(unsigned long)self.schedule.schedule_image_count, NSLocalizedString(@"Images uploaded", @"")];
    } else {
        self.gigImageCountLabel.text = NSLocalizedString(@"No images uploaded", @"");
    }
}

- (void)setupRatingView {
    self.adminRatingView = [[DJWStarRatingView alloc] initWithStarSize:CGSizeMake(25, 25) numberOfStars:5 rating:2.0 fillColor:[UIColor colorWithRed:1.00 green:0.81 blue:0.34 alpha:1.0] unfilledColor:[UIColor colorWithRed:0.87 green:0.87 blue:0.87 alpha:0.6]strokeColor:[UIColor colorWithRed:0.65 green:0.65 blue:0.65 alpha:1.0]];
    self.adminRatingView.lineWidth = 0.2;
    [self.adminRatingContentView addSubview:self.adminRatingView];
    self.adminRatingView.editable = NO;
    self.adminRatingView.padding = 1;
    self.adminRatingView.rating = 0;
    self.adminRatingView.allowsHalfIntegralRatings = NO;
    self.adminRatingView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    self.adminRatingView.hidden = NO;
    
    self.gigstrRatingView = [[DJWStarRatingView alloc] initWithStarSize:CGSizeMake(25, 25) numberOfStars:5 rating:2.0 fillColor:[UIColor colorWithRed:1.00 green:0.81 blue:0.34 alpha:1.0] unfilledColor:[UIColor colorWithRed:0.87 green:0.87 blue:0.87 alpha:0.6]strokeColor:[UIColor colorWithRed:0.65 green:0.65 blue:0.65 alpha:1.0]];
    self.gigstrRatingView.lineWidth = 0.2;
    [self.gigstrRatingContentView addSubview:self.gigstrRatingView];
    self.gigstrRatingView.editable = NO;
    self.gigstrRatingView.padding = 1;
    self.gigstrRatingView.rating = 0;
    self.gigstrRatingView.allowsHalfIntegralRatings = NO;
    self.gigstrRatingView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    self.gigstrRatingView.hidden = NO;
}

-(void)viewDidLayoutSubviews {
//    CGRect frame = _descriptionWebView.frame;
//    frame.size.height = 1;
//    _descriptionWebView.frame = frame;
//    CGSize fittingSize = [_descriptionWebView sizeThatFits:CGSizeZero];
//    frame.size = fittingSize;
//    _descriptionWebView.frame = frame;
//    
//    if (self.webViewHeight != fittingSize.height) {
//        self.webViewHeight = fittingSize.height;
//    }
}

- (void)setupFeedbackView {
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 240;
    
    self.shiftTimeLabel.text = @"";
    
    self.dayReportImageView.clipsToBounds = YES;
    self.dayReportImageView.image = [self.dayReportImageView.image stackBlur:10];
    self.dayReportImageView.image = [self darkenImage:self.dayReportImageView.image toLevel:0.2f];

    [self.descriptionWebView setBackgroundColor:[UIColor clearColor]];
    [self.descriptionWebView setOpaque:NO];
    self.descriptionWebView.scrollView.showsHorizontalScrollIndicator = NO;
    self.descriptionWebView.scrollView.showsVerticalScrollIndicator = NO;
    self.descriptionWebView.delegate = self;
    self.descriptionWebView.scrollView.scrollEnabled = NO;
    self.descriptionWebView.scrollView.bounces = NO;
//    self.webViewHeight = 0.0f;

    [[self.feedbackContentView layer] setCornerRadius:5.0f];
    [[self.feedbackContentView layer] setMasksToBounds:YES];
    
    [[self.gigstrFeedbackContentView layer] setCornerRadius:5.0f];
    [[self.gigstrFeedbackContentView layer] setMasksToBounds:YES];
    
    self.gigstrImageView.layer.cornerRadius = 42.0 / 2;
    self.gigstrImageView.clipsToBounds = YES;
}

- (UIImage *)darkenImage:(UIImage *)image toLevel:(CGFloat)level
{
    // Create a temporary view to act as a darkening layer
    CGRect frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
    UIView *tempView = [[UIView alloc] initWithFrame:frame];
    tempView.backgroundColor = [UIColor blackColor];
    tempView.alpha = level;
    
    // Draw the image into a new graphics context
    UIGraphicsBeginImageContext(frame.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [image drawInRect:frame];
    
    // Flip the context vertically so we can draw the dark layer via a mask that
    // aligns with the image's alpha pixels (Quartz uses flipped coordinates)
    CGContextTranslateCTM(context, 0, frame.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextClipToMask(context, frame, image.CGImage);
    [tempView.layer renderInContext:context];
    
    // Produce a new image from this context
    CGImageRef imageRef = CGBitmapContextCreateImage(context);
    UIImage *toReturn = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    UIGraphicsEndImageContext();
    return toReturn;
}
    
- (void)webViewDidStartLoad:(UIWebView *)webView {
    webView.hidden = YES;
    [self.webViewLoading startAnimating];
        
}
    
- (void)webViewDidFinishLoad:(UIWebView *)webView
    {
        CGFloat contentHeightReal = webView.scrollView.contentSize.height;
        CGFloat contentHeight = [[webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight"] floatValue];
        self.webViewHeight = contentHeight;
        NSLog(@"Real value : %f", contentHeightReal);
        self.descriptionWebView.hidden = NO;
        [self.webViewLoading stopAnimating];
        [self.tableView reloadData];

//   [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(updateWeb) userInfo:nil repeats:NO];
}

-(void)updateWeb
{
    CGRect frame = self.descriptionWebView.frame;
    frame.size.height = 1;
    _descriptionWebView.frame = frame;
    [self.descriptionWebView layoutSubviews];
    CGSize fittingSize = [self.descriptionWebView sizeThatFits:CGSizeZero];
    frame.size = fittingSize;
//    if (fittingSize.height > self.webViewHeight) {
        _descriptionWebView.frame = frame;
        
        self.webViewHeight = fittingSize.height; // [result floatValue]; // fittingSize.height;
        [self.tableView reloadData];
        self.descriptionWebView.hidden = NO;
    [self.webViewLoading stopAnimating];
//    } else {
//        
//    }
    
    
//    CGRect newBounds = _descriptionWebView.bounds;
//    newBounds.size.height = _descriptionWebView.scrollView.contentSize.height;
//    self.webViewHeight = _descriptionWebView.scrollView.contentSize.height;
//    _descriptionWebView.bounds = newBounds;
//    [self.tableView reloadData];
}

//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//    CGRect frame = _descriptionWebView.frame;
//    frame.size.height = 1;
//    _descriptionWebView.frame = frame;
//    CGSize fittingSize = [_descriptionWebView sizeThatFits:CGSizeZero];
//    frame.size = fittingSize;
//    _descriptionWebView.frame = frame;
//    
//    if (self.webViewHeight != fittingSize.height) {
//        self.webViewHeight = fittingSize.height;
//        [tableView reloadData];
//    }
//}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        NSLog(@"link clicked = %@",request.mainDocumentURL);
        
        WebViewController *detailController   = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];;
        detailController.webURL               = request.mainDocumentURL;
        [self.navigationController pushViewController:detailController animated:YES];
        return NO;
    }
    return YES;
}


- (IBAction)editCommentTapped:(id)sender {
     [self performSegueWithIdentifier:@"segueEnterComment" sender:self];
}
- (IBAction)fullDescriptionTapped:(id)sender {
    WebViewController *detailController   = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
    detailController.webURL               = [NSURL URLWithString:self.schedule.details];
    detailController.isBoundsAdded = YES;
    [self.navigationController pushViewController:detailController animated:YES];

    
}

- (IBAction)editShiftDataTapped:(id)sender {
    [self performSegueWithIdentifier:@"showEditShiftDataSG" sender:self];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 2) {
        if (self.schedule.shift_data.count > 0) {
            return self.schedule.shift_data.count + 1;
        } else {
            return 0;
        }
        
    } else {
        return [super tableView:tableView numberOfRowsInSection:section];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (indexPath.section == 2 && indexPath.row != 0) {
        ShiftData *data = [self.schedule.shift_data objectAtIndex:(indexPath.row - 1)];
        if ([data.field_type isEqualToString:ShiftData_TITLE]) {
            
            ShiftTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ShiftTitleCell"];
            cell.titleLbl.text = data.field_name;
            cell.titleLbl.textColor = [UIColor colorWithRed:131.0/255.0 green:213.0/255.0 blue:210.0/255.0 alpha:1.0];
            cell.trailingConstraint.constant = 14.0f;
            cell.leadingConstraint.constant = 14.0f;
            return cell;
        } else if ([data.field_type isEqualToString:ShiftData_CHECKBOX]) {
            ShiftCheckboxCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ShiftCheckboxCell"];
            cell.fieldTitle.text = data.field_name;
            cell.shiftData = data;
            cell.fieldSwitch.on = [data.field_value boolValue];
            cell.fieldSwitch.enabled = NO;
            cell.trailingConstraint.constant = 14.0f;
            cell.leadingConstraint.constant = 14.0f;
            cell.fieldTitle.textColor = [UIColor blackColor];

            return cell;
        } else {
            
            ShiftDataCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ShiftDataCell"];
            cell.shiftData = data;
            cell.floatingLabel.placeholder = data.field_name;
            cell.floatingLabel.title = data.field_name;
            cell.floatingLabel.selectedTitle = data.field_name;
            cell.floatingLabel.text = data.field_value;
            if ([data.field_type isEqualToString:ShiftData_DROPDOWN]) {
                if ([data.field_type isEqualToString:ShiftData_DROPDOWN]) {
                    NSArray *filtered = [data.field_data filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(value == %@)", data.field_value]];
                    if (filtered.count > 0) {
                        NSDictionary *dict = [filtered objectAtIndex:0];
                        cell.floatingLabel.text = dict[@"label"];
                    }
                }
            }
            [cell configureType:data.field_type];
            
            cell.floatingLabel.errorMessage = data.errorMessage;
            cell.delegate = self;
            cell.viewModeOnly = YES;
            
            cell.separatorInset = UIEdgeInsetsMake(0.f, cell.bounds.size.width, 0.f, 0.f);
            cell.trailingConstraint.constant = 14.0f;
            cell.leadingConstraint.constant = 14.0f;
            cell.floatingLabel.enabled = NO;
            if (data.field_value.length > 0) {
                if ([data.field_type isEqualToString:ShiftData_DROPDOWN] && [data.field_value isEqualToString:@"UnvfEH9UVI"]) {
                    cell.floatingLabel.textColor = [UIColor colorWithRed: 204.0/255.0 green: 204.0/255.0 blue: 204.0/255.0 alpha: 1.0];
                } else {
                    cell.floatingLabel.textColor = [UIColor blackColor];
                }
                cell.floatingLabel.titleColor = [UIColor colorWithRed:131.0/255.0 green:213.0/255.0 blue:210.0/255.0 alpha:1.0];
                cell.floatingLabel.lineColor = [UIColor colorWithRed:131.0/255.0 green:213.0/255.0 blue:210.0/255.0 alpha:1.0];
            } else {
                cell.floatingLabel.lineColor = [UIColor colorWithRed: 204.0/255.0 green: 204.0/255.0 blue: 204.0/255.0 alpha: 1.0];
                cell.floatingLabel.titleColor = [UIColor colorWithRed: 204.0/255.0 green: 204.0/255.0 blue: 204.0/255.0 alpha: 1.0];
            }
            return cell;
            
        }

    } else {
        UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
        return cell;
    }
    
}

-(NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 0;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1 && indexPath.row == 0 && self.schedule.internal_rating == 0 && self.schedule.gigstr_rating == 0) {
        return 0;
    }
    
    if (indexPath.section == 1 && indexPath.row == 1 && self.schedule.internal_rating == 0) {
        return 0;
    }
    
    if (indexPath.section == 1 && indexPath.row == 2 && self.schedule.gigstr_rating == 0) {
        return 0;
    }
    
    if (indexPath.section == 2 && indexPath.row != 0) {
        ShiftData *data = [self.schedule.shift_data objectAtIndex:(indexPath.row - 1)];
        if ([data.field_type isEqualToString:ShiftData_CHECKBOX]) {
            return 35;
        } else if ([data.field_type isEqualToString:ShiftData_TITLE]) {
            return 26;
        }
        return 48;
    }
    if (indexPath.section == 3 && [self.schedule.comment isEqualToString:@""]) {
        return 0;
    }
    
    if (indexPath.section == 5 && [self.schedule.details isEqualToString:@""]) {
        return 0;
    }
    
    if (indexPath.section == 5 && indexPath.row == 1) {
    
        return self.webViewHeight;
    }
    
    return UITableViewAutomaticDimension;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"segueEnterComment"]){
        
        EnterPitchViewController *detailController   = (EnterPitchViewController *)[segue destinationViewController];
        detailController.pitchDelegate               = self;
        if ([self.schedule.comment isEqualToString:@""])
        {
            detailController.pitchText                   = NSLocalizedString(@"Eg I beat my sales record today", nil);
            
        }else{
            detailController.pitchText                   = self.schedule.comment;
            
        }
        detailController.placeholder = NSLocalizedString(@"Eg I beat my sales record today", nil);
        detailController.viewTitle = NSLocalizedString(@"ENTER YOUR COMMENT", nil);
        
    } else if ([[segue identifier] isEqualToString:@"showEditShiftDataSG"]) {
        EditShiftDataViewController *detailController   = (EditShiftDataViewController *)[segue destinationViewController];
        detailController.shiftDataArr               = [NSMutableArray arrayWithArray:self.schedule.shift_data];
        detailController.schedule = self.schedule;
    }
    
    
}

-(void)setPitchText:(NSString *)textPitch{
    
    if ([textPitch isEqualToString:NSLocalizedString(@"Eg I beat my sales record today", nil)]) {
    
        return;
    }
    
    NSUserDefaults *defaults    = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedArray = [defaults objectForKey:@"userObject"];
    if (dataRepresentingSavedArray != nil)
    {
        User *user =  (User *)[NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
        self.shiftCommentLabel.text = textPitch;
        [Schedule editWorkLogSchedule:self.schedule.scheduleId token:user.token userID:user.userid comment:textPitch completion:^(BOOL success, NSDictionary *data, NSError *error) {
            if (error) {
                [self showAlert:[error localizedDescription]];
                return;
            }
            
            int status = [data[@"status"] intValue];
            if (status != 0) {
                return;
            }
            self.schedule.comment = textPitch;
            self.shiftCommentLabel.text = self.schedule.comment;
            [self.tableView reloadData];

        }];
    }

}

- (void)textFieldReturn:(SkyFloatingLabelTextField *)textField toNextIndexPath:(NSIndexPath *)indexPath {

}

- (void)pitchTextReturn:(SkyFloatingLabelTextField *)textField text:(NSString *)text indexPath:(NSIndexPath *)indexPath {
    
}

- (UIViewController *)getParentViewController {
    return self;
}

-(void)showAlert:(NSString *)message{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                         }];
    [alert addAction:ok];
    
}
@end
