//
//  ShiftPickerCell.m
//  Gigster
//
//  Created by Rakitha Perera on 7/15/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import "ShiftPickerCell.h"

@implementation ShiftPickerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (IBAction)doneTapped:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"HideKeyboard" object:nil];
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}
    
- (void) dealloc
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
    
    @end
