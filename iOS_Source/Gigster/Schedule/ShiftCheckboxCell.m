//
//  ShiftCheckboxCell.m
//  Gigster
//
//  Created by Rakitha Perera on 7/16/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import "ShiftCheckboxCell.h"

@implementation ShiftCheckboxCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)switchChanged:(UISwitch *)sender {
    self.fieldTitle.textColor = [UIColor blackColor];
    self.shiftData.field_value = sender.on ? @"1" : @"0";
    int current = [self.parentShiftData.field_value intValue];
    if (sender.on) {
        current ++;
    } else {
        current --;
    }
    self.parentShiftData.field_value = @(current).stringValue;
    self.parentShiftData.errorMessage = @"";
    self.shiftData.errorMessage = @"";
    [self.delegate shiftCheckboxCell:self changeStatus:sender.on];
}

@end
