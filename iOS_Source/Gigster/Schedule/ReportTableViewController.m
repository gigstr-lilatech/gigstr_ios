//
//  ReportTableViewController.m
//  Gigster
//
//  Created by Rakitha Perera on 7/25/16.
//  Copyright © 2016 EFutures. All rights reserved.
//

#import "ReportTableViewController.h"
#import "NSDateFormatter+Addtions.h"
#import "EnterPitchViewController.h"
#import "User.h"
#import "MBProgressHUD.h"
#import "LocationManager.h"
#import "FeedbackViewController.h"
#import "QBImagePickerController.h"
#import "AFNetworking.h"
#import "HTTPClient.h"
#import "UIImage+Resize.h"
#import "ReportViewHeaderView.h"
#import <SkyFloatingLabelTextField/SkyFloatingLabelTextField-Swift.h>
#import "ShiftDataCell.h"
#import "ShiftTitleCell.h"
#import "ShiftCheckboxCell.h"

typedef enum : NSUInteger {
    startPicker,
    endPicker,
    none,
} PickerOpen;

@interface ReportTableViewController ()<UpdatePichTextProtocol, FeedbackViewControllerDelegate, QBImagePickerControllerDelegate, ShiftDataCellDelegate, ShiftCheckboxCellDelegate>
@property (weak, nonatomic) IBOutlet UITableViewCell *startCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *startPickerCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *endPickerCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *endCell;

@property (weak, nonatomic) IBOutlet UILabel *startDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *endDateLabel;

@property (atomic, assign) PickerOpen pickerOpen;
@property (strong, nonatomic) QBImagePickerController *imagePickerController;

@property (weak, nonatomic) IBOutlet UIDatePicker *startDatePicker;
@property (weak, nonatomic) IBOutlet UIDatePicker *endDatePicker;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet UITableViewCell *okCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *imageUploadCell;
@property (weak, nonatomic) IBOutlet UILabel *imageNumberLabel;
@property (strong, nonatomic) NSMutableArray *uploadImageArray;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
    
@property (strong, nonatomic) IBOutlet UIView *viewPickerBackground;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerView;

@end

@implementation ReportTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.pickerOpen = none;
    self.uploadImageArray = NSMutableArray.new;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [[NSDateFormatter dafaultDateFormatter] setDateFormat:@"dd MMM yyyy HH:mm"];

    NSDateComponents *components = [[NSCalendar currentCalendar]
                                    components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitTimeZone
                                    fromDate:self.schedule.date];
    NSTimeZone *tempTimeZone = [components timeZone];
    
    if ([tempTimeZone isDaylightSavingTimeForDate:self.schedule.date]) {
        [NSDateFormatter dafaultDateFormatter].timeZone = [NSTimeZone timeZoneForSecondsFromGMT:7200];
    } else {
        [NSDateFormatter dafaultDateFormatter].timeZone = [NSTimeZone timeZoneForSecondsFromGMT:3600];
    }
    self.startDateLabel.text = [[NSDateFormatter dafaultDateFormatter] stringFromDate:self.schedule.startTime];
    self.endDateLabel.text = [[NSDateFormatter dafaultDateFormatter] stringFromDate:self.schedule.endTime];
    
    self.startDatePicker.date = self.schedule.startTime;
    self.endDatePicker.date = self.schedule.endTime;
    
    self.commentButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.commentButton.titleLabel.numberOfLines = 2;
    [self setupImagePicker];
    [self.tableView registerNib:[UINib nibWithNibName:@"ReportViewHeaderView" bundle:nil] forCellReuseIdentifier:@"ReportViewHeaderView"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ShiftDataCell" bundle:nil] forCellReuseIdentifier:@"ShiftDataCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ShiftTitleCell" bundle:nil] forCellReuseIdentifier:@"ShiftTitleCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ShiftCheckboxCell" bundle:nil] forCellReuseIdentifier:@"ShiftCheckboxCell"];
}

- (void)setupImagePicker {
    self.imagePickerController = [QBImagePickerController new];
    self.imagePickerController.delegate = self;
    self.imagePickerController.mediaType = QBImagePickerMediaTypeAny;
    self.imagePickerController.allowsMultipleSelection = YES;
    self.imagePickerController.showsNumberOfSelectedAssets = YES;
    self.imagePickerController.maximumNumberOfSelection = 10;
    self.imagePickerController.mediaType = QBImagePickerMediaTypeImage;
    self.imageNumberLabel.textColor = [UIColor colorWithRed:0.80 green:0.80 blue:0.80 alpha:1.0];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[LocationManager sharedInstance] startUpdatingLocation];
}

- (void)moveFrameToVerticalPosition:(float)position forDuration:(float)duration {
    CGRect frame = self.view.frame;
    frame.origin.y = position;
    
    [UIView animateWithDuration:duration animations:^{
        self.view.frame = frame;
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
- (UIToolbar *)attachToolBar {
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(buttonPickerDone:)],
                           nil];
    [numberToolbar sizeToFit];
     return numberToolbar;
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 1) {
        return self.schedule.shift_data.count;
    } else {
       return [super tableView:tableView numberOfRowsInSection:section];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (indexPath.section == 1) {
        ShiftData *data = [self.schedule.shift_data objectAtIndex:indexPath.row];
        if ([data.field_type isEqualToString:ShiftData_TITLE]) {
        
            ShiftTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ShiftTitleCell"];
            cell.titleLbl.text = data.field_name;
            if ([data.errorMessage isEqualToString:@""]) {
                cell.titleLbl.textColor = [UIColor colorWithRed:131.0/255.0 green:213.0/255.0 blue:210.0/255.0 alpha:1.0];
            } else {
                cell.titleLbl.textColor = [UIColor redColor];
            }
            return cell;
        } else if ([data.field_type isEqualToString:ShiftData_CHECKBOX]) {
            ShiftCheckboxCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ShiftCheckboxCell"];
            cell.fieldTitle.text = data.field_name;
            cell.shiftData = data;
            cell.fieldSwitch.on = [data.field_value boolValue];
            NSArray *filtered = [self.schedule.shift_data filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(field_id == %@)", data.field_parent]];
            if (filtered.count > 0) {
                cell.parentShiftData = [filtered objectAtIndex:0];
            }
            cell.delegate = self;
            cell.fieldTitle.textColor = [UIColor blackColor];
            return cell;
        } else {
        
            ShiftDataCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ShiftDataCell"];
            cell.shiftData = data;
            cell.floatingLabel.placeholder = data.field_name;
            cell.floatingLabel.title = data.field_name;
            cell.floatingLabel.selectedTitle = data.field_name;
            cell.floatingLabel.text = data.field_value;
            cell.floatingLabel.inputAccessoryView = [self attachToolBar];
            if ([data.field_type isEqualToString:ShiftData_DROPDOWN]) {
                NSArray *filtered = [data.field_data filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(value == %@)", data.field_value]];
                if (filtered.count > 0) {
                    NSDictionary *dict = [filtered objectAtIndex:0];
                    cell.floatingLabel.text = dict[@"label"];
                }
            }
            [cell configureType:data.field_type];
            
            cell.floatingLabel.errorMessage = data.errorMessage;
            
            cell.separatorInset = UIEdgeInsetsMake(0.f, cell.bounds.size.width, 0.f, 0.f);
            if (data.field_value.length > 0) {
                if ([data.field_type isEqualToString:ShiftData_DROPDOWN] && [data.field_value isEqualToString:@"UnvfEH9UVI"]) {
                    cell.floatingLabel.textColor = [UIColor colorWithRed: 204.0/255.0 green: 204.0/255.0 blue: 204.0/255.0 alpha: 1.0];
                } else {
                    cell.floatingLabel.textColor = [UIColor blackColor];
                }
                cell.floatingLabel.titleColor = [UIColor colorWithRed:131.0/255.0 green:213.0/255.0 blue:210.0/255.0 alpha:1.0];
                cell.floatingLabel.lineColor = [UIColor colorWithRed:131.0/255.0 green:213.0/255.0 blue:210.0/255.0 alpha:1.0];
            } else {
                cell.floatingLabel.lineColor = [UIColor colorWithRed: 204.0/255.0 green: 204.0/255.0 blue: 204.0/255.0 alpha: 1.0];
                cell.floatingLabel.titleColor = [UIColor colorWithRed: 204.0/255.0 green: 204.0/255.0 blue: 204.0/255.0 alpha: 1.0];
            }
            cell.currentIndex = indexPath;
            cell.delegate = self;
            return cell;

        }
        } else {
        UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
        cell.preservesSuperviewLayoutMargins = false;
        cell.separatorInset = UIEdgeInsetsZero;
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.separatorInset = UIEdgeInsetsMake(0.f, cell.bounds.size.width, 0.f, 0.f);
        if (cell == _okCell || cell == self.imageUploadCell) {
            
        }
        return cell;
    }
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.separatorInset = UIEdgeInsetsMake(0.f, cell.bounds.size.width, 0.f, 0.f);
    if (indexPath.section == 1 || cell == _okCell || cell == self.imageUploadCell) {
        return;
    }
    if ([cell viewWithTag:22] == nil) //add separator only once
    {
        CGRect sizeRect = [UIScreen mainScreen].applicationFrame;
        NSInteger separatorHeight = 1;
        UIView * separator = [[UIView alloc] initWithFrame:CGRectMake(0,cell.frame.size.height-separatorHeight,sizeRect.size.width,separatorHeight)];
        separator.backgroundColor = [UIColor colorWithRed:131.0/255.0 green:213.0/255.0 blue:210.0/255.0 alpha:1.0];
        separator.tag = 22;
        [cell addSubview:separator];
    }
}

-(NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 0;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    ReportViewHeaderView * customHeaderCell = [tableView dequeueReusableCellWithIdentifier:@"ReportViewHeaderView"];
    if (section == 0) {
        customHeaderCell.headerTitleLabel.text = NSLocalizedString(@"Working Hours", nil);
    } else if (section == 1) {
        customHeaderCell.headerTitleLabel.text = NSLocalizedString(@"Report", nil);
    } else if (section == 2) {
        customHeaderCell.headerTitleLabel.text = NSLocalizedString(@"Day Report", nil);
    }

    
    return customHeaderCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1 && self.schedule.shift_data.count == 0) {
        return 0;
    } else if (section == 2 && self.schedule.shift_data.count > 0) {
        return 0;
    }else if (section == 3) {
        return 0;
    }
    return 46.0f;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // This is the index path of the date picker cell in the static table
    if (indexPath.section == 0 && indexPath.row == 1 && (self.pickerOpen != startPicker)){
        return 0;
    } else if (indexPath.section == 0 && indexPath.row == 3 && (self.pickerOpen != endPicker)) {
        return 0;
    }
    
    if (indexPath.section == 1) {
        ShiftData *data = self.schedule.shift_data[indexPath.row];
        if ([data.field_type isEqualToString:ShiftData_CHECKBOX]) {
            return 35;
        } else if ([data.field_type isEqualToString:ShiftData_TITLE]) {
            return 26;
        }
        return 48;
    }
    
    else if (indexPath.section == 2 && self.schedule.shift_data.count > 0) {
        return 0;
    }
    return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
//    [tableView beginUpdates];
    if (cell == self.startCell){
        self.pickerOpen = self.pickerOpen != startPicker ? startPicker : none;
    } else if (cell == self.endCell) {
        self.pickerOpen = self.pickerOpen != endPicker ? endPicker : none;
    } else {
        self.pickerOpen = none;
    }
//    [tableView reloadData];
//    [self.tableView endUpdates];
    
//    NSIndexSet* reloadSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 0)];
//    [tableView reloadSections:reloadSet withRowAnimation:UITableViewRowAnimationAutomatic];
//    [tableView reloadData];
    
    [UIView transitionWithView: self.tableView
                      duration: 0.35f
                       options: UIViewAnimationOptionTransitionCrossDissolve
                    animations: ^(void)
     {
         [self.tableView reloadData];
     }
                    completion: nil];
}
- (IBAction)startDateChanged:(id)sender {
    self.startDateLabel.text = [[NSDateFormatter dafaultDateFormatter] stringFromDate:self.startDatePicker.date];
    self.startDateLabel.textColor = [UIColor colorWithRed: 50.0/255.0 green: 150.0/255.0 blue: 150.0/255.0 alpha: 1.0];
}

- (IBAction)endDateChanged:(id)sender {
    self.endDateLabel.text = [[NSDateFormatter dafaultDateFormatter] stringFromDate:self.endDatePicker.date];
    self.endDateLabel.textColor = [UIColor colorWithRed: 50.0/255.0 green: 150.0/255.0 blue: 150.0/255.0 alpha: 1.0];
}

- (IBAction)commentTapped:(id)sender {
    [self performSegueWithIdentifier:@"segueCommentEnter" sender:self];
    
}
- (IBAction)uploadImageTapped:(id)sender {
    [self presentViewController:self.imagePickerController animated:YES completion:NULL];
}
    
- (IBAction)submitReport:(id)sender {
    
    if ([self isEndDate:self.endDatePicker.date isSmallerThanStartDate:self.startDatePicker.date]) {
        [self showAlert:NSLocalizedString(@"End time is earlier than start time. Please do provide a valid times", nil)];
        return;
    }
    
    NSString *shiftComment = @"";
    if ([_commentButton.titleLabel.text isEqualToString:NSLocalizedString(@"Here you can report your results...", nil)]) {
        shiftComment = @"";
    } else {
        shiftComment = _commentButton.titleLabel.text;
    }
    
    
    if (![[LocationManager sharedInstance] isLocationAvilableForVC:self]) {
        return;
    }
    
    if (self.schedule.shift_data.count > 0) {
        BOOL isValidate = YES;
        int i = 0;
        for (ShiftData *data in self.schedule.shift_data) {
            if (data.required && [data.field_value isEqualToString:@""]) {
                isValidate = NO;
                if ([data.field_type isEqualToString:ShiftData_CHECKBOX]) {
                    ShiftCheckboxCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:1]];
                    cell.fieldTitle.textColor = [UIColor redColor];
                    data.errorMessage = data.field_name;
                } else if ([data.field_type isEqualToString:ShiftData_TITLE]) {
                
                } else {
                    ShiftDataCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:1]];
                    cell.floatingLabel.errorMessage = data.field_name;
                    data.errorMessage = data.field_name;
                }
                
            }
            
            // CheckBox Validation
            if ([data.field_type isEqualToString:ShiftData_TITLE] && data.required && [data.field_value intValue] <= 0) {
                 isValidate = NO;
                data.errorMessage = data.field_name;
                ShiftTitleCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:1]];
                cell.titleLbl.textColor = [UIColor redColor];
            }
            i ++;
        }
        
        if (!isValidate) {
            return;
        }
    }
    
    NSUserDefaults *defaults    = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedArray = [defaults objectForKey:@"userObject"];
    if (dataRepresentingSavedArray != nil)
    {
        User *user =  (User *)[NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
        NSString *lat = @([[LocationManager sharedInstance] currentLocation].coordinate.latitude).stringValue;
        NSString *longtitude = @([[LocationManager sharedInstance] currentLocation].coordinate.longitude).stringValue;
        
        [MBProgressHUD showHUDAddedTo:self.parentVC.view animated:YES];
        [Schedule checkOUTSchedule:self.schedule.scheduleId user:user.userid token:user.token start:self.startDatePicker.date stop:self.endDatePicker.date comment:shiftComment latitude:lat longtitude:longtitude shifData:self.schedule.shift_data completion:^(BOOL success, NSDictionary *data, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.parentVC.view animated:YES];
            });
            
            if (error) {
                [self showAlert:[error localizedDescription]];
                return;
            }
            
            int status = [data[@"status"] intValue];
            if (status != 0) {
                return;
            }
            
            //Success
            FeedbackViewController *feedbackVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedbackViewController"];
            feedbackVC.schedule = self.schedule;
            feedbackVC.delegate = self;
            [self.navigationController pushViewController:feedbackVC animated:true];
        }];
//        int i = 0;
//        for (PHAsset *asset in self.uploadImageArray) {
//            
//            NSString *imagePathToUpload = [NSString stringWithFormat:@"uploadImage%i", i];
//            UIImage *imageToUpload = [self getUIImage:asset];
//            i ++;
//            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
//            [Schedule uploadImagesForSchedule:self.schedule.scheduleId userID:user.userid imagePath:imagePathToUpload image:imageToUpload progress:^(NSProgress * _Nonnull uploadProgress) {
//                
//            } completion:^(NSURLResponse * _Nullable response, id  _Nullable responseObject, NSError * _Nullable error) {
//                NSLog(@"DONE !!!!");
//            }];
//        }
//    
    
    
    
        for (PHAsset *asset in self.uploadImageArray) {
            
            UIImage *imageToUpload = [self getUIImage:asset];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;

                NSURLSessionUploadTask *uploadTask = [Schedule uploadImagesForSchedule:self.schedule.scheduleId userID:user.userid image:imageToUpload progress:^(NSProgress * _Nonnull uploadProgress) {
                    
                } completion:^(NSURLResponse * _Nullable response, id  _Nullable responseObject, NSError * _Nullable error) {
                    NSLog(@"DONE !!!!");
                }];
        }
    }
    
    
    
}

- (UIImage *)getUIImage:(PHAsset *)asset {
    __block UIImage *img = nil;
    PHImageManager *manager = [PHImageManager defaultManager];
    PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
    options.version = PHImageRequestOptionsVersionOriginal;
    options.synchronous = YES;
//    options.resizeMode = PHImageRequestOptionsResizeModeFast;
//    let maxDimension = UIScreen.mainScreen().bounds.width / 3 * UIScreen.mainScreen().scale
//    CGFloat maxDimension = [[UIScreen mainScreen] bounds].size.width / 3 * [[UIScreen mainScreen] scale];
//    CGSize retinaSquare = CGSizeMake(maxDimension, maxDimension);
//    CGSize maxDimension = CGSizeMake(800, 600);
    
    [manager requestImageDataForAsset:asset options:options resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
        
        if (imageData != nil) {
            int imageSize   = imageData.length;
            NSLog(@"size of image in Mb: %.2f Mb ", (float)imageSize/1024/1024);
            
            img = [UIImage imageWithData:imageData];
            NSData *imgData= UIImageJPEGRepresentation(img,0.025 /*compressionQuality*/);
            
            int imageSize1   = imgData.length;
            NSLog(@"size of image in Mb: %.2f Mb ", (float)imageSize1/1024/1024);
            img = [UIImage imageWithData:imgData];
            
//            UIImage *resized = [img resizedImageWithContentMode:UIViewContentModeScaleAspectFill bounds:CGSizeMake(500, 500) interpolationQuality:kCGInterpolationLow];
//            NSData *imageSize1 = UIImageJPEGRepresentation(resized, 1.0);
//            NSLog(@"size of image in Mb: %.2f Mb ", (float)imageSize1.length/1024/1024);
//            img = resized;
        }
        
    }];
    
//    [manager requestImageForAsset:asset targetSize:maxDimension contentMode:PHImageContentModeAspectFill options:options resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
//        img = result;
//    }];
    return img;
}

- (NSString *)getUIImagePath:(PHAsset *)asset withName:(NSString *)name {
    __block UIImage *img = nil;
    __block NSString *filePath = nil;
    PHImageManager *manager = [PHImageManager defaultManager];
    PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
    options.version = PHImageRequestOptionsVersionOriginal;
    options.synchronous = YES;
    //    options.resizeMode = PHImageRequestOptionsResizeModeFast;
    //    let maxDimension = UIScreen.mainScreen().bounds.width / 3 * UIScreen.mainScreen().scale
    //    CGFloat maxDimension = [[UIScreen mainScreen] bounds].size.width / 3 * [[UIScreen mainScreen] scale];
    //    CGSize retinaSquare = CGSizeMake(maxDimension, maxDimension);
    //    CGSize maxDimension = CGSizeMake(800, 600);
    
    [manager requestImageDataForAsset:asset options:options resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
        
        if (imageData != nil) {
            int imageSize   = imageData.length;
            NSLog(@"size of image in Mb: %.2f Mb ", (float)imageSize/1024/1024);
            
            img = [UIImage imageWithData:imageData];
            NSData *imgData= UIImageJPEGRepresentation(img,0.025 /*compressionQuality*/);
            
            int imageSize1   = imgData.length;
            NSLog(@"size of image in Mb: %.2f Mb ", (float)imageSize1/1024/1024);
            img = [UIImage imageWithData:imgData];
//            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//            NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
            filePath = [NSTemporaryDirectory()  stringByAppendingPathComponent:name]; //Add the file name
            [imgData writeToFile:filePath atomically:YES];
            
            //            UIImage *resized = [img resizedImageWithContentMode:UIViewContentModeScaleAspectFill bounds:CGSizeMake(500, 500) interpolationQuality:kCGInterpolationLow];
            //            NSData *imageSize1 = UIImageJPEGRepresentation(resized, 1.0);
            //            NSLog(@"size of image in Mb: %.2f Mb ", (float)imageSize1.length/1024/1024);
            //            img = resized;
        }
        
    }];
    
    //    [manager requestImageForAsset:asset targetSize:maxDimension contentMode:PHImageContentModeAspectFill options:options resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
    //        img = result;
    //    }];
    return filePath;
}

- (void)FeedbackViewController:(FeedbackViewController *)feedbackViewController
         didDismissWithSuccess:(BOOL)success {
    [self.parentVC.delegate ReportViewController:self.parentVC didDismissWithSuccess:success];
}

-(void)setPitchText:(NSString *)textPitch {
    if ([textPitch isEqualToString:NSLocalizedString(@"Eg I beat my sales record today", nil)]) {
        
        [_commentButton setTitle:NSLocalizedString(@"Here you can report your results...", nil) forState:UIControlStateNormal];
        [_commentButton setTitleColor:[UIColor colorWithRed:204.0f/255.0f green:204.0f/255.0f blue:204.0f/255.0f alpha:1.0] forState:UIControlStateNormal];
        _commentButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        
    }else{
        [_commentButton setTitle:textPitch forState:UIControlStateNormal];
        [_commentButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        _commentButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        
    }

}

- (BOOL)isEndDate:(NSDate *)endDate isSmallerThanStartDate:(NSDate *)startDate
{
    NSTimeInterval distanceBetweenDates = [endDate timeIntervalSinceDate:startDate];
    double secondsInMinute = 60;
    NSInteger secondsBetweenDates = distanceBetweenDates / secondsInMinute;
    
    if (secondsBetweenDates == 0)
        return YES;
    else if (secondsBetweenDates < 0)
        return YES;
    else
        return NO;
}

-(void)showAlert:(NSString *)message{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                         }];
    [alert addAction:ok];
    
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"segueCommentEnter"]){
        
        EnterPitchViewController *detailController   = (EnterPitchViewController *)[segue destinationViewController];
        detailController.pitchDelegate               = self;
        if ([self.commentButton.titleLabel.text isEqualToString:NSLocalizedString(@"Here you can report your results...", nil)])
        {
            detailController.pitchText                   = NSLocalizedString(@"Eg I beat my sales record today", nil);
            
        }else{
            detailController.pitchText                   = self.commentButton.titleLabel.text;
            
        }
        detailController.placeholder = NSLocalizedString(@"Eg I beat my sales record today", nil);
        detailController.viewTitle = NSLocalizedString(@"ENTER YOUR COMMENT", nil);
    }
    
    
}

#pragma mark - QBImagePickerControllerDelegate

- (void)qb_imagePickerController:(QBImagePickerController *)imagePickerController didFinishPickingAssets:(NSArray *)assets
{
    NSLog(@"Selected assets:");
    NSLog(@"%@", assets);
    
    self.uploadImageArray = [NSMutableArray arrayWithArray:assets];
    if (self.uploadImageArray.count > 0 && self.uploadImageArray.count == 1) {
        self.imageNumberLabel.textColor = [UIColor colorWithRed:0.33 green:0.33 blue:0.33 alpha:1.0];
        self.imageNumberLabel.text = [NSString stringWithFormat:@"%lu %@",(unsigned long)self.uploadImageArray.count, NSLocalizedString(@"Image Added ;)", @"")];
    } else if (self.uploadImageArray.count > 0) {
        self.imageNumberLabel.textColor = [UIColor colorWithRed:0.33 green:0.33 blue:0.33 alpha:1.0];
        self.imageNumberLabel.text = [NSString stringWithFormat:@"%lu %@",(unsigned long)self.uploadImageArray.count, NSLocalizedString(@"Images Added ;)", @"")];
    } else {
        self.imageNumberLabel.textColor = [UIColor colorWithRed:0.80 green:0.80 blue:0.80 alpha:1.0];
        self.imageNumberLabel.text = NSLocalizedString(@"Tap to upload images", @"");
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)qb_imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController
{
    NSLog(@"Canceled.");
    self.uploadImageArray = [NSMutableArray new];
    self.imageNumberLabel.textColor = [UIColor colorWithRed:0.80 green:0.80 blue:0.80 alpha:1.0];
    self.imageNumberLabel.text = NSLocalizedString(@"Tap to upload images", @"");
    
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    self.imagePickerController = nil;
    [self setupImagePicker];
}

- (void)qb_imagePickerController:(QBImagePickerController *)imagePickerController didSelectAsset:(PHAsset *)asset {

}

- (void)qb_imagePickerController:(QBImagePickerController *)imagePickerController didDeselectAsset:(PHAsset *)asset {

}


- (void)textFieldReturn:(SkyFloatingLabelTextField *)textField toNextIndexPath:(NSIndexPath *)indexPath {
    ShiftDataCell *cell = (ShiftDataCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    if (cell.floatingLabel == nil) {
        [textField resignFirstResponder];
    } else {
        [cell.floatingLabel becomeFirstResponder];
    }
}

- (void)pitchTextReturn:(SkyFloatingLabelTextField *)textField text:(NSString *)text indexPath:(NSIndexPath *)indexPath {

}

- (UIViewController *)getParentViewController {
    return self;
}

- (void)shiftCheckboxCell:(ShiftCheckboxCell *)cell changeStatus:(BOOL)status {
    
    // clears error message
    NSUInteger index = [self.schedule.shift_data indexOfObject:cell.parentShiftData];
    ShiftTitleCell *titleCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:1]];
    titleCell.titleLbl.textColor = [UIColor colorWithRed:131.0/255.0 green:213.0/255.0 blue:210.0/255.0 alpha:1.0];
    
}

- (IBAction)buttonPickerDone:(id)sender {
    
    [self.view endEditing:YES];
}

@end
