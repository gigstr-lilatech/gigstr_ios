//
//  ReportViewHeaderView.h
//  Gigster
//
//  Created by Rakitha Perera on 5/24/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportViewHeaderView : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

@end
