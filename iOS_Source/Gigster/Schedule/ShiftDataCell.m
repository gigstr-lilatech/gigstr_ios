//
//  ShiftDataCell.m
//  Gigster
//
//  Created by Rakitha Perera on 5/24/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import "ShiftDataCell.h"


@implementation ShiftDataCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.floatingLabel.delegate = self;
    self.floatingLabel.titleFormatter = ^NSString * _Nonnull(NSString * text) {
        return text;
    };
    self.floatingLabel.titleLabel.font = [UIFont systemFontOfSize:10];
    self.separatorInset = UIEdgeInsetsMake(0.f, self.bounds.size.width, 0.f, 0.f);
    
    self.pickerView = [[[NSBundle mainBundle] loadNibNamed:@"ShiftPickerCell" owner:self options:nil] firstObject];
    self.pickerView.picker.delegate = self;
}

-(void)dealloc {

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {

    NSString * fullStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    self.shiftData.field_value = fullStr;
    if (self.shiftData.field_value.length > 0) {
        self.floatingLabel.titleColor = [UIColor colorWithRed:131.0/255.0 green:213.0/255.0 blue:210.0/255.0 alpha:1.0];
        self.floatingLabel.lineColor = [UIColor colorWithRed:131.0/255.0 green:213.0/255.0 blue:210.0/255.0 alpha:1.0];
    } else {
        self.floatingLabel.lineColor = [UIColor colorWithRed: 204.0/255.0 green: 204.0/255.0 blue: 204.0/255.0 alpha: 1.0];
        self.floatingLabel.titleColor = [UIColor colorWithRed: 204.0/255.0 green: 204.0/255.0 blue: 204.0/255.0 alpha: 1.0];
    }
    self.floatingLabel.errorMessage = @"";
    self.shiftData.errorMessage = @"";
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isKindOfClass:[SkyFloatingLabelTextField class]]) {
        [self.delegate textFieldReturn:(SkyFloatingLabelTextField *)textField toNextIndexPath:[NSIndexPath indexPathForRow:self.currentIndex.row + 1 inSection:self.currentIndex.section]];
    }
    
    return YES;
}


- (void)configureType:(NSString *)type {
    [self.textAreaButton setHidden:YES];
    if ([type isEqualToString:ShiftData_STRING]) {
        [self.floatingLabel setKeyboardType:UIKeyboardTypeDefault];
        [self.textAreaButton setHidden:NO];
    } else if ([type isEqualToString:ShiftData_NUMBER]) {
        [self.floatingLabel setKeyboardType:UIKeyboardTypeDecimalPad];
    } else if ([type isEqualToString:ShiftData_DROPDOWN]) {
        // remove cursor
        self.floatingLabel.tintColor = [UIColor clearColor];
        self.floatingLabel.inputAccessoryView = nil;
        self.floatingLabel.inputView = self.pickerView;
        NSArray *filtered = [self.shiftData.field_data filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(value == %@)", self.shiftData.field_value]];
        if (filtered.count > 0) {
            NSDictionary *dict = [filtered objectAtIndex:0];
            NSUInteger index = [self.shiftData.field_data indexOfObject:dict];
            [self.pickerView.picker selectRow:index inComponent:0 animated:NO];
        }
    }
    else {
        [self.floatingLabel setKeyboardType:UIKeyboardTypeDefault];
    }
}
    
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
}
    
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return self.shiftData.field_data.count;
}
    
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{

    NSDictionary *dic = self.shiftData.field_data[row];
    return [dic objectForKeyNotNull:@"label"];
    
}
    
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {

    self.floatingLabel.lineColor = [UIColor colorWithRed:131.0/255.0 green:213.0/255.0 blue:210.0/255.0 alpha:1.0];

    NSDictionary *dic = self.shiftData.field_data[row];
    if ([[dic objectForKeyNotNull:@"value"] isEqualToString:@"UnvfEH9UVI"]) {
        self.floatingLabel.textColor = [UIColor colorWithRed: 204.0/255.0 green: 204.0/255.0 blue: 204.0/255.0 alpha: 1.0];
    } else {
        self.floatingLabel.textColor = [UIColor blackColor];
    }
    self.floatingLabel.titleColor = [UIColor colorWithRed:131.0/255.0 green:213.0/255.0 blue:210.0/255.0 alpha:1.0];
    self.floatingLabel.text = [dic objectForKeyNotNull:@"label"];
    self.shiftData.field_value = [dic objectForKeyNotNull:@"value"];
    self.shiftData.errorMessage = @"";
    self.floatingLabel.errorMessage = @"";
}

- (IBAction)textAreaButtonTapped:(id)sender {
    if (_viewModeOnly && [self.shiftData.field_value isEqualToString:@""]) {
        return;
    }
    ShiftTextAreaPopController *textAreaPop = [[ShiftTextAreaPopController alloc] initWithNibName:@"ShiftTextAreaPopController" bundle:nil];
    textAreaPop.pitchDelegate = self;
    textAreaPop.viewModeOnly = self.viewModeOnly;
    textAreaPop.pitchText = self.shiftData.field_value;
    textAreaPop.placeholder = self.shiftData.field_name;
    [self.delegate.getParentViewController.navigationController pushViewController:textAreaPop animated:YES];
    
}

-(void)setPitchText:(NSString *)textPitch {
    self.shiftData.field_value = textPitch;
    self.floatingLabel.text = textPitch;
    if (self.shiftData.field_value.length > 0) {
        self.floatingLabel.titleColor = [UIColor colorWithRed:131.0/255.0 green:213.0/255.0 blue:210.0/255.0 alpha:1.0];
        self.floatingLabel.lineColor = [UIColor colorWithRed:131.0/255.0 green:213.0/255.0 blue:210.0/255.0 alpha:1.0];
    } else {
        self.floatingLabel.lineColor = [UIColor colorWithRed: 204.0/255.0 green: 204.0/255.0 blue: 204.0/255.0 alpha: 1.0];
        self.floatingLabel.titleColor = [UIColor colorWithRed: 204.0/255.0 green: 204.0/255.0 blue: 204.0/255.0 alpha: 1.0];
    }
    self.floatingLabel.errorMessage = @"";
    self.shiftData.errorMessage = @"";
}



@end
