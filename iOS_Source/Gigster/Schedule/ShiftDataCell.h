//
//  ShiftDataCell.h
//  Gigster
//
//  Created by Rakitha Perera on 5/24/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SkyFloatingLabelTextField/SkyFloatingLabelTextField-Swift.h>
#import "ShiftData.h"
#import "ShiftPickerCell.h"
#import "ShiftTextAreaPopController.h"


@class ShiftDataCell;
@protocol ShiftDataCellDelegate<NSObject>
- (void)pitchTextReturn:(SkyFloatingLabelTextField *)textField text:(NSString *)text indexPath:(NSIndexPath *)indexPath;
- (void)textFieldReturn:(SkyFloatingLabelTextField *)textField toNextIndexPath:(NSIndexPath *)indexPath;
- (UIViewController *)getParentViewController;

@end

@interface ShiftDataCell : UITableViewCell <UITextFieldDelegate, UIPickerViewDelegate, ShiftTextAreaProtocol>
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *floatingLabel;
@property (nonatomic, strong) ShiftData *shiftData;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *trailingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingConstraint;
@property (strong, nonatomic) NSIndexPath *currentIndex;
@property (nonatomic, weak) id <ShiftDataCellDelegate> delegate;
@property (strong, nonatomic) ShiftPickerCell *pickerView;
@property (weak, nonatomic) IBOutlet UIButton *textAreaButton;
@property BOOL viewModeOnly;
- (void)configureType:(NSString *)type;
@end
