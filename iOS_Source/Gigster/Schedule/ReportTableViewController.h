//
//  ReportTableViewController.h
//  Gigster
//
//  Created by Rakitha Perera on 7/25/16.
//  Copyright © 2016 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Schedule.h"
#import "ReportViewController.h"

@interface ReportTableViewController : UITableViewController

@property (strong, nonatomic) Schedule *schedule;
@property (strong, nonatomic) ReportViewController *parentVC;

@end
