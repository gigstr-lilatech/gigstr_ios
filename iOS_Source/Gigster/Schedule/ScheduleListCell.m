//
//  ScheduleListCell.m
//  Gigster
//
//  Created by Rakitha Perera on 7/5/16.
//  Copyright © 2016 EFutures. All rights reserved.
//

#import "ScheduleListCell.h"

@implementation ScheduleListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self setupRatingView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)prepareForScheduleState:(int)state {

    if (state == 1) {
        // Active State
        self.statusImageView.image = nil;
        
    } else if (state == 2) {
        // Checkin State
        self.statusImageView.image = [UIImage imageNamed:@"stop_clock"];
    
    } else if (state == 3) {
        // Checkout/Complete State
        self.statusImageView.image = [UIImage imageNamed:@"check_box"];
    
    } else if (state == 0) {
        // Inactive State
        self.statusImageView.image = nil;
    
    }
}

- (void)prepareForScheduleDate:(NSDate *)date andState:(int)state {
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:[NSDate date]];
    NSDate *today = [cal dateFromComponents:components];
    components = [cal components:(NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:date];
    NSDate *otherDate = [cal dateFromComponents:components];
    
    if([today isEqualToDate:otherDate] && state != 3) {
        //do stuff
        self.dayLabel.font = [self boldFontWithFont:self.dayLabel.font];
        self.dateLabel.font = [self boldFontWithFont:self.dateLabel.font];
        self.workingHoursLabel.font = [self boldFontWithFont:self.workingHoursLabel.font];
        self.nameLabel.font = [self boldFontWithFont:self.nameLabel.font];
        
        self.dayLabel.textColor = [UIColor darkTextColor];
        self.dateLabel.textColor = [UIColor darkTextColor];
        self.workingHoursLabel.textColor = [UIColor darkTextColor];
        self.nameLabel.textColor = [UIColor darkTextColor];
        
    } else if ([today compare:date] == NSOrderedDescending) {
        self.dayLabel.font = [self regularFontWithFont:self.dayLabel.font];
        self.dateLabel.font = [self regularFontWithFont:self.dateLabel.font];
        self.workingHoursLabel.font = [self regularFontWithFont:self.workingHoursLabel.font];
        self.nameLabel.font = [self regularFontWithFont:self.nameLabel.font];
        
        
        self.dayLabel.textColor = [UIColor grayColor];
        self.dateLabel.textColor = [UIColor grayColor];
        self.workingHoursLabel.textColor = [UIColor lightGrayColor];
        self.nameLabel.textColor = [UIColor lightGrayColor];
    
    } else {
        self.dayLabel.font = [self regularFontWithFont:self.dayLabel.font];
        self.dateLabel.font = [self regularFontWithFont:self.dateLabel.font];
        self.workingHoursLabel.font = [self regularFontWithFont:self.workingHoursLabel.font];
        self.nameLabel.font = [self regularFontWithFont:self.nameLabel.font];
        
        self.dayLabel.textColor = [UIColor darkTextColor];
        self.dateLabel.textColor = [UIColor darkTextColor];
        self.workingHoursLabel.textColor = [UIColor darkTextColor];
        self.nameLabel.textColor = [UIColor darkTextColor];
    }
    [self.contentView setNeedsUpdateConstraints];

}

- (void)setupRatingView {
    self.ratingView = [[DJWStarRatingView alloc] initWithStarSize:CGSizeMake(14, 14) numberOfStars:5 rating:2.0 fillColor:[UIColor colorWithRed:1.00 green:0.87 blue:0.34 alpha:1.0] unfilledColor:[UIColor colorWithRed:0.82 green:0.82 blue:0.82 alpha:1.0]strokeColor:[UIColor colorWithRed:0.65 green:0.65 blue:0.65 alpha:1.0]];
    self.ratingView.lineWidth = 0.2;
    [self.ratingContentView addSubview:self.ratingView];
    CGPoint p = [self.ratingContentView.superview convertPoint:self.ratingContentView.center toView:self.ratingContentView];
    
    self.ratingView.center = p;
    self.ratingView.editable = NO;
    self.ratingView.padding = 1;
    self.ratingView.rating = 0;
    self.ratingView.allowsHalfIntegralRatings = NO;
    self.ratingView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    self.ratingView.hidden = NO;
}

- (void)prepareForScheduleRating:(Schedule *)schedule {
    if (schedule.internal_rating == 0) {
        self.ratingView.hidden = YES;
    } else {
        self.ratingView.rating = schedule.internal_rating;
        self.ratingView.hidden = NO;
    }
}

- (UIFont *)boldFontWithFont:(UIFont *)font
{
    UIFontDescriptor * fontD = [font.fontDescriptor
                                fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitBold];
    return [UIFont fontWithDescriptor:fontD size:font.pointSize];
}

- (UIFont *)regularFontWithFont:(UIFont *)font
{
    UIFontDescriptor * fontD = [font.fontDescriptor
                                fontDescriptorWithSymbolicTraits:0];
    return [UIFont fontWithDescriptor:fontD size:font.pointSize];
}

@end
