//
//  EditShiftDataTableViewController.m
//  Gigster
//
//  Created by Rakitha Perera on 5/29/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import "EditShiftDataTableViewController.h"
#import "ShiftDataCell.h"
#import "ShiftData.h"
#import "ShiftTitleCell.h"
#import "ShiftCheckboxCell.h"

@interface EditShiftDataTableViewController () <ShiftDataCellDelegate,ShiftCheckboxCellDelegate>

@end

@implementation EditShiftDataTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerNib:[UINib nibWithNibName:@"ShiftDataCell" bundle:nil] forCellReuseIdentifier:@"ShiftDataCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ShiftTitleCell" bundle:nil] forCellReuseIdentifier:@"ShiftTitleCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ShiftCheckboxCell" bundle:nil] forCellReuseIdentifier:@"ShiftCheckboxCell"];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (UIToolbar *)attachToolBar {
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(buttonPickerDone:)],
                           nil];
    [numberToolbar sizeToFit];
    return numberToolbar;
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.shiftDataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ShiftData *data = [self.shiftDataArr objectAtIndex:indexPath.row];
    if ([data.field_type isEqualToString:ShiftData_TITLE]) {
        
        ShiftTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ShiftTitleCell"];
        cell.titleLbl.text = data.field_name;
        cell.trailingConstraint.constant = 14.0f;
        cell.leadingConstraint.constant = 14.0f;
        if ([data.errorMessage isEqualToString:@""]) {
            cell.titleLbl.textColor = [UIColor colorWithRed:131.0/255.0 green:213.0/255.0 blue:210.0/255.0 alpha:1.0];
        } else {
            cell.titleLbl.textColor = [UIColor redColor];
        }
        return cell;
    } else if ([data.field_type isEqualToString:ShiftData_CHECKBOX]) {
        ShiftCheckboxCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ShiftCheckboxCell"];
        cell.fieldTitle.text = data.field_name;
        cell.shiftData = data;
        cell.fieldSwitch.on = [data.field_value boolValue];
        cell.trailingConstraint.constant = 14.0f;
        cell.leadingConstraint.constant = 14.0f;
        cell.fieldTitle.textColor = [UIColor blackColor];
        NSArray *filtered = [self.shiftDataArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(field_id == %@)", data.field_parent]];
        if (filtered.count > 0) {
            cell.parentShiftData = [filtered objectAtIndex:0];
        }
        cell.delegate = self;

        return cell;
    } else {
        
        ShiftDataCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ShiftDataCell"];
        cell.shiftData = data;
        cell.floatingLabel.placeholder = data.field_name;
        cell.floatingLabel.title = data.field_name;
        cell.floatingLabel.selectedTitle = data.field_name;
        cell.floatingLabel.text = data.field_value;
        cell.floatingLabel.inputAccessoryView = [self attachToolBar];
        if ([data.field_type isEqualToString:ShiftData_DROPDOWN]) {
            NSArray *filtered = [data.field_data filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(value == %@)", data.field_value]];
            if (filtered.count > 0) {
                NSDictionary *dict = [filtered objectAtIndex:0];
                cell.floatingLabel.text = dict[@"label"];
            }
        }
        [cell configureType:data.field_type];
        
        cell.floatingLabel.errorMessage = data.errorMessage;
        
        cell.separatorInset = UIEdgeInsetsMake(0.f, cell.bounds.size.width, 0.f, 0.f);
        cell.trailingConstraint.constant = 14.0f;
        cell.leadingConstraint.constant = 14.0f;
        if (data.field_value.length > 0) {
            if ([data.field_type isEqualToString:ShiftData_DROPDOWN] && [data.field_value isEqualToString:@"UnvfEH9UVI"]) {
                cell.floatingLabel.textColor = [UIColor colorWithRed: 204.0/255.0 green: 204.0/255.0 blue: 204.0/255.0 alpha: 1.0];
            } else {
                cell.floatingLabel.textColor = [UIColor blackColor];
            }
            cell.floatingLabel.titleColor = [UIColor colorWithRed:131.0/255.0 green:213.0/255.0 blue:210.0/255.0 alpha:1.0];
            cell.floatingLabel.lineColor = [UIColor colorWithRed:131.0/255.0 green:213.0/255.0 blue:210.0/255.0 alpha:1.0];

        } else {
            cell.floatingLabel.lineColor = [UIColor colorWithRed: 204.0/255.0 green: 204.0/255.0 blue: 204.0/255.0 alpha: 1.0];
            cell.floatingLabel.titleColor = [UIColor colorWithRed: 204.0/255.0 green: 204.0/255.0 blue: 204.0/255.0 alpha: 1.0];
        }
        cell.currentIndex = indexPath;
        cell.delegate = self;
        return cell;
        
    }
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.separatorInset = UIEdgeInsetsMake(0.f, cell.bounds.size.width, 0.f, 0.f);
}

-(NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 0;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    ShiftData *data = self.shiftDataArr[indexPath.row];
    if ([data.field_type isEqualToString:ShiftData_CHECKBOX]) {
        return 35;
    } else if ([data.field_type isEqualToString:ShiftData_TITLE]) {
        return 18;
    }

    return 48;
}

- (BOOL)isValidatedAllFields {
    BOOL isValidate = YES;
    if (self.shiftDataArr.count > 0) {
        
        int i = 0;
        for (ShiftData *data in self.shiftDataArr) {
            if (data.required && [data.field_value isEqualToString:@""]) {
                if ([data.field_type isEqualToString:ShiftData_CHECKBOX]) {
                    ShiftCheckboxCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
                    cell.fieldTitle.textColor = [UIColor redColor];
                    data.errorMessage = data.field_name;
                    isValidate = NO;
                } else if ([data.field_type isEqualToString:ShiftData_TITLE]) {
                    
                } else {
                    ShiftDataCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
                    cell.floatingLabel.errorMessage = data.field_name;
                    data.errorMessage = data.field_name;
                    isValidate = NO;
                }
            }
            // CheckBox Validation
            if ([data.field_type isEqualToString:ShiftData_TITLE] && data.required && [data.field_value intValue] <= 0) {
                isValidate = NO;
                data.errorMessage = data.field_name;
                ShiftTitleCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
                cell.titleLbl.textColor = [UIColor redColor];
            }
            
            i ++;
        }
    }
    return isValidate;
    
}

- (void)textFieldReturn:(SkyFloatingLabelTextField *)textField toNextIndexPath:(NSIndexPath *)indexPath {
    ShiftDataCell *cell = (ShiftDataCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    if (cell.floatingLabel == nil) {
        [textField resignFirstResponder];
    } else {
        [cell.floatingLabel becomeFirstResponder];
    }
}

- (void)shiftCheckboxCell:(ShiftCheckboxCell *)cell changeStatus:(BOOL)status {
    
    // clears error message
    NSUInteger index = [self.shiftDataArr indexOfObject:cell.parentShiftData];
    ShiftTitleCell *titleCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
    titleCell.titleLbl.textColor = [UIColor colorWithRed:131.0/255.0 green:213.0/255.0 blue:210.0/255.0 alpha:1.0];
    
}


- (void)pitchTextReturn:(SkyFloatingLabelTextField *)textField text:(NSString *)text indexPath:(NSIndexPath *)indexPath {
    
}

- (UIViewController *)getParentViewController {
    return self;
}
    
- (IBAction)buttonPickerDone:(id)sender {
        
        [self.view endEditing:YES];
}

@end
