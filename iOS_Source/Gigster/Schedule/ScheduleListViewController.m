//
//  ScheduleListViewController.m
//  Gigster
//
//  Created by Rakitha Perera on 7/3/16.
//  Copyright © 2016 EFutures. All rights reserved.
//

#import "ScheduleListViewController.h"
#import "ChatViewController.h"
#import "SigninViewController.h"
#import "SignupViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "User.h"
#import "Utility.h"
#import "ScheduleListCell.h"
#import "Schedule.h"
#import "NSDateFormatter+Addtions.h"
#import "ScheduleDetailViewController.h"
#import "LocationManager.h"

@interface ScheduleListViewController () <UITableViewDelegate, UITableViewDataSource>
@property(nonatomic,weak)IBOutlet UIView *loginView;
@property(nonatomic,weak)IBOutlet UIView *loginViewWhite;

@property(nonatomic,weak)IBOutlet UIButton *fbSignup;
@property(nonatomic,weak)IBOutlet UIButton *emailSignup;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic,strong) NSMutableArray *schedules;
@property(nonatomic,strong) UIActivityIndicatorView *indicatorFooter;
@property(nonatomic,strong) UIRefreshControl *indicatorHeader;

@property BOOL isLoadingMoreData;
@property BOOL isScrolledToToday;
@end

@implementation ScheduleListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.schedules = [NSMutableArray new];
    [[_fbSignup layer] setCornerRadius:5.0f];
    [[_fbSignup layer] setMasksToBounds:YES];
    
    [[_emailSignup layer] setCornerRadius:5.0f];
    [[_emailSignup layer] setMasksToBounds:YES];
    
    [[_loginViewWhite layer]setCornerRadius:5.0f];
    _indicatorFooter = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_tableView.frame), 44)];
    [_indicatorFooter setColor:[UIColor blackColor]];
    [_indicatorFooter setHidesWhenStopped:YES];
    [_indicatorFooter stopAnimating];
    [self.tableView setTableFooterView:_indicatorFooter];
    
    self.isScrolledToToday = NO;
    
//    self.indicatorHeader = [[UIRefreshControl alloc]init];
//    [self.tableView addSubview:self.indicatorHeader];
//    self.indicatorHeader.transform = CGAffineTransformMakeScale(0.7, 0.7);
//    [self.indicatorHeader addTarget:self action:@selector(loadOldRecords) forControlEvents:UIControlEventValueChanged];
//
//    _indicatorHeader = [[UIRefreshControl alloc] init];
//    [self.tableView addSubview:_indicatorHeader];
    
    
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [[LocationManager sharedInstance] stopUpdatingLocation];
    
    [self getScheduleForDate:[NSDate date]];
}

-(void)viewDidDisappear:(BOOL)animated {

    [super viewDidDisappear:animated];
    self.schedules = [NSMutableArray new];
    [self.tableView reloadData];
}

- (void)getScheduleForDate:(NSDate *)date {
    NSUserDefaults *defaults    = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedArray = [defaults objectForKey:@"userObject"];
    if (dataRepresentingSavedArray != nil)
    {
        self.loginView.hidden = YES;
        User *user =  (User *)[NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
        // Fetch the schedule data here
        self.isLoadingMoreData = YES;
        [Schedule listSchedules:user.userid token:user.token pageID:@"0" date:[[NSDateFormatter serverDateFormatter] stringFromDate:[NSDate date]] completion:^(BOOL success, NSDictionary *data, NSError *error) {
            self.isLoadingMoreData = NO;
            int status = [data[@"status"] intValue];
            if (status != 0) {
                return;
            }
            NSArray *tempSchedules = data[@"schedule"];
            NSMutableArray *unfilterdSchedules = NSMutableArray.new;
            
            for (NSDictionary *dict in tempSchedules) {
                Schedule *schedule = [[Schedule alloc] init];
                schedule.scheduleId = dict[@"id"];
                schedule.name = dict[@"name"];
                schedule.date = [[NSDateFormatter serverDateFormatter] dateFromString:dict[@"date"]];
                schedule.startTime = [[NSDateFormatter serverDateFormatter] dateFromString:dict[@"start_time"]];
                schedule.endTime = [[NSDateFormatter serverDateFormatter] dateFromString:dict[@"end_time"]];
                schedule.status = [dict[@"status"] intValue];
                schedule.details = dict[@"description"];
                schedule.gigstr_rating = [dict[@"gigstr_rating"] intValue];
                schedule.internal_rating = [dict[@"internal_rating"] intValue];
                schedule.gigstr_rating_comment = dict[@"gigstr_rating_comment"];
                schedule.internal_rating_comment = dict[@"internal_rating_comment"];
                
                [unfilterdSchedules addObject:schedule];
            }
            self.schedules = [NSMutableArray new];
            [self addSchedules:unfilterdSchedules];
            
//            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:YES];
//            [self.schedules sortUsingDescriptors:@[sortDescriptor]];
            
            NSCalendar *cal = [NSCalendar currentCalendar];
            NSDateComponents *components = [cal components:(NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:[NSDate date]];
            NSDate *today = [cal dateFromComponents:components];
            
            
            Schedule *closestSchedule = nil;
            
            if (!self.isScrolledToToday) {
                
                for (Schedule *schedule in self.schedules) {
                    components = [cal components:(NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:schedule.date];
                    NSDate *otherDate = [cal dateFromComponents:components];
                    
                    if(closestSchedule == nil && [today isEqualToDate:otherDate]) {
                        closestSchedule = schedule;
                        break;
                    }
                    
                }
                if (closestSchedule == nil) {
                    NSTimeInterval closestInterval = DBL_MAX;
                    for (Schedule *schedule in self.schedules) {
                        NSTimeInterval interval = fabs([schedule.date timeIntervalSinceDate:[NSDate date]]);
                        if (interval < closestInterval) {
                            closestInterval = interval;
                            closestSchedule = schedule;
                        }
                    }
                }
                self.isScrolledToToday = YES;
            }
            [self.tableView reloadData];
            
            if (closestSchedule != nil) {
                NSUInteger row = [self.schedules indexOfObject:closestSchedule];
                NSIndexPath *scrollIndexPath = [NSIndexPath indexPathForRow:row inSection:0];
                
                [self.tableView scrollToRowAtIndexPath:scrollIndexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
            }
             [self.indicatorHeader removeFromSuperview];
            if (self.schedules.count > 0) {
               
                self.indicatorHeader = nil;
                
            self.indicatorHeader = [[UIRefreshControl alloc]init];
            [self.tableView addSubview:self.indicatorHeader];
            self.indicatorHeader.transform = CGAffineTransformMakeScale(0.7, 0.7);
            [self.indicatorHeader addTarget:self action:@selector(loadOldRecords) forControlEvents:UIControlEventValueChanged];
            }
            
        }];
        
    } else {
        self.loginView.hidden = NO;
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numOfSections = 0;
    if (self.schedules.count > 0)
    {
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        numOfSections                = 1;
        self.tableView.backgroundView = nil;
    }
    else
    {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, self.tableView.bounds.size.height)];
        noDataLabel.text             = NSLocalizedString(@"You can see your work schedule here when you have been assigned a Gig.", nil);
        noDataLabel.textColor        = [UIColor blackColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        noDataLabel.numberOfLines = 3;
        noDataLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
        if (self.loginView.hidden == YES && self.isLoadingMoreData == NO) {
            self.tableView.backgroundView = noDataLabel;
            self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            
        }
        [self.indicatorHeader setEnabled:NO];
    }
    
    return numOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.schedules.count;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    
//    return 88.0f;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"ScheduleListCell";
    
    ScheduleListCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    Schedule *schedule = self.schedules[indexPath.row];
    [[NSDateFormatter dafaultDateFormatter] setDateFormat:@"EEEE"];
    NSDateComponents *components = [[NSCalendar currentCalendar]
                                    components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitTimeZone
                                    fromDate:schedule.date];
    NSTimeZone *tempTimeZone = [components timeZone];
    
    if ([tempTimeZone isDaylightSavingTimeForDate:schedule.date]) {
        [NSDateFormatter dafaultDateFormatter].timeZone = [NSTimeZone timeZoneForSecondsFromGMT:7200];
    } else {
        [NSDateFormatter dafaultDateFormatter].timeZone = [NSTimeZone timeZoneForSecondsFromGMT:3600];
    }
    
    cell.dayLabel.text = [[NSDateFormatter dafaultDateFormatter] stringFromDate:schedule.date].uppercaseString;
    [[NSDateFormatter dafaultDateFormatter] setDateFormat:@"d MMM"];
    cell.dateLabel.text = [[[NSDateFormatter dafaultDateFormatter] stringFromDate:schedule.date].uppercaseString stringByReplacingOccurrencesOfString:@"." withString:@""];
    
    [[NSDateFormatter dafaultDateFormatter] setDateFormat:@"HH:mm"];
    NSString *startTime = [[NSDateFormatter dafaultDateFormatter] stringFromDate:schedule.startTime];
    NSString *endTime = [[NSDateFormatter dafaultDateFormatter] stringFromDate:schedule.endTime];
    cell.workingHoursLabel.text = [NSString stringWithFormat:@"%@-%@",startTime,endTime];
    cell.nameLabel.text = schedule.name;
    
    [cell prepareForScheduleState:schedule.status];
    [cell prepareForScheduleDate:schedule.date andState:schedule.status];
    [cell prepareForScheduleRating:schedule];
    
    cell.preservesSuperviewLayoutMargins = false;
    cell.separatorInset = UIEdgeInsetsZero;
    cell.layoutMargins = UIEdgeInsetsZero;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Schedule *schedule = self.schedules[indexPath.row];
    
    ScheduleDetailViewController *scheduleDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ScheduleDetailViewController"];
    scheduleDetailVC.schedule = schedule;
    
    [self.navigationController pushViewController:scheduleDetailVC animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    float reload_distance = 300;
    CGFloat actualPosition = aScrollView.contentOffset.y;
    CGFloat contentHeight = aScrollView.contentSize.height - (self.tableView.frame.size.height+reload_distance);
    
    if (self.schedules.count == 0) {
        return;
    }
    
    CGFloat yVelocity = [aScrollView.panGestureRecognizer velocityInView:aScrollView].y;
    
    if (yVelocity > 0 && actualPosition <= 300 && !self.isLoadingMoreData) {
//        self.isLoadingMoreData = YES;
//        _indicatorHeader = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_tableView.frame), 44)];
//        [_indicatorHeader setColor:[UIColor blackColor]];
//        [_indicatorHeader setHidesWhenStopped:YES];
//        [_indicatorHeader stopAnimating];
//        [self.tableView setTableHeaderView:_indicatorHeader];
//        [_indicatorHeader startAnimating];z
////         [_indicatorHeader beginRefreshing];
//        NSUserDefaults *defaults    = [NSUserDefaults standardUserDefaults];
//        NSData *dataRepresentingSavedArray = [defaults objectForKey:@"userObject"];
//        if (dataRepresentingSavedArray != nil)
//        {
//            User *user =  (User *)[NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
//            Schedule *lastSchedule = self.schedules.firstObject;
//            
//            [Schedule loadMoreSchedules:user.userid token:user.token pageID:-1 date:[[NSDateFormatter serverDateFormatter] stringFromDate:[NSDate date]] lastScheduleID:lastSchedule.scheduleId completion:^(BOOL success, NSDictionary *data, NSError *error) {
//                [_indicatorHeader stopAnimating];
//                
//                [self.tableView setTableHeaderView:nil];
//                _indicatorHeader = nil;
////                 [_indicatorHeader endRefreshing];
//                int status = [data[@"status"] intValue];
//                if (status != 0) {
//                    self.isLoadingMoreData = NO;
//                    return;
//                }
//                NSArray *tempSchedules = data[@"schedule"];
//                NSMutableArray *tempArr = NSMutableArray.new;
//                for (NSDictionary *dict in tempSchedules) {
//                    Schedule *schedule = [[Schedule alloc] init];
//                    schedule.scheduleId = dict[@"id"];
//                    schedule.name = dict[@"name"];
//                    schedule.date = [[NSDateFormatter serverDateFormatter] dateFromString:dict[@"date"]];
//                    schedule.startTime = [[NSDateFormatter serverDateFormatter] dateFromString:dict[@"start_time"]];
//                    schedule.endTime = [[NSDateFormatter serverDateFormatter] dateFromString:dict[@"end_time"]];
//                    schedule.status = [dict[@"status"] intValue];
//                    schedule.details = dict[@"description"];
//                    
//                    [tempArr addObject:schedule];
//                }
//                NSIndexSet *indexes = [NSIndexSet indexSetWithIndexesInRange:
//                                       NSMakeRange(0,[tempArr count])];
//
//                [self.schedules insertObjects:tempArr atIndexes:indexes];
//                CGSize beforeContentSize = self.tableView.contentSize;
//                [self.tableView reloadData];
//                CGSize afterContentSize = self.tableView.contentSize;
//                CGPoint afterContentOffset = self.tableView.contentOffset;
//                self.tableView.contentOffset = CGPointMake(afterContentOffset.x, afterContentOffset.y + afterContentSize.height - beforeContentSize.height);
//
//                self.isLoadingMoreData = NO;
//            }];
//        }
//
//        NSLog(@"load more rows top");
    } else if (yVelocity < 0 && actualPosition >= contentHeight && !self.isLoadingMoreData) {
//        [self.newsFeedData_ addObjectsFromArray:self.newsFeedData_];
//        [self.tableView reloadData];
        self.isLoadingMoreData = YES;
        [_indicatorFooter startAnimating];
        NSUserDefaults *defaults    = [NSUserDefaults standardUserDefaults];
        NSData *dataRepresentingSavedArray = [defaults objectForKey:@"userObject"];
        if (dataRepresentingSavedArray != nil)
        {
            User *user =  (User *)[NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
            Schedule *lastSchedule = self.schedules.lastObject;
            
           [Schedule loadMoreSchedules:user.userid token:user.token pageID:1 date:[[NSDateFormatter serverDateFormatter] stringFromDate:[NSDate date]] lastScheduleID:lastSchedule.scheduleId completion:^(BOOL success, NSDictionary *data, NSError *error) {
               [_indicatorFooter stopAnimating];
               int status = [data[@"status"] intValue];
               if (status != 0) {
                   self.isLoadingMoreData = NO;
                   return;
               }
               NSArray *tempSchedules = data[@"schedule"];
               NSMutableArray *unfilterdSchedules = NSMutableArray.new;
               for (NSDictionary *dict in tempSchedules) {
                   Schedule *schedule = [[Schedule alloc] init];
                   schedule.scheduleId = dict[@"id"];
                   schedule.name = dict[@"name"];
                   schedule.date = [[NSDateFormatter serverDateFormatter] dateFromString:dict[@"date"]];
                   schedule.startTime = [[NSDateFormatter serverDateFormatter] dateFromString:dict[@"start_time"]];
                   schedule.endTime = [[NSDateFormatter serverDateFormatter] dateFromString:dict[@"end_time"]];
                   schedule.status = [dict[@"status"] intValue];
                   schedule.details = dict[@"description"];
                   schedule.gigstr_rating = [dict[@"gigstr_rating"] intValue];
                   schedule.internal_rating = [dict[@"internal_rating"] intValue];
                   schedule.gigstr_rating_comment = dict[@"gigstr_rating_comment"];
                   schedule.internal_rating_comment = dict[@"internal_rating_comment"];
                   
                   [unfilterdSchedules addObject:schedule];
               }
               [self addSchedules:unfilterdSchedules];
               [self.tableView reloadData];
               self.isLoadingMoreData = NO;
           }];
        }
        
         NSLog(@"load more rows bottom");
    }
}

- (void)loadOldRecords {
    if (self.schedules.count == 0) {
        [self.indicatorHeader endRefreshing];
        return;
    }
    
    self.isLoadingMoreData = YES;

    NSUserDefaults *defaults    = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedArray = [defaults objectForKey:@"userObject"];
    if (dataRepresentingSavedArray != nil)
    {
        User *user =  (User *)[NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
        Schedule *lastSchedule = self.schedules.firstObject;
        
        [Schedule loadMoreSchedules:user.userid token:user.token pageID:-1 date:[[NSDateFormatter serverDateFormatter] stringFromDate:[NSDate date]] lastScheduleID:lastSchedule.scheduleId completion:^(BOOL success, NSDictionary *data, NSError *error) {

            int status = [data[@"status"] intValue];
            if (status != 0) {
                self.isLoadingMoreData = NO;
                [self.indicatorHeader endRefreshing];
                return;
            }
            NSArray *tempSchedules = data[@"schedule"];
            NSMutableArray *tempArr = NSMutableArray.new;
            for (NSDictionary *dict in tempSchedules) {
                Schedule *schedule = [[Schedule alloc] init];
                schedule.scheduleId = dict[@"id"];
                schedule.name = dict[@"name"];
                schedule.date = [[NSDateFormatter serverDateFormatter] dateFromString:dict[@"date"]];
                schedule.startTime = [[NSDateFormatter serverDateFormatter] dateFromString:dict[@"start_time"]];
                schedule.endTime = [[NSDateFormatter serverDateFormatter] dateFromString:dict[@"end_time"]];
                schedule.status = [dict[@"status"] intValue];
                schedule.details = dict[@"description"];
                schedule.gigstr_rating = [dict[@"gigstr_rating"] intValue];
                schedule.internal_rating = [dict[@"internal_rating"] intValue];
                schedule.gigstr_rating_comment = dict[@"gigstr_rating_comment"];
                schedule.internal_rating_comment = dict[@"internal_rating_comment"];
                
                [tempArr addObject:schedule];
            }
            NSIndexSet *indexes = [NSIndexSet indexSetWithIndexesInRange:
                                   NSMakeRange(0,[tempArr count])];
            
            [self.schedules insertObjects:tempArr atIndexes:indexes];
            
            int extraScrollHeight = 0;
            
            if (tempArr.count > 0) {
                extraScrollHeight = 60;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.indicatorHeader endRefreshing];
                CGFloat oldTableViewHeight = self.tableView.contentSize.height + extraScrollHeight;
                [self.tableView reloadData];
                CGFloat newTableViewHeight = self.tableView.contentSize.height;
                [self.tableView setContentOffset:CGPointMake(0, newTableViewHeight - oldTableViewHeight) animated:NO];
            });
            
            self.isLoadingMoreData = NO;
        }];
    }
    
    NSLog(@"load more rows top");
}

-(void)addSchedules:(NSMutableArray *)objects {
    
    for (Schedule *schedule in objects) {
        if ([self scheduleExists:schedule]) {
            [self.schedules enumerateObjectsUsingBlock:^(Schedule *obj, NSUInteger idx, BOOL *stop) {
                if (obj.scheduleId == schedule.scheduleId) {
                    [self.schedules replaceObjectAtIndex:idx withObject:schedule];
                    *stop = YES;
                }
                
            }];
        } else {
            [self.schedules addObject:schedule];
        }
    }
}

- (BOOL)scheduleExists:(Schedule *)schedule {
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"scheduleId==%@",schedule.scheduleId];

    NSArray * filteredarray  = [self.schedules filteredArrayUsingPredicate:predicate];
    
    return filteredarray.count > 0;
}



-(IBAction)fblogingClicked:(id)sender{
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    [login
     logInWithReadPermissions: @[@"public_profile",@"user_birthday",@"email"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error %@",error.description);
         } else if (result.isCancelled) {
             NSLog(@"%@",result);
             [self showAlert:@"Couldn’t connect to Facebook. Please try again."];
         } else {
             NSLog(@"%@",result);
             NSLog(@"Logged in");
             [self getDetailsAndLogin];
         }
     }];
}


- (IBAction)emailSignUpClicked:(id)sender {
    UINavigationController *navigationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpNavigationView"];
    SignupViewController *signupVC   = [navigationVC.viewControllers objectAtIndex:0];
    signupVC.isFromTabbar            = YES;
    [self presentViewController:navigationVC animated:YES completion:nil];

    
}

- (IBAction)signinButtonClicked:(id)sender {
    UINavigationController *navigationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"signinNavigationView"];
    SigninViewController *signinVC   = [navigationVC.viewControllers objectAtIndex:0];
    signinVC.isFromTabbar            = YES;
    [self presentViewController:navigationVC animated:YES completion:nil];
}

- (IBAction)termsAndConditionsClicked:(id)sender {
    UINavigationController *navigationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"termsAndConditionNavigationView"];
    [self presentViewController:navigationVC animated:YES completion:nil];

}

-(void)getDetailsAndLogin{
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:[NSDictionary dictionaryWithObject:@"cover,picture.type(large),id,name,first_name,last_name,gender,birthday,email,location,hometown,bio,photos" forKey:@"fields"]]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         if (!error) {
             
             NSLog(@"%@",result);
             NSString *userID = [[FBSDKAccessToken currentAccessToken] userID];
             NSString *userName = [result objectForKeyNotNull:@"name"];
             NSString *email =  [result objectForKeyNotNull:@"email"];
             NSString *userImageurl = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [[FBSDKAccessToken currentAccessToken] userID]];
             NSString *gender = [[result objectForKeyNotNull:@"gender"] uppercaseString];
             // NSString *DOB = [result objectForKeyNotNull:@"birthday"];
             
             
             NSString *pushToken;
             if ([[NSUserDefaults standardUserDefaults] stringForKey:@"pushDeviceToken"]) {
                 
                 pushToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"pushDeviceToken"];
             }else{
                 
                 pushToken = @"";
             }
             
             NSString *emails = (email) ? email : @"";
             
             //NSString *dobFB = (DOB) ? DOB : @"";
             
             NSString *dateString = [result objectForKeyNotNull:@"birthday"];
             NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
             [dateFormatter setDateFormat:@"MM/dd/yyyy"];
             NSDate *dateFromString = [dateFormatter dateFromString:dateString];
             dateFromString = [dateFormatter dateFromString:dateString];
             
             NSString *date;
             NSString *month;
             NSString *year;
             
             if (dateFromString) {
                 
                 NSDateFormatter *dateF1 = [[NSDateFormatter alloc] init];
                 [dateF1 setDateFormat:@"yyyy"];
                 year = [dateF1 stringFromDate:dateFromString];
                 
                 NSDateFormatter *dateF2 = [[NSDateFormatter alloc] init];
                 [dateF2 setDateFormat:@"MM"];
                 month = [dateF2 stringFromDate:dateFromString];
                 
                 NSDateFormatter *dateF3 = [[NSDateFormatter alloc] init];
                 [dateF3 setDateFormat:@"dd"];
                 date = [dateF3 stringFromDate:dateFromString];
             }
             else{
                 
                 NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                 [dateFormatter setDateFormat:@"MM/dd"];
                 NSDate *dateFromString = [[NSDate alloc] init];
                 dateFromString = [dateFormatter dateFromString:dateString];
                 
                 year = @"";
                 
                 NSDateFormatter *dateF2 = [[NSDateFormatter alloc] init];
                 [dateF2 setDateFormat:@"MM"];
                 month = [dateF2 stringFromDate:dateFromString];
                 
                 NSDateFormatter *dateF3 = [[NSDateFormatter alloc] init];
                 [dateF3 setDateFormat:@"dd"];
                 date = [dateF3 stringFromDate:dateFromString];
                 
             }
             
             NSString *dateX = (date) ? date : @"";
             NSString *monthX = (month) ? month : @"";
             NSString *yearX = (year) ? year : @"";
             
             NSString *genderFB;
             
             if ([gender isEqualToString:@"MALE"]) {
                 genderFB = @"M";
             }else if ([gender isEqualToString:@"FEMALE"]){
                 genderFB = @"F";
             }else{
                 genderFB = @"";
             }
             
             
             NSDictionary *params    = @{@"fbid": userID, @"name": userName, @"email":emails, @"day": dateX, @"month": monthX, @"year": yearX, @"gender": genderFB, @"accountType": [NSNumber numberWithInt:1], @"device_id":pushToken, @"device_type": @"2", @"image" :userImageurl};
             
             //             NSDictionary *params    = @{@"fbid": @"10156311001425287", @"name": @"Rasmus Solholm", @"email":@"solholmrasmus@gmail.com", @"day": dateX, @"month": monthX, @"year": yearX, @"gender": genderFB, @"accountType": [NSNumber numberWithInt:1], @"device_id":pushToken, @"device_type": @"2", @"image" :userImageURL};
             
             [User loginWithFacebookid:params completion:^(BOOL success, NSDictionary *data, NSError *error) {
                 
                 //                 [_activity stopAnimating];
                 //                 [_activity setHidden:YES];
                 
                 [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
                 
                 NSLog(@"success: %d", success);
                 NSLog(@"data: %@", data);
                 NSLog(@"Error: %@", error);
                 
                 if (success) {
                     
                     User *user         = [[User alloc] init];
                     user.userid        = [NSString stringWithFormat:@"%@",[data valueForKey:@"user_id"]];
                     user.token         = [NSString stringWithFormat:@"%@",[data valueForKey:@"token"]];
                     user.email         = email;
                     user.name          = userName;
                     user.userImageURL  = userImageurl;
                     //user.password    = password;
                     
                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                     [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:user] forKey:@"userObject"];
                     [defaults synchronize];
                     
                     [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                     
                     [self viewWillAppear:NO];
                 }
                 
                 else{
                     
                     if ([data valueForKey:@"message"]) {
                         NSString *status = [NSString stringWithFormat:@"%@",[data valueForKey:@"message"]];
                         [self showAlert:status];
                         
                     }else{
                         [self showAlert:NSLocalizedString(@"Could not connect to the server. Please try again", nil)];
                     }
                 }
                 
             }];
         }
         else{
             
             NSLog(@"%@",error.localizedDescription);
             [self showAlert:error.localizedDescription];
         }
     }];
    
}

-(void)showAlert:(NSString *)message{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:NSLocalizedString(message, nil)
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:NSLocalizedString(@"OK", nil)
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             [self dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    [alert addAction:ok];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
