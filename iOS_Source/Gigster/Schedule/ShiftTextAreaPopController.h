//
//  ShiftTextAreaPopController.h
//  Gigster
//
//  Created by Rakitha Perera on 7/28/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@protocol ShiftTextAreaProtocol <NSObject>

-(void)setPitchText:(NSString *)textPitch;

@end

@interface ShiftTextAreaPopController : BaseViewController
@property(nonatomic,strong) NSString *placeholder;
@property(nonatomic,strong)NSString *pitchText;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property(nonatomic,unsafe_unretained) id<ShiftTextAreaProtocol> pitchDelegate;
@property BOOL viewModeOnly;
@end
