//
//  EditShiftDataViewController.m
//  Gigster
//
//  Created by Rakitha Perera on 5/29/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import "EditShiftDataViewController.h"
#import "EditShiftDataTableViewController.h"
#import "User.h"
#import "MBProgressHUD.h"

@interface EditShiftDataViewController ()

@property (strong, nonatomic) EditShiftDataTableViewController *childController;
@end

@implementation EditShiftDataViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated {

}
-(void)dismissKeyboard
{
    [self.view endEditing:YES];

}
- (IBAction)saveTapped:(id)sender {
    
    if ([self.childController isValidatedAllFields]) {
        NSUserDefaults *defaults    = [NSUserDefaults standardUserDefaults];
        NSData *dataRepresentingSavedArray = [defaults objectForKey:@"userObject"];
        if (dataRepresentingSavedArray != nil)
        {
            User *user =  (User *)[NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
            
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [Schedule editShiftDataForSchedule:self.schedule.scheduleId token:user.token userID:user.userid shiftData:self.shiftDataArr completion:^(BOOL success, NSDictionary * _Nullable data, NSError * _Nullable error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                });
                
                if (error) {
                    [self showAlert:[error localizedDescription]];
                    return;
                }
                
                int status = [data[@"status"] intValue];
                if (status != 0) {
                    
                    
                    return;
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:true];
                });
                
            }];
        }
  
    }
    
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"EditShiftDataSegue"]) {
        EditShiftDataTableViewController *embed = segue.destinationViewController;
        embed.shiftDataArr = self.shiftDataArr;
        embed.parentVC = self;
        self.childController = embed;
    }
}

-(void)showAlert:(NSString *)message{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                         }];
    [alert addAction:ok];
    
}

@end
