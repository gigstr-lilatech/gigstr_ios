//
//  ScheduleDetailViewController.m
//  Gigster
//
//  Created by Rakitha Perera on 7/12/16.
//  Copyright © 2016 EFutures. All rights reserved.
//

#import "ScheduleDetailViewController.h"
#import "NSDateFormatter+Addtions.h"
#import "User.h"
#import "ReportViewController.h"
#import "WebViewController.h"
#import "LocationManager.h"
#import "CompletedShiftView.h"
#import "CompletedShiftTableViewController.h"

@interface ScheduleDetailViewController () <UIWebViewDelegate, ReportViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet CompletedShiftView *completedShiftView;
@property (weak, nonatomic) IBOutlet UIView *descriptionView;
@property (strong, nonatomic) CompletedShiftTableViewController *completeSiftTVC;

@end

@implementation ScheduleDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_webView setBackgroundColor:[UIColor clearColor]];
    [_webView setOpaque:NO];
    _webView.scrollView.showsHorizontalScrollIndicator = NO;
    _webView.scrollView.showsVerticalScrollIndicator = NO;
    self.descriptionView.hidden = YES;
    self.completedShiftView.hidden = YES;
//    [self setupViewWithSchedule:self.schedule];
    
    
//    NSString *embedHTML = @"<p>Welcome to this Gig</p>\n<p><span style=\"text-decoration: underline;\"><span style=\"font-weight: bold;\">LOCATION</span></span></p>\n<p>we are&nbsp; meeting at<span style=\"text-decoration: underline;\"><span style=\"font-weight: bold;\"> https://www.google.lk";
    
//    [self.webView loadHTMLString: embedHTML baseURL: nil];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self fetchScheduleDetails];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)fetchScheduleDetails {
    NSUserDefaults *defaults    = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedArray = [defaults objectForKey:@"userObject"];
    if (dataRepresentingSavedArray != nil)
    {
        User *user =  (User *)[NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
        [Schedule getScheduleDetails:self.schedule.scheduleId user:user.userid token:user.token completion:^(BOOL success, NSDictionary *data, NSError *error) {
            
            if (error) {
                [self showAlert:[error localizedDescription]];
                return;
            }

//            data = [Schedule getShceduleDetailsTestData];
            int status = [data[@"status"] intValue];
            if (status != 0) {
                return;
            }
            
            NSDictionary *schedule = data[@"schedule"];
            
            self.schedule.scheduleId = schedule[@"id"];
            self.schedule.name = schedule[@"name"];
            self.schedule.date = [[NSDateFormatter serverDateFormatter] dateFromString:schedule[@"date"]];
            self.schedule.startTime = [[NSDateFormatter serverDateFormatter] dateFromString:schedule[@"start_time"]];
            self.schedule.endTime = [[NSDateFormatter serverDateFormatter] dateFromString:schedule[@"end_time"]];
            self.schedule.status = [schedule[@"status"] intValue];
            self.schedule.gigstr_rating = [schedule[@"gigstr_rating"] intValue];
            self.schedule.internal_rating = [schedule[@"internal_rating"] intValue];
            self.schedule.gigstr_rating_comment = schedule[@"gigstr_rating_comment"];
            self.schedule.internal_rating_comment = schedule[@"internal_rating_comment"];
            self.schedule.comment = schedule[@"comment"];
            self.schedule.internal_user = schedule[@"internal_name"];
            self.schedule.gigstr_name = schedule[@"gigstr_name"];
            self.schedule.gigstr_start = [[NSDateFormatter serverDateFormatter] dateFromString:schedule[@"gigstr_start"]];
            self.schedule.gigstr_stop = [[NSDateFormatter serverDateFormatter] dateFromString:schedule[@"gigstr_stop"]];
            self.schedule.gigstr_image = schedule[@"gigstr_image"];
            [self.schedule addShiftData:schedule[@"gig_data"]];
            self.schedule.shift_data_editable = [schedule[@"shift_data_editable"] boolValue];
            self.schedule.schedule_image_count = [schedule[@"schedule_image_count"] intValue];
            if ([schedule[@"description"] isEqualToString:@""]) {
                self.schedule.details = @"";
            } else {
                self.schedule.details = schedule[@"description_link"];
            }
            
            //This is a test link to test the description
//            self.schedule.details = @"http://staging-gigster.efserver.net/gig/view_gig/74";
            
            [self setupViewWithSchedule:self.schedule];
            
        }];
        
       
    }
    
    
    self.nameLabel.text = self.schedule.name;
    
}

- (void)setupViewWithSchedule:(Schedule *)schedule {
    [[NSDateFormatter dafaultDateFormatter] setDateFormat:@"EEEE"];

    NSDateComponents *components = [[NSCalendar currentCalendar]
                                    components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitTimeZone
                                    fromDate:schedule.date];
    NSTimeZone *tempTimeZone = [components timeZone];
    
    if ([tempTimeZone isDaylightSavingTimeForDate:schedule.date]) {
        [NSDateFormatter dafaultDateFormatter].timeZone = [NSTimeZone timeZoneForSecondsFromGMT:7200];
    } else {
        [NSDateFormatter dafaultDateFormatter].timeZone = [NSTimeZone timeZoneForSecondsFromGMT:3600];
    }
    
    self.dayLabel.text = [[NSDateFormatter dafaultDateFormatter] stringFromDate:schedule.date].uppercaseString;
    [[NSDateFormatter dafaultDateFormatter] setDateFormat:@"d MMM"];
    self.dateLabel.text = [[[NSDateFormatter dafaultDateFormatter] stringFromDate:schedule.date].uppercaseString stringByReplacingOccurrencesOfString:@"." withString:@""];;
    
    [[NSDateFormatter dafaultDateFormatter] setDateFormat:@"HH:mm"];
    NSString *startTime = [[NSDateFormatter dafaultDateFormatter] stringFromDate:schedule.startTime];
    NSString *endTime = [[NSDateFormatter dafaultDateFormatter] stringFromDate:schedule.endTime];
    self.timeLabel.text = [NSString stringWithFormat:@"%@-%@",startTime,endTime];
    
    self.nameLabel.text = schedule.name;
    
    self.actionButton.enabled = YES;
    self.actionButton.alpha = 1.0;
    if (schedule.status == 2) {
        //Complete mode
        self.descriptionView.hidden = NO;
        self.completedShiftView.hidden = YES;
        self.actionButton.backgroundColor = [UIColor colorWithRed:0.463 green:0.800 blue:0.784 alpha:1.000];
        [self.actionButton setImage:[UIImage imageNamed:@"Stop"] forState:UIControlStateNormal];
        [self.actionButton setTitle:NSLocalizedString(@"CHECK OUT", nil) forState:UIControlStateNormal];
        self.actionButton.hidden = NO;
    } else if (schedule.status == 1) {
        //Active mode
        self.descriptionView.hidden = NO;
        self.completedShiftView.hidden = YES;
        self.actionButton.backgroundColor = [UIColor colorWithRed:0.992 green:0.776 blue:0.180 alpha:1.000];
        [self.actionButton setImage:[UIImage imageNamed:@"stop_clock_ffffff"] forState:UIControlStateNormal];
        [self.actionButton setTitle:NSLocalizedString(@"CHECK IN", nil) forState:UIControlStateNormal];
        self.actionButton.hidden = NO;
        [[LocationManager sharedInstance] startUpdatingLocation];
    } else if (schedule.status == 3) {
        self.descriptionView.hidden = YES;
        self.completedShiftView.hidden = NO;
        self.actionButton.backgroundColor = [UIColor colorWithRed:0.494 green:0.718 blue:0.176 alpha:1.000];
        [self.actionButton setImage:[UIImage imageNamed:@"applied"] forState:UIControlStateNormal];
        [self.actionButton setTitle:NSLocalizedString(@"COMPLETED", nil) forState:UIControlStateNormal];
        self.actionButton.hidden = NO;
        self.actionButton.alpha = 1;
        [self.completeSiftTVC setupShiftDetails:self.schedule];
    } else {
        //Hide button if the schedule is complete or inactive
        self.actionButton.hidden = YES;
        self.descriptionView.hidden = NO;
        self.completedShiftView.hidden = YES;
    }
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:schedule.details]]];
}

- (void)checkINSchedule:(Schedule *)schedule {
    
    if (![[LocationManager sharedInstance] isLocationAvilableForVC:self]) {
        return;
    }
    NSUserDefaults *defaults    = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedArray = [defaults objectForKey:@"userObject"];
    if (dataRepresentingSavedArray != nil)
    {
    User *user =  (User *)[NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
        NSString *lat = @([[LocationManager sharedInstance] currentLocation].coordinate.latitude).stringValue;
        NSString *longtitude = @([[LocationManager sharedInstance] currentLocation].coordinate.longitude).stringValue;
        
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [Schedule checkINSchedule:schedule.scheduleId user:user.userid token:user.token latitude:lat longtitude:longtitude completion:^(BOOL success, NSDictionary *data, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
        
        if (error) {
            [self showAlert:[error localizedDescription]];
            return;
        }
        
        int status = [data[@"status"] intValue];
        if (status != 0) {
            return;
        }
        
        //Success
        [self fetchScheduleDetails];
    }];
    }
}

- (void)checkOUTSchedule:(Schedule *)schedule{

    ReportViewController *reportVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportViewController"];
    reportVC.schedule = self.schedule;
    reportVC.delegate = self;
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:reportVC];
    [self presentViewController:navigationController animated:YES completion:nil];
}

- (void)ReportViewController:(ReportViewController *)reportViewController
       didDismissWithSuccess:(BOOL)success {
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (void)goBack {
    [[LocationManager sharedInstance] stopUpdatingLocation];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionButtonTapped:(id)sender {
    
    if (self.schedule.status == 2) {
        [self checkOUTSchedule:self.schedule];
    } else if (self.schedule.status == 1) {
        [self checkINSchedule:self.schedule];
    } else if (self.schedule.status == 3) {
        [self goBack];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"CompletedShiftTableViewController"]) {
        CompletedShiftTableViewController *embed = segue.destinationViewController;
        embed.schedule = self.schedule;
        self.completeSiftTVC = embed;
    }
}



- (void)webViewDidFinishLoad:(UIWebView *)webView
{
//    NSString *fontSize=@"143";
//    NSString *jsString = [[NSString alloc]      initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%d%%'",[fontSize intValue]];
//    [webView stringByEvaluatingJavaScriptFromString:jsString];
    
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        NSLog(@"link clicked = %@",request.mainDocumentURL);
        
        WebViewController *detailController   = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];;
        detailController.webURL               = request.mainDocumentURL;
        [self.navigationController pushViewController:detailController animated:YES];
        return NO;
    }
    return YES;
}

-(void)showAlert:(NSString *)message{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                         }];
    [alert addAction:ok];
    
}


@end
