//
//  EditShiftDataViewController.h
//  Gigster
//
//  Created by Rakitha Perera on 5/29/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Schedule.h"
#import "XCDFormInputAccessoryView.h"

@interface EditShiftDataViewController : UIViewController

@property (strong, nonatomic) NSMutableArray *shiftDataArr;
@property (strong, nonatomic) Schedule *schedule;

@end
