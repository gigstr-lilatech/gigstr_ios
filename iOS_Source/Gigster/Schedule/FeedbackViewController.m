//
//  FeedbackViewController.m
//  Gigster
//
//  Created by Rakitha Perera on 4/17/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import "FeedbackViewController.h"
#import "DJWStarRatingView.h"
#import "User.h"
#import "MBProgressHUD.h"
#import "IQKeyboardManager.h"


@interface FeedbackViewController () <UITextViewDelegate, DJWStarRatingViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *placeholder;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIView *ratingContentView;
@property(nonatomic,strong) DJWStarRatingView *ratingView;
@property (weak, nonatomic) IBOutlet UILabel *youRockLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomLayConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *youRockBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewHeightConstraint;

@end

@implementation FeedbackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    self.placeholder = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0.0,self.textView.frame.size.width - 10.0, 34.0)];
    
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self.view action:@selector(endEditing:)]];
    
    [[self.submitButton layer] setCornerRadius:5.0f];
    [[self.submitButton layer] setMasksToBounds:YES];
    if (IS_IPHONE_5_OR_LESS) {
        [self.youRockLabel setFont:[UIFont fontWithName:HELVETICA_BOLD_FONT size:35.0f]];
        self.bottomLayConstraint.constant = 10;
        self.youRockBottomConstraint.constant = 5.0f;
        self.textViewHeightConstraint.constant = 105.0f;
        
    }
    
    [self.placeholder setText:NSLocalizedString(@"Tell us about your day", "")];
    [self.placeholder setBackgroundColor:[UIColor clearColor]];
    [self.placeholder setTextColor:[UIColor lightGrayColor]];
    self.placeholder.numberOfLines = 5;
    self.textView.delegate = self;
    
//    [self.textView addSubview:self.placeholder];
    
    [self setupRatingView];
    
    [self.navigationController.navigationBar setHidden:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
}

-(void)viewWillDisappear:(BOOL)animated {
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
}

- (void)textViewDidEndEditing:(UITextView *)theTextView
{
    if (![self.textView hasText]) {
        self.placeholder.hidden = NO;
    }
}

- (void) textViewDidChange:(UITextView *)textView
{
    if(![textView hasText]) {
        self.placeholder.hidden = NO;
    }
    else{
        self.placeholder.hidden = YES;
    }
}

- (IBAction)submitClicked:(id)sender {
    
    if (self.ratingView.rating == 0) {
        [self showAlert:NSLocalizedString(@"Please give rating with comment", nil)];
        return;
    } else if ([self.textView.text isEqualToString:@""]) {
        [self showAlert:NSLocalizedString(@"Please give rating with comment", nil)];
        return;
    }
    
    NSUserDefaults *defaults    = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedArray = [defaults objectForKey:@"userObject"];
    if (dataRepresentingSavedArray != nil)
    {
        User *user =  (User *)[NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
        int rating = @(self.ratingView.rating).intValue;
        NSString *feedback = self.textView.text;
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [Schedule rateSchedule:self.schedule.scheduleId token:user.token userID:user.userid rate:rating feedback:feedback completion:^(BOOL success, NSDictionary *data, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            });
            
            if (error) {
                [self showAlert:[error localizedDescription]];
                return;
            }
            
            int status = [data[@"status"] intValue];
            if (status != 0) {
                return;
            }
            
            //Success
            [self.delegate FeedbackViewController:self didDismissWithSuccess:YES];
            [self dismissViewControllerAnimated:YES completion:nil];
            
        }];
    }
}
- (IBAction)notTodayClicked:(id)sender {
    [self.delegate FeedbackViewController:self didDismissWithSuccess:NO];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewWillLayoutSubviews {
    
    CGRect frame = self.ratingView.frame;
    frame.size = self.ratingView.intrinsicContentSize;
    self.ratingView.frame = frame;

    CGPoint p = [self.ratingContentView.superview convertPoint:self.ratingContentView.center toView:self.ratingContentView];
    
    self.ratingView.center = p;
}

- (void)setupRatingView {
    if (IS_IPHONE_5_OR_LESS) {
        self.ratingView = [[DJWStarRatingView alloc] initWithStarSize:CGSizeMake(45, 45) numberOfStars:5 rating:2.0 fillColor:[UIColor colorWithRed:1.00 green:0.81 blue:0.34 alpha:1.0] unfilledColor:[UIColor colorWithRed:0.87 green:0.87 blue:0.87 alpha:0.6] strokeColor:[UIColor colorWithRed:0.65 green:0.65 blue:0.65 alpha:1.0]];
    } else {
        self.ratingView = [[DJWStarRatingView alloc] initWithStarSize:CGSizeMake(50, 50) numberOfStars:5 rating:2.0 fillColor:[UIColor colorWithRed:1.00 green:0.81 blue:0.34 alpha:1.0] unfilledColor:[UIColor colorWithRed:0.87 green:0.87 blue:0.87 alpha:0.6] strokeColor:[UIColor colorWithRed:0.65 green:0.65 blue:0.65 alpha:1.0]];
    }
    
    self.ratingView.lineWidth = 0.2;
    [self.ratingContentView addSubview:self.ratingView];
    CGPoint p = [self.ratingContentView.superview convertPoint:self.ratingContentView.center toView:self.ratingContentView];
    
    self.ratingView.center = p;
    self.ratingView.editable = YES;
    self.ratingView.padding = 10;
    self.ratingView.rating = 0;
    self.ratingView.allowsHalfIntegralRatings = NO;
    self.ratingView.hidden = NO;
    self.ratingView.delegate = self;
    
    _textView.textContainerInset = UIEdgeInsetsMake(10, 5, 10, 5);
}

- (void)djwStarRatingChangedValue:(DJWStarRatingView *)view {

}

-(void)showAlert:(NSString *)message{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                         }];
    [alert addAction:ok];
    
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
