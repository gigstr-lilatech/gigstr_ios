//
//  ScheduleDetailViewController.h
//  Gigster
//
//  Created by Rakitha Perera on 7/12/16.
//  Copyright © 2016 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "Schedule.h"

@interface ScheduleDetailViewController : BaseViewController

@property (strong, nonatomic) Schedule *schedule;

@end
