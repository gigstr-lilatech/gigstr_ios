//
//  HTTPClient.h
//  Gigster
//
//  Created by Rakitha Perera on 5/4/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

@interface HTTPClient : AFHTTPSessionManager 
+ (instancetype)sharedClient;
    
@property (nonatomic, strong) AFURLSessionManager *downloadManager;
@property (nonatomic, strong) AFHTTPSessionManager *uploadManager;
@property (nonatomic, copy) void (^backgroundSessionCompletionHandler)(void);
    
-(void)initSessionConfiguration:(NSURLSessionConfiguration *)configuration;
- (void)configureBackgroundSessionCompletion;
@end
