//
//  ChatListCell.h
//  Gigster
//
//  Created by EFutures on 12/10/15.
//  Copyright © 2015 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatListCell : UITableViewCell

@property(nonatomic,weak)IBOutlet UIImageView *gigImage;
@property(nonatomic,weak)IBOutlet UILabel *title;
@property(nonatomic,weak)IBOutlet UILabel *desc;
@property(nonatomic,weak)IBOutlet UILabel *modifiedTime;
@property(nonatomic,weak)IBOutlet UILabel *unreadCount;

-(void)adjustFrames;

@end
