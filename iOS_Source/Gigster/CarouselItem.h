//
//  CarouselItem.h
//  Gigster
//
//  Created by Rakitha Perera on 8/8/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CarouselItem : NSObject
@property (nonatomic, strong) NSNumber* carousel_id;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* item_description;
@property (nonatomic, strong) NSString* action;
@property (nonatomic, strong) NSString* image;
@property (nonatomic, strong) NSString* url;

-(id)initWithDic:(NSDictionary *)data;
@end
