//
//  MPSkewedCell.h
//  MPSkewed
//
//  Created by Alex Manzella on 17/05/15.
//  Copyright (c) 2015 Alex Manzella. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MPSkewedCell : UICollectionViewCell


@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *subttl1;
@property (nonatomic, strong) UILabel *subttl2;
@property (nonatomic, strong) UIImageView *imageViewNew;
@property (nonatomic, strong) UIActivityIndicatorView *activity;

@property (nonatomic, assign) UIImage *image;
@property (nonatomic, assign) NSString *text;
@property (nonatomic, assign) NSString *sub1Text;
@property (nonatomic, assign) NSString *sub2Text;
@property (nonatomic, assign) BOOL appliedStatus;
@property (nonatomic, strong) UIButton *appliedStamp;

-(void)adjustAppliedUI;

@end
