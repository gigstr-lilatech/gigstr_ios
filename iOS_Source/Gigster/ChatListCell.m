//
//  ChatListCell.m
//  Gigster
//
//  Created by EFutures on 12/10/15.
//  Copyright © 2015 EFutures. All rights reserved.
//

#import "ChatListCell.h"

@implementation ChatListCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)adjustFrames{
    
    self.unreadCount.layer.cornerRadius = self.unreadCount.frame.size.width / 2;
    self.unreadCount.clipsToBounds = YES;
    ;
}
@end
