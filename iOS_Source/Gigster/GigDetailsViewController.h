//
//  GigDetailsViewController.h
//  Gigster
//
//  Created by EFutures on 11/24/15.
//  Copyright © 2015 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Jobs.h"
#import "BaseViewController.h"

@interface GigDetailsViewController : BaseViewController

@property(nonatomic,strong)Jobs *job;
@end
