//
//  TermsViewController.m
//  Gigster
//
//  Created by EFutures on 11/23/15.
//  Copyright © 2015 EFutures. All rights reserved.
//

#import "TermsViewController.h"

@interface TermsViewController ()

@property(nonatomic,weak)IBOutlet UIWebView *webView;
@property(nonatomic,weak)IBOutlet UIActivityIndicatorView *activity;
@end

@implementation TermsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupBackButton:@"Back"];
    [_activity setHidden:NO];
    [_activity startAnimating];
    NSString *urlString = [NSString stringWithFormat:@"https://www.gigstr.com/terms-gigstr/"];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [_activity setHidden:YES];
    [_activity stopAnimating];
    NSLog(@"Current URL = %@",webView.request.URL);
    
    //-- Add further custom actions if needed
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
