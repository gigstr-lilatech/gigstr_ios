//
//  SocialViewController.h
//  Gigster
//
//  Created by Rakitha Perera on 4/28/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface SocialViewController : BaseViewController

@property(nonatomic,strong)NSURL *webURL;

@end
