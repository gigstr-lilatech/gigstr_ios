//
//  Api.h
//  Znapify
//
//  Created by Chathura Payagala on 12/8/14.
//  Copyright (c) 2014 Chathura Payagala. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Api : NSObject

+ (void) post:(NSDictionary *) data onCompletion:(void (^) (BOOL success, id response, NSError *error)) onCompletion;

@end
