//
//  CarouselCollectionViewCell.h
//  Gigster
//
//  Created by Rakitha Perera on 8/9/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CarouselItem.h"

@interface CarouselCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *cImageView;
@property (weak, nonatomic) IBOutlet UILabel *cTitleLbel;
@property (weak, nonatomic) IBOutlet UILabel *cDescriptionLabel;

- (void)configCellWithData:(CarouselItem *)item;
@end
