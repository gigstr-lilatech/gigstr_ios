//
//  ChatListViewController.m
//  Gigster
//
//  Created by EFutures on 12/7/15.
//  Copyright © 2015 EFutures. All rights reserved.
//

#import "ChatListViewController.h"
#import "ChatDTO.h"
#import "User.h"
#import "ChatListCell.h"
#import "UIImageView+AFNetworking.h"
#import "ChatViewController.h"
#import "SigninViewController.h"
#import "SignupViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface ChatListViewController (){
    
    NSString *userImageURL;
}

@property(nonatomic,weak)IBOutlet UITableView *chatsListTable;

@property(nonatomic,weak)IBOutlet UIView *loginView;
@property(nonatomic,weak)IBOutlet UIView *loginViewWhite;

@property(nonatomic,strong)NSMutableArray *chatsArray;

@property(nonatomic,weak)IBOutlet UIButton *fbSignup;
@property(nonatomic,weak)IBOutlet UIButton *emailSignup;

@property(nonatomic,weak)IBOutlet UIActivityIndicatorView *activity;
@property(nonatomic,weak)IBOutlet UILabel *lblNoChat;

@end

@implementation ChatListViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [_activity setHidden:YES];
    [_lblNoChat setHidden:YES];
    
    [[_fbSignup layer] setCornerRadius:5.0f];
    [[_fbSignup layer] setMasksToBounds:YES];
    
    [[_emailSignup layer] setCornerRadius:5.0f];
    [[_emailSignup layer] setMasksToBounds:YES];
    
    [[_loginViewWhite layer]setCornerRadius:5.0f];
    
    self.chatsArray = [[NSMutableArray alloc]init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateChatList)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
}

-(void)dealloc {

    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self pushBadgeClear];
    
    [self updateChatList];
    
}

- (void)updateChatList {
    NSUserDefaults *defaults    = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedArray = [defaults objectForKey:@"userObject"];
    if (dataRepresentingSavedArray != nil)
    {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
        _loginView.hidden = YES;
        
        User *user =  (User *)[NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
        
        [ChatDTO getChatLiostforUser:user.userid token:user.token completion:^(BOOL success, NSDictionary *data, NSError *error) {
            
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            
            NSLog(@"success: %d", success);
            NSLog(@"data: %@", data);
            NSLog(@"Error: %@", error);
            
            if (success) {
                
                [self passData:[data objectForKey:@"chat"]];
                
                userImageURL = [data objectForKey:@"user_image"];
                
                [_chatsListTable reloadData];
            }
            else{
                
                if ([data valueForKey:@"message"]) {
                    NSString *status = [NSString stringWithFormat:@"%@",[data valueForKey:@"message"]];
                    [self showAlert:status];
                    
                }else{
                    //[self showAlert:NSLocalizedString(@"Could not connect to the server. Please try again", nil)];
                }
            }
            
        }];
    }else{
        
        _loginView.hidden = NO;
    }
}

-(void)pushBadgeClear{
    
    [User updatePushCountcompletion:^(BOOL success, NSDictionary *data, NSError *error) {
        
        [UIApplication sharedApplication].applicationIconBadgeNumber= 0;
        
        NSLog(@"success: %d", success);
        NSLog(@"data: %@", data);
        NSLog(@"Error: %@", error);
        
    }];
    
}


-(void)passData:(NSArray *)dataArray{
    
    [self.chatsArray removeAllObjects];
    
    for (int i =0; i<[dataArray count]; i++) {
        
        NSMutableDictionary *dic = [dataArray objectAtIndex:i];
        
        ChatDTO *chat = [[ChatDTO alloc]init];
        chat.chatID = [dic objectForKey:@"chat_id"];
        chat.gigID = [dic objectForKey:@"gig_id"];
        chat.imageURL = [dic objectForKey:@"image"];
        chat.title = [dic objectForKey:@"title"];
        chat.modified = [dic objectForKey:@"modified"];
        chat.unreadCount = [dic objectForKey:@"unread_message"];
        chat.unreadMessage = [dic objectForKey:@"unread_message_max"];
        
        [self.chatsArray addObject:chat];
    }
    
    if (self.chatsArray.count > 0) {
        
        [_lblNoChat setHidden:YES];
        
    }else{
        
        [_lblNoChat setHidden:NO];
    }
}

-(void)showAlert:(NSString *)message{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:NSLocalizedString(message, nil)
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:NSLocalizedString(@"OK", nil)
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             [self dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    [alert addAction:ok];
    
}


-(IBAction)fblogingClicked:(id)sender{
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    [login
     logInWithReadPermissions: @[@"public_profile",@"user_birthday",@"email"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error %@",error.description);
         } else if (result.isCancelled) {
             NSLog(@"%@",result);
             [self showAlert:@"Couldn’t connect to Facebook. Please try again."];
         } else {
             NSLog(@"%@",result);
             NSLog(@"Logged in");
             [self getDetailsAndLogin];
         }
     }];
}

-(void)getDetailsAndLogin{
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:[NSDictionary dictionaryWithObject:@"cover,picture.type(large),id,name,first_name,last_name,gender,birthday,email,location,hometown,bio,photos" forKey:@"fields"]]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         if (!error) {
             
             NSLog(@"%@",result);
             NSString *userID = [[FBSDKAccessToken currentAccessToken] userID];
             NSString *userName = [result objectForKeyNotNull:@"name"];
             NSString *email =  [result objectForKeyNotNull:@"email"];
             NSString *userImageurl = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [[FBSDKAccessToken currentAccessToken] userID]];
             NSString *gender = [[result objectForKeyNotNull:@"gender"] uppercaseString];
             // NSString *DOB = [result objectForKeyNotNull:@"birthday"];
             
             
             NSString *pushToken;
             if ([[NSUserDefaults standardUserDefaults] stringForKey:@"pushDeviceToken"]) {
                 
                 pushToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"pushDeviceToken"];
             }else{
                 
                 pushToken = @"";
             }
             
             NSString *emails = (email) ? email : @"";
             
             //NSString *dobFB = (DOB) ? DOB : @"";
             
             NSString *dateString = [result objectForKeyNotNull:@"birthday"];
             NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
             [dateFormatter setDateFormat:@"MM/dd/yyyy"];
             NSDate *dateFromString = [dateFormatter dateFromString:dateString];
             dateFromString = [dateFormatter dateFromString:dateString];
             
             NSString *date;
             NSString *month;
             NSString *year;
             
             if (dateFromString) {
                 
                 NSDateFormatter *dateF1 = [[NSDateFormatter alloc] init];
                 [dateF1 setDateFormat:@"yyyy"];
                 year = [dateF1 stringFromDate:dateFromString];
                 
                 NSDateFormatter *dateF2 = [[NSDateFormatter alloc] init];
                 [dateF2 setDateFormat:@"MM"];
                 month = [dateF2 stringFromDate:dateFromString];
                 
                 NSDateFormatter *dateF3 = [[NSDateFormatter alloc] init];
                 [dateF3 setDateFormat:@"dd"];
                 date = [dateF3 stringFromDate:dateFromString];
             }
             else{
                 
                 NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                 [dateFormatter setDateFormat:@"MM/dd"];
                 NSDate *dateFromString = [[NSDate alloc] init];
                 dateFromString = [dateFormatter dateFromString:dateString];
                 
                 year = @"";
                 
                 NSDateFormatter *dateF2 = [[NSDateFormatter alloc] init];
                 [dateF2 setDateFormat:@"MM"];
                 month = [dateF2 stringFromDate:dateFromString];
                 
                 NSDateFormatter *dateF3 = [[NSDateFormatter alloc] init];
                 [dateF3 setDateFormat:@"dd"];
                 date = [dateF3 stringFromDate:dateFromString];
                 
             }
             
             NSString *dateX = (date) ? date : @"";
             NSString *monthX = (month) ? month : @"";
             NSString *yearX = (year) ? year : @"";
             
             NSString *genderFB;
             
             if ([gender isEqualToString:@"MALE"]) {
                 genderFB = @"M";
             }else if ([gender isEqualToString:@"FEMALE"]){
                 genderFB = @"F";
             }else{
                 genderFB = @"";
             }
             
             
             NSDictionary *params    = @{@"fbid": userID, @"name": userName, @"email":emails, @"day": dateX, @"month": monthX, @"year": yearX, @"gender": genderFB, @"accountType": [NSNumber numberWithInt:1], @"device_id":pushToken, @"device_type": @"2", @"image" :userImageurl};
             
             //             NSDictionary *params    = @{@"fbid": @"10156311001425287", @"name": @"Rasmus Solholm", @"email":@"solholmrasmus@gmail.com", @"day": dateX, @"month": monthX, @"year": yearX, @"gender": genderFB, @"accountType": [NSNumber numberWithInt:1], @"device_id":pushToken, @"device_type": @"2", @"image" :userImageURL};
             
             [User loginWithFacebookid:params completion:^(BOOL success, NSDictionary *data, NSError *error) {
                 
                 //                 [_activity stopAnimating];
                 //                 [_activity setHidden:YES];
                 
                 [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
                 
                 NSLog(@"success: %d", success);
                 NSLog(@"data: %@", data);
                 NSLog(@"Error: %@", error);
                 
                 if (success) {
                     
                     User *user         = [[User alloc] init];
                     user.userid        = [NSString stringWithFormat:@"%@",[data valueForKey:@"user_id"]];
                     user.token         = [NSString stringWithFormat:@"%@",[data valueForKey:@"token"]];
                     user.email         = email;
                     user.name          = userName;
                     user.userImageURL  = userImageurl;
                     //user.password    = password;
                     
                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                     [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:user] forKey:@"userObject"];
                     [defaults synchronize];
                     
                     [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                     
                     [self viewWillAppear:NO];
                 }
                 
                 else{
                     
                     if ([data valueForKey:@"message"]) {
                         NSString *status = [NSString stringWithFormat:@"%@",[data valueForKey:@"message"]];
                         [self showAlert:status];
                         
                     }else{
                         [self showAlert:NSLocalizedString(@"Could not connect to the server. Please try again", nil)];
                     }
                 }
                 
             }];
         }
         else{
             
             NSLog(@"%@",error.localizedDescription);
             [self showAlert:error.localizedDescription];
         }
     }];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.chatsArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 88.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        
    static NSString *cellIdentifier = @"ChatListCell";
    
    ChatListCell *cell = [_chatsListTable dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ChatListCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        
    }
    
    [cell adjustFrames];
    
    ChatDTO *chat = [self.chatsArray objectAtIndex:indexPath.row];
    cell.title.text = chat.title;
    cell.desc.text = NSLocalizedString(chat.unreadMessage, nil);

    if ([chat.unreadCount intValue]>0) {
        
        cell.unreadCount.hidden = NO;
        cell.unreadCount.text = chat.unreadCount;
        cell.modifiedTime.textColor = [UIColor colorWithRed:0.55 green:0.75 blue:0.31 alpha:1.0];;
        
    }else{
        cell.unreadCount.hidden = YES;
        cell.modifiedTime.textColor = [UIColor blackColor];
    }
    
    
    if (chat.chatImage) {
        
        cell.gigImage.image = chat.chatImage;
    }
    else{
        
        __weak ChatListCell *weakCell = cell;
        
        NSURL *url = [NSURL URLWithString:chat.imageURL];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        UIImage *placeholderImage = [UIImage imageNamed:@"gigstr_inbox_placeholder_icon"];
        
        
        [weakCell.gigImage setImageWithURLRequest:request
                                 placeholderImage:placeholderImage
                                          success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                              
                                              chat.chatImage = image;
                                              weakCell.gigImage.image = image;
                                              //                                           [weakCell.activity stopAnimating];
                                              //                                           weakCell.activity.hidden = YES;
                                              
                                              //[weakCell setNeedsLayout];
                                              
                                              
                                          } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                              
                                              NSLog(@"response %@",response);
                                              NSLog(@"error %@",error);
                                          }];
        
    }
 
    
    cell.modifiedTime.text = [self dateAfterComparision:chat.modified];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    }

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self performSegueWithIdentifier:@"segueChatview" sender:self];
}

-(NSString *)dateAfterComparision:(NSString *)serverDate{
    
    //server Date
    NSString *dateServer = serverDate;
    NSDateFormatter *dateF1 = [[NSDateFormatter alloc] init];
    [dateF1 setDateFormat:@"yyy-MM-dd HH:mm:ss"];
    dateF1.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateF1 dateFromString:dateServer];
    
    NSDateFormatter *dateF2 = [[NSDateFormatter alloc] init];
    dateF2.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:[NSTimeZone localTimeZone].secondsFromGMT];
    [dateF2 setDateFormat:@"dd MMM"];
    NSString *stringFromServerDate = [dateF2 stringFromDate:dateFromString];
    
    
    //Current date
    NSDateFormatter *df1 = [[NSDateFormatter alloc] init];
    df1.dateFormat = @"dd MMM";
    NSString *currentDate = [df1 stringFromDate:[NSDate date]];

    //comparison
    if ([currentDate isEqualToString:stringFromServerDate]) {
        
        NSString *dateString = serverDate;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyy-MM-dd HH:mm:ss"];
        dateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        NSDate *dateFromString = [[NSDate alloc] init];
        dateFromString = [dateFormatter dateFromString:dateString];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:[NSTimeZone localTimeZone].secondsFromGMT];
        [formatter setDateFormat:@"HH:mm a"];
        NSString *stringFromDate = [formatter stringFromDate:dateFromString];
        
        //return time
        return stringFromDate;
        
    }else{
        //return Date
        return stringFromServerDate;
    }
    
}

#pragma mark - Navigation

 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 
 if ([[segue identifier] isEqualToString:@"segueChatview"]){
 
     NSIndexPath *path = [self.chatsListTable indexPathForSelectedRow];
     ChatDTO *chat = [self.chatsArray objectAtIndex:path.row];
     chat.userImageURL = userImageURL;
     //[segue.destinationViewController setDetail:detail];
     ChatViewController *chatMessages   = (ChatViewController *)[segue destinationViewController];
     chatMessages.userImageURL          = userImageURL;
     chatMessages.chatDTO               = chat;
 }else if([[segue identifier] isEqualToString:@"segueSignupInbox"]){
     
     UINavigationController *navcontroller = (UINavigationController *)[segue destinationViewController];
     SignupViewController *signupVC   = [navcontroller.viewControllers objectAtIndex:0];
     signupVC.isFromTabbar            = YES;
     
 }else if ([[segue identifier] isEqualToString:@"segueSigninInbox"]){
     
     UINavigationController *navcontroller = (UINavigationController *)[segue destinationViewController];
     SigninViewController *signinVC   = [navcontroller.viewControllers objectAtIndex:0];
     signinVC.isFromTabbar            = YES;
 }
}

@end
