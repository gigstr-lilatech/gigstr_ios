//
//  CarouselCell.h
//  Gigster
//
//  Created by Rakitha Perera on 8/9/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Explore.h"
#import "CarouselItem.h"

@class CarouselCell;
@protocol CarouselCellDelegate<NSObject>

- (void)CarouselCell:(CarouselCell *)cell
                 didSelecectItem:(CarouselItem *)carouselItem;

@end

@interface CarouselCell : UITableViewCell <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *carouselTitleLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) id <CarouselCellDelegate> delegate;

@property (strong, nonatomic) NSArray *items;

- (void)configCellWithData:(Explore *)item;

@end
