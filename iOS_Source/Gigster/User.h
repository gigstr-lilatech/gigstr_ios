//
//  User.h
//  Gigster
//
//  Created by EFutures on 11/11/15.
//  Copyright © 2015 EFutures. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString * const kCountryCode = @"X-Country";
static NSString * const kLanguageCode = @"X-Language";
static NSString * const kDeviceID = @"X-deviceID";

@interface User : NSObject

@property NSString* userid;
@property NSString* name;
@property NSString* email;
@property NSString* password;
@property NSString* token;
@property NSString* accountType;
@property NSString* mobile;
@property NSString* address;
@property NSString* gender;
@property NSString* person_number;
@property NSString* fbID;
@property NSString* userImageURL;
@property NSString* bankAccountNo;


+ (void) loginWithEmail:(NSString *) email password:(NSString *) password accountType:(int)accountType completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion;

+ (void) userRegisterWithName:(NSString *)name email:(NSString *)email password:(NSString *)password accountType:(int)accountType completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion;

+ (void) resetPasswordwithEmail:(NSString *)email completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion;

+ (void) loginWithFacebookid:(NSDictionary *)params completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion;

+ (void) logoutWithUserID:(NSDictionary *)params  completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion;

+ (void) getPersonnelInfoForUserID:(NSString *)userID token:(NSString *)token completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion;

+ (void) getAccountInfoForUserID:(NSString *)userID token:(NSString *)token completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion;

+ (void) savePersonnelData:(NSMutableDictionary *)parameters completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion;

+ (void) saveBanklData:(NSMutableDictionary *)parameters completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion;

+ (void)updatePushCountcompletion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion;

+ (void)getCountryListOncompletion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion;

+ (void)setCountry:(NSDictionary *)params Oncompletion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion;

+ (void)registerDeviceWithInviter:(NSString *)userID Completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion;
@end
