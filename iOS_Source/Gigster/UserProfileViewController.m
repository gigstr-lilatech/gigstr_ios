    //
//  UserProfileViewController.m
//  Gigster
//
//  Created by EFutures on 12/7/15.
//  Copyright © 2015 EFutures. All rights reserved.
//

#import "UserProfileViewController.h"
#import "User.h"
#import "UIImageView+AFNetworking.h"
#import "AppDelegate.h"
#import "LoginFlowViewController.h"
#import "AFNetworking.h"
#import "UIImage+Resize.h"
#import "EnterPitchViewController.h"
#import "SignupViewController.h"
#import "SigninViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "UIDevice+Resolutions.h"
#import "CountrySelectViewController.h"
#import "DJWStarRatingView.h"
#import "IQKeyboardManager.h"
#import "HTTPClient.h"

@interface UserProfileViewController () <UIPickerViewDelegate,UITextViewDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UpdatePichTextProtocol, CountrySelectViewControllerDelegate, UITextFieldDelegate>{
    
    NSArray *pickerArray;
    NSArray *countryArray;
    NSString *selectedPicker;
    NSString *selectedCountry;
    
    UIImagePickerController *ipc;
    UIPopoverController *popover;
    CGFloat bottomContraint;
    CGFloat bottomContraintToView;
    BOOL fromCameraPopup;
    BOOL fromPitchPopup;
}

@property(nonatomic,weak)IBOutlet UIView *profileView;
@property(nonatomic,weak)IBOutlet UIView *bankView;
@property(nonatomic,weak)IBOutlet UIView *infoView;
@property(nonatomic,weak)IBOutlet UIView *loginView;

@property(nonatomic,weak)IBOutlet UIButton *fbSignup;
@property(nonatomic,weak)IBOutlet UIButton *emailSignup;

@property(nonatomic,weak)IBOutlet UIScrollView *profileScroll;
@property (weak, nonatomic) IBOutlet UIScrollView *infoScroll;
@property (weak, nonatomic) IBOutlet UIScrollView *loginScroll;

@property(nonatomic,weak)IBOutlet UIButton *btnProfile;
@property(nonatomic,weak)IBOutlet UIButton *btnBank;
@property(nonatomic,weak)IBOutlet UIButton *btnInfo;


@property(nonatomic,weak)IBOutlet UILabel *lblInfo;
@property(nonatomic,weak)IBOutlet UILabel *lblprofile;
@property(nonatomic,weak)IBOutlet UILabel *lblBank;

@property(nonatomic,weak)IBOutlet UIButton *btnProfileSave;
@property(nonatomic,weak)IBOutlet UIButton *btnBankSave;

@property(nonatomic,weak)IBOutlet UITextField *txtName;
@property(nonatomic,weak)IBOutlet UITextField *txtEmail;
//@property(nonatomic,weak)IBOutlet UITextView *txtPitch;
@property(nonatomic,weak)IBOutlet UIButton *btnPitch;
@property(nonatomic,weak)IBOutlet UITextField *txtAddress;
@property(nonatomic,weak)IBOutlet UITextField *txtStreet;
@property(nonatomic,weak)IBOutlet UITextField *txtCity;
@property(nonatomic,weak)IBOutlet UITextField *txtCountry;
@property(nonatomic,weak)IBOutlet UITextField *txtZip;
@property(nonatomic,weak)IBOutlet UITextField *txtMobile;
@property(nonatomic,weak)IBOutlet UITextField *txtPersonNumbr;
@property(nonatomic,weak)IBOutlet UITextField *txtGender;
@property(nonatomic,weak)IBOutlet UITextField *txtBirthday;
@property(nonatomic,weak)IBOutlet UIButton *btndrivingLice;

@property(nonatomic,weak)IBOutlet UITextField *txtBankname;
@property(nonatomic,weak)IBOutlet UITextField *txtBankClearingNo;
@property(nonatomic,weak)IBOutlet UITextField *txtBankAccNo;

@property(nonatomic,weak)IBOutlet UIImageView *userImage;

@property(nonatomic,weak)IBOutlet UILabel *lblUsername;
@property(nonatomic,weak)IBOutlet UILabel *lblEmail;

@property(nonatomic,weak)IBOutlet UIActivityIndicatorView *activity;
@property(nonatomic,weak)IBOutlet UIActivityIndicatorView *activityProfileSave;
@property(nonatomic,weak)IBOutlet UIActivityIndicatorView *activityBankSave;
@property(nonatomic,weak)IBOutlet UIActivityIndicatorView *activityProfImage;

@property (strong, nonatomic) IBOutlet UIView *viewPickerBackground;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerView;

@property (strong, nonatomic) IBOutlet UIView *datePickerBackground;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePickerView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtViewbottomContraint;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtViewbottomContraintToView;

@property(nonatomic,strong)User *userDetails;
@property (weak, nonatomic) IBOutlet UIButton *countryButton;
@property (weak, nonatomic) IBOutlet UIView *ratingContentView;
@property(nonatomic,strong) DJWStarRatingView *ratingView;



@end

@implementation UserProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [self performSelector:@selector(setScrollview) withObject:nil afterDelay:1.0];
    
    _btnProfile.selected = YES;
    _lblprofile.textColor = [UIColor colorWithRed:131.0/255.0 green:213.0/255.0 blue:210.0/255.0 alpha:1.0];
    _lblInfo.textColor      = [UIColor lightGrayColor];
    _lblBank.textColor      = [UIColor lightGrayColor];
    
    _btnPitch.titleLabel.textAlignment = NSTextAlignmentCenter;
    _btnPitch.titleLabel.lineBreakMode = NSLineBreakByCharWrapping;
    _btnPitch.titleLabel.numberOfLines = 2;
    
    self.userDetails = [[User alloc]init];
    self.userDetails.gender = @"";
    [_activity setHidden:YES];
    [_activityProfileSave setHidden:YES];
    [_activityBankSave setHidden:YES];
    [_activityProfImage setHidden:YES];
    
    self.userImage.layer.cornerRadius = self.userImage.frame.size.width / 2;
    self.userImage.clipsToBounds = YES;
    fromCameraPopup = NO;
    fromPitchPopup  = NO;
    bottomContraint = self.txtViewbottomContraint.constant;
    [self setupRatingView];
    
    [[_fbSignup layer] setCornerRadius:5.0f];
    [[_fbSignup layer] setMasksToBounds:YES];
    
    [[_emailSignup layer] setCornerRadius:5.0f];
    [[_emailSignup layer] setMasksToBounds:YES];
    
    self.txtBankAccNo.delegate = self;
    self.txtPersonNumbr.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [[IQKeyboardManager sharedManager] setEnable:NO];
    [_btnProfileSave setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateNormal];
    _btnProfileSave.titleLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:17];
    
    [_btnBankSave setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateNormal];
    _btnBankSave.titleLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:17];
    
    if (fromCameraPopup || fromPitchPopup) {
        return;
    }
    NSString *response = [self getCountryList];
    NSError *e;
    NSArray *responseArray = [NSJSONSerialization JSONObjectWithData: [response dataUsingEncoding:NSUTF8StringEncoding]
                                                                options: NSJSONReadingMutableContainers
                                                                  error: &e];
    countryArray = [[NSArray alloc]initWithArray:responseArray];
    
    [self profileClicked:nil];
    [self initializeTextFieldInputView];
    [self loadPersonnelInfo];
    
    NSString *identifier = [[NSUserDefaults standardUserDefaults] valueForKey:kCountryCode];
    NSString *country = [[NSLocale currentLocale] displayNameForKey: NSLocaleCountryCode value: identifier];
    [self.countryButton setTitle:country forState:UIControlStateNormal];
}

-(void)viewWillDisappear:(BOOL)animated {
    [[IQKeyboardManager sharedManager] setEnable:YES];
}

- (void) initializeTextFieldInputView {
    
    self.pickerView.backgroundColor =[UIColor whiteColor];
    self.txtGender.inputView        = self.viewPickerBackground;
    self.txtCountry.inputView       = self.viewPickerBackground;
    [self.viewPickerBackground removeFromSuperview];
    self.pickerView.delegate        = self;
    
    self.datePickerView.backgroundColor =[UIColor whiteColor];
//    self.txtBirthday.inputView       = self.datePickerBackground;
    [self.datePickerBackground removeFromSuperview];
    //self.datePickerView        = self;
}
    
    
- (void)setupRatingView {
    self.ratingView = [[DJWStarRatingView alloc] initWithStarSize:CGSizeMake(25, 25) numberOfStars:5 rating:2.0 fillColor:[UIColor colorWithRed:1.00 green:0.81 blue:0.34 alpha:1.0] unfilledColor:[UIColor clearColor] strokeColor:[UIColor colorWithRed:0.65 green:0.65 blue:0.65 alpha:1.0]];
    self.ratingView.lineWidth = 0.6;
    [self.ratingContentView addSubview:self.ratingView];
    CGPoint p = [self.ratingContentView.superview convertPoint:self.ratingContentView.center toView:self.ratingContentView];
    
    self.ratingView.center = p;
    self.ratingView.editable = NO;
    self.ratingView.allowsHalfIntegralRatings = YES;
    self.ratingView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    self.ratingView.hidden = YES;
}


-(void)setScrollview{
    
    [_profileScroll setContentSize:CGSizeMake(_profileScroll.frame.size.width, 650)];
    [_infoScroll setContentSize:CGSizeMake(_infoScroll.frame.size.width, 323)];
    [_loginScroll setContentSize:CGSizeMake(_loginScroll.frame.size.width, 314)];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark buttonActions

-(IBAction)profileClicked:(id)sender{
    [_btnBankSave setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateNormal];
    _btnBankSave.titleLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:17];
    
    [self.view endEditing:YES];
    
    
    _btnProfile.selected    = YES;
    _btnBank.selected       = NO;
    _btnInfo.selected       = NO;
    _lblprofile.textColor   = [UIColor colorWithRed:131.0/255.0 green:213.0/255.0 blue:210.0/255.0 alpha:1.0];
    _lblInfo.textColor      = [UIColor lightGrayColor];
    _lblBank.textColor      = [UIColor lightGrayColor];
    
    _profileView.hidden     = NO;
    _bankView.hidden        = YES;
    _infoView.hidden        = YES;
    
    [self loadInitialView];
}

-(IBAction)bankClicked:(id)sender{
    [_btnProfileSave setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateNormal];
    _btnProfileSave.titleLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:17];
    
    [self.view endEditing:YES];
    
    _btnProfile.selected    = NO;
    _btnBank.selected       = YES;
    _btnInfo.selected       = NO;
    _lblBank.textColor   = [UIColor colorWithRed:131.0/255.0 green:213.0/255.0 blue:210.0/255.0 alpha:1.0];
    _lblInfo.textColor      = [UIColor lightGrayColor];
    _lblprofile.textColor      = [UIColor lightGrayColor];
 
    _profileView.hidden = YES;
    _bankView.hidden    = NO;
    _infoView.hidden    = YES;
}

-(IBAction)infoClicked:(id)sender{
    
    [_btnProfileSave setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateNormal];
    _btnProfileSave.titleLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:17];
    
    [_btnBankSave setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateNormal];
    _btnBankSave.titleLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:17];
    
    [self.view endEditing:YES];
    
    _btnProfile.selected    = NO;
    _btnBank.selected       = NO;
    _btnInfo.selected       = YES;
    _lblInfo.textColor      = [UIColor colorWithRed:131.0/255.0 green:213.0/255.0 blue:210.0/255.0 alpha:1.0];
    _lblprofile.textColor   = [UIColor lightGrayColor];
    _lblBank.textColor      = [UIColor lightGrayColor];
    
    _profileView.hidden = YES;
    _bankView.hidden    = YES;
    _infoView.hidden    = NO;
}

-(IBAction)drivingLicenceClicked:(id)sender{
    
    if (_btndrivingLice.selected) {
        
        _btndrivingLice.selected = NO;
    }else{
        
        _btndrivingLice.selected = YES;
    }
}

- (IBAction)buttonPickerDone:(id)sender {

    [self.view endEditing:YES];
}

-(IBAction)datePickerDone:(id)sender{
    
    [self.view endEditing:YES];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    
    NSString *formatedDate = [dateFormatter stringFromDate:self.datePickerView.date];
    _txtBirthday.text = formatedDate;
}

-(IBAction)savePersonnelData:(id)sender{
    
    [self.view endEditing:YES];
    NSUserDefaults *defaults            = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedArray  = [defaults objectForKey:@"userObject"];
    
    User *user =  (User *)[NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
    
    NSMutableDictionary *params    = [[NSMutableDictionary alloc]init];
    [params setObject:user.userid forKey:@"user_id"];
    [params setObject:user.token forKey:@"token"];
    [params setObject:_txtName.text forKey:@"name"];
    [params setObject:_txtEmail.text forKey:@"email"];
    
    //[params setObject:_txtAddress.text forKey:@"address"];
    [params setObject:_txtStreet.text forKey:@"address_co"];
    [params setObject:_txtZip.text forKey:@"zip_code"];
    [params setObject:_txtCity.text forKey:@"city"];
    
    
    if ([_btnPitch.titleLabel.text isEqualToString:NSLocalizedString(@"Enter Pitch Text", nil)]) {
        
        [params setObject:@"" forKey:@"pitch"];
        
    }else{
        [params setObject:_btnPitch.titleLabel.text forKey:@"pitch"];
    }
    if ([_txtCountry.text length] > 0) {
        
        NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.country_name contains[cd] %@",_txtCountry.text];
        NSArray *array  = [countryArray filteredArrayUsingPredicate:bPredicate];
        NSDictionary *dict = [array objectAtIndex:0];
        
        [params setObject:[dict objectForKey:@"country_code"] forKey:@"country"];
        
    }else{
        
        [params setObject:_txtCountry.text forKey:@"country"];
    }
    
    [params setObject:_txtMobile.text forKey:@"mobile"];
    
    if ([_txtPersonNumbr.text containsString:@"*"]) {
        
        [params setObject:self.userDetails.person_number forKey:@"person_number"];
    }
    else{
        [params setObject:_txtPersonNumbr.text forKey:@"person_number"];
    }
    // [params setObject:_txtPersonNumbr.text forKey:@"person_number"];
    
    [params setObject:self.userDetails.gender forKey:@"gender"];
    
    if (_btndrivingLice.selected) {
        [params setObject:@"1" forKey:@"car_driver_license"];
    }else{
        [params setObject:@"0" forKey:@"car_driver_license"];
    }
    
    
//    if (![_txtBirthday.text isEqualToString:@""]) {
//        
//        NSString *dateString = _txtBirthday.text;
//        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
//        NSDate *dateFromString = [dateFormatter dateFromString:dateString];
//        dateFromString = [dateFormatter dateFromString:dateString];
//        
//        NSString *date;
//        NSString *month;
//        NSString *year;
//        
//        if (dateFromString) {
//            
//            NSDateFormatter *dateF1 = [[NSDateFormatter alloc] init];
//            [dateF1 setDateFormat:@"yyyy"];
//            year = [dateF1 stringFromDate:dateFromString];
//            
//            NSDateFormatter *dateF2 = [[NSDateFormatter alloc] init];
//            [dateF2 setDateFormat:@"MM"];
//            month = [dateF2 stringFromDate:dateFromString];
//            
//            NSDateFormatter *dateF3 = [[NSDateFormatter alloc] init];
//            [dateF3 setDateFormat:@"dd"];
//            date = [dateF3 stringFromDate:dateFromString];
//        }
//        else{
//            
//            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//            [dateFormatter setDateFormat:@"MM/dd"];
//            NSDate *dateFromString = [[NSDate alloc] init];
//            dateFromString = [dateFormatter dateFromString:dateString];
//            
//            year = @"";
//            
//            NSDateFormatter *dateF2 = [[NSDateFormatter alloc] init];
//            [dateF2 setDateFormat:@"MM"];
//            month = [dateF2 stringFromDate:dateFromString];
//            
//            NSDateFormatter *dateF3 = [[NSDateFormatter alloc] init];
//            [dateF3 setDateFormat:@"dd"];
//            date = [dateF3 stringFromDate:dateFromString];
//            
//        }
//
//        NSString *dateX = (date) ? date : @"";
//        NSString *monthX = (month) ? month : @"";
//        NSString *yearX = (year) ? year : @"";
//        
//         [params setObject:dateX forKey:@"day"];
//         [params setObject:monthX forKey:@"month"];
//         [params setObject:yearX forKey:@"year"];
//    }
    
    [_btnProfileSave setTitle:NSLocalizedString(@"", nil) forState:UIControlStateNormal];
    
    [_activityProfileSave startAnimating];
    [_activityProfileSave setHidden:NO];
    
    [User savePersonnelData:params completion:^(BOOL success, NSDictionary *data, NSError *error) {
        
        NSLog(@"success: %d", success);
        NSLog(@"data: %@", data);
        NSLog(@"Error: %@", error);
        [_activityProfileSave stopAnimating];
        [_activityProfileSave setHidden:YES];
        
        if (success) {
            
            [_btnProfileSave setTitle:NSLocalizedString(@"SAVED", nil) forState:UIControlStateNormal];
            _btnProfileSave.titleLabel.font = [UIFont fontWithName:BOLD_FONT_NAME size:17];
            fromPitchPopup = NO;
            
            if (![_txtPersonNumbr.text containsString:@"*"]) {
                
                self.userDetails.person_number = _txtPersonNumbr.text;
            }
            
            [self personalNumberTrim];
            
        }
        else{
            [_btnProfileSave setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateNormal];
            _btnProfileSave.titleLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:17];
            
            if ([[data valueForKey:@"status"] integerValue] == 405) {
                NSString *error = NSLocalizedString(@"This email already exist in the system.", nil);
                [self showAlert:error];
            } else if ([data valueForKey:@"message"]) {
                NSString *status = [NSString stringWithFormat:@"%@",[data valueForKey:@"message"]];
                [self showAlert:status];
                
            }else{
                [self showAlert:NSLocalizedString(@"Could not connect to the server. Please try again", nil)];
            }
        }
    }];
}

-(IBAction)saveBankData:(id)sender{
    
    [self.view endEditing:YES];
    
    NSUserDefaults *defaults            = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedArray  = [defaults objectForKey:@"userObject"];
    
    User *user =  (User *)[NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
    
    NSMutableDictionary *params    = [[NSMutableDictionary alloc]init];
    [params setObject:user.userid forKey:@"user_id"];
    [params setObject:user.token forKey:@"token"];
    [params setObject:_txtBankname.text forKey:@"bank_name"];
    
    if ([_txtBankAccNo.text containsString:@"*"]) {
        
        [params setObject:self.userDetails.bankAccountNo forKey:@"account"];
        
    }else{
        
        [params setObject:_txtBankAccNo.text forKey:@"account"];
    }
    [params setObject:_txtBankClearingNo.text forKey:@"clearing_number"];
    [_btnBankSave setTitle:NSLocalizedString(@"", nil) forState:UIControlStateNormal];
    
    [_activityBankSave startAnimating];
    [_activityBankSave setHidden:NO];
    
    [User saveBanklData:params completion:^(BOOL success, NSDictionary *data, NSError *error) {
        
        NSLog(@"success: %d", success);
        NSLog(@"data: %@", data);
        NSLog(@"Error: %@", error);
        
        [_activityBankSave stopAnimating];
        [_activityBankSave setHidden:YES];
        
        if (success) {
            
            if (![_txtBankAccNo.text containsString:@"*"]) {
                
                self.userDetails.bankAccountNo = _txtBankAccNo.text;
            }
            
            [self bankAccountTrim];
            [_btnBankSave setTitle:NSLocalizedString(@"SAVED", nil) forState:UIControlStateNormal];
            _btnBankSave.titleLabel.font = [UIFont fontWithName:BOLD_FONT_NAME size:17];
        }
        else{
            [_btnBankSave setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateNormal];
            _btnBankSave.titleLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:17];
            
            if ([data valueForKey:@"message"]) {
                NSString *status = [NSString stringWithFormat:@"%@",[data valueForKey:@"message"]];
                [self showAlert:status];
                
            }else{
                [self showAlert:NSLocalizedString(@"Could not connect to the server. Please try again", nil)];
            }
        }
    }];
    
}
-(IBAction)termsClicked:(id)sender{
    
    [self.navigationItem.backBarButtonItem setTitle:NSLocalizedString(@"Info", nil)];
    [self performSegueWithIdentifier:@"segueTerms" sender:self];
    
}

-(IBAction)aboutClicked:(id)sender{
    
    [self.navigationItem.backBarButtonItem setTitle:NSLocalizedString(@"Info", nil)];
    [self performSegueWithIdentifier:@"segueAbout" sender:self];
}


-(IBAction)intranetClicked:(id)sender{
    
    [self.navigationItem.backBarButtonItem setTitle:NSLocalizedString(@"Info", nil)];
    [self performSegueWithIdentifier:@"segueIntranet" sender:self];
}

- (IBAction)countryClicked:(id)sender {
    
    self.tabBarController.definesPresentationContext = YES;
    CountrySelectViewController *countryVC = [[CountrySelectViewController alloc] initWithNibName:@"CountrySelectViewController" bundle:nil];
    countryVC.delegate = self;
    countryVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self.tabBarController presentViewController:countryVC animated:YES completion:nil];
    
}

- (void)CountrySelectViewController:(CountrySelectViewController *)countrySelectViewController
                 didSelecectCountry:(NSDictionary *)country {
    
    [[NSUserDefaults standardUserDefaults] setValue:country[@"code"] forKey:kCountryCode];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.countryButton setTitle:country[@"name"] forState:UIControlStateNormal];
}

-(IBAction)signoutClicked:(id)sender{
    
    NSUserDefaults *defaults            = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedArray  = [defaults objectForKey:@"userObject"];
    
    User *user =  (User *)[NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
    
    NSMutableDictionary *params    = [[NSMutableDictionary alloc]init];
    [params setObject:user.userid forKey:@"user_id"];
    
    [User logoutWithUserID:params completion:^(BOOL success, NSDictionary *data, NSError *error) {
        
        NSLog(@"success: %d", success);
        NSLog(@"data: %@", data);
        NSLog(@"Error: %@", error);
        
    }];
    
    [defaults removeObjectForKey:@"userObject"];
    [defaults synchronize];
    
    UIStoryboard *storyboard        = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UINavigationController *login  = [storyboard instantiateViewControllerWithIdentifier:@"Loging_flow"];
    
    AppDelegate *appDelegate                = [[UIApplication sharedApplication] delegate];
    appDelegate.window.rootViewController   = login;
    [appDelegate.window makeKeyAndVisible];
}


-(IBAction)selectProfilePic:(id)sender{
    NSUserDefaults *defaults    = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedArray = [defaults objectForKey:@"userObject"];
    if (dataRepresentingSavedArray != nil)
    {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@""
                                                                 delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil
                                                        otherButtonTitles:NSLocalizedString(@"Take Photo With Camera",nil), NSLocalizedString(@"Select Photo From Library",nil), NSLocalizedString(@"Cancel",nil), nil];
        actionSheet.actionSheetStyle = UIActionSheetStyleAutomatic;
        actionSheet.destructiveButtonIndex = 1;
        [actionSheet showInView:self.view];

    }
    
    
}

-(IBAction)pitchEnterClicked:(id)sender{
    
    [self.navigationItem.backBarButtonItem setTitle:NSLocalizedString(@"Profile", nil)];
    [self performSegueWithIdentifier:@"seguePitchEnter" sender:self];
}

-(IBAction)fblogingClicked:(id)sender{
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    [login
     logInWithReadPermissions: @[@"public_profile",@"user_birthday",@"email"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error %@",error.description);
         } else if (result.isCancelled) {
             NSLog(@"%@",result);
             [self showAlert:NSLocalizedString(@"Couldn’t connect to Facebook. Please try again.", nil)];
         } else {
             NSLog(@"%@",result);
             NSLog(@"Logged in");
             [self getDetailsAndLogin];
         }
     }];
}

#pragma mark loadData

-(void)loadPersonnelInfo{
    
    NSUserDefaults *defaults    = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedArray = [defaults objectForKey:@"userObject"];
    if (dataRepresentingSavedArray != nil)
    {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
         User *user =  (User *)[NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
    
        [User getPersonnelInfoForUserID:user.userid token:user.token completion:^(BOOL success, NSDictionary *data, NSError *error) {
            NSLog(@"success: %d", success);
            NSLog(@"data: %@", data);
            NSLog(@"Error: %@", error);
            
            if (success) {
                [self passData:[data objectForKey:@"data"]];
                [self loadBankInfoForUser:user];
            }
            else{
                
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                
                
                if ([data valueForKey:@"message"]) {
                    NSString *status = [NSString stringWithFormat:@"%@",[data valueForKey:@"message"]];
                    [self showAlert:status];
                    
                }else{
                    [self showAlert:NSLocalizedString(@"Could not connect to the server. Please try again", nil)];
                }
            }
            
        }];
        
    }
    
}

-(void)loadBankInfoForUser:(User *)user{
    
    [User getAccountInfoForUserID:user.userid token:user.token completion:^(BOOL success, NSDictionary *data, NSError *error) {
        NSLog(@"success: %d", success);
        NSLog(@"data: %@", data);
        NSLog(@"Error: %@", error);
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if (success) {
            
            [self passBankData:[data objectForKey:@"data"]];
            
        }
        else{
            
            if ([data valueForKey:@"message"]) {
                NSString *status = [NSString stringWithFormat:@"%@",[data valueForKey:@"message"]];
                [self showAlert:status];
                
            }else{
                [self showAlert:NSLocalizedString(@"Could not connect to the server. Please try again", nil)];
            }
        }
        
    }];
}

-(void)passData:(NSDictionary *)data{
    
    [self loadProfilePic:[data objectForKeyNotNull:@"image"]];
    
    if ([data objectForKeyNotNull:@"name"]) {
        _txtName.text       = [data objectForKeyNotNull:@"name"];
        _lblUsername.text   = [data objectForKeyNotNull:@"name"];
        self.userDetails.name = [data objectForKeyNotNull:@"name"];
    }
    if ([data objectForKeyNotNull:@"email"]) {
        _txtEmail.text   = [data objectForKeyNotNull:@"email"];
        _lblEmail.text = [data objectForKeyNotNull:@"email"];
        self.userDetails.email = [data objectForKeyNotNull:@"email"];
    }
    if ([data objectForKeyNotNull:@"pitch"]) {
        if ([[data objectForKeyNotNull:@"pitch"] isEqualToString:@""]) {
            
            [_btnPitch setTitle:NSLocalizedString(@"Enter Pitch Text", nil) forState:UIControlStateNormal];
            [_btnPitch setTitleColor:[UIColor colorWithRed:204.0f/255.0f green:204.0f/255.0f blue:204.0f/255.0f alpha:1.0] forState:UIControlStateNormal];
            _btnPitch.titleLabel.textAlignment = NSTextAlignmentCenter;


        }else{
            [_btnPitch setTitle:[data objectForKeyNotNull:@"pitch"] forState:UIControlStateNormal];
            [_btnPitch setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            _btnPitch.titleLabel.textAlignment = NSTextAlignmentCenter;

            //[self adjustFrames];
        }
        

    }
    if ([data objectForKeyNotNull:@"address"]) {
        _txtAddress.text = [data objectForKeyNotNull:@"address"];
    }
    if ([data objectForKeyNotNull:@"address_co"]) {
        _txtStreet.text = [data objectForKeyNotNull:@"address_co"];
    }
    if ([data objectForKeyNotNull:@"zip_code"]) {
        _txtZip.text = [data objectForKeyNotNull:@"zip_code"];
    }
    if ([data objectForKeyNotNull:@"city"]) {
        _txtCity.text = [data objectForKeyNotNull:@"city"];
    }
    if (![[data objectForKeyNotNull:@"country"] isEqualToString:@""] && [data objectForKeyNotNull:@"country"]) {
        
        NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.country_code contains[cd] %@",[data objectForKeyNotNull:@"country"]];
        NSArray *array  = [countryArray filteredArrayUsingPredicate:bPredicate];
        NSDictionary *dict = [array objectAtIndex:0];
        
        _txtCountry.text = [dict objectForKey:@"country_name"];
    }
    if ([data objectForKeyNotNull:@"mobile"]) {
        _txtMobile.text = [data objectForKeyNotNull:@"mobile"];
    }
    
//    
//    NSString *date;
//    NSString *month;
//    NSString *year;
//    
//    
//    if (![[data objectForKeyNotNull:@"year"] isEqualToString:@""] && [data objectForKeyNotNull:@"year"]) {
//        
//        year = [data objectForKeyNotNull:@"year"];
//        month = [data objectForKeyNotNull:@"month"];
//        date  = [data objectForKeyNotNull:@"day"];
//
//        NSString *dateString = [NSString stringWithFormat:@"%@/%@/%@",month,date,year];
//        _txtBirthday.text = dateString;
//        
//        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
//        NSDate *dateFromString = [[NSDate alloc] init];
//        dateFromString = [dateFormatter dateFromString:dateString];
//        [self.datePickerView setDate:dateFromString];
//    }
//    else{
//        
//        if(![[data objectForKeyNotNull:@"month"] isEqualToString:@""] && [data objectForKeyNotNull:@"month"]){
//            
//            month = [data objectForKeyNotNull:@"month"];
//            date  = [data objectForKeyNotNull:@"day"];
//            
//            NSString *dateString = [NSString stringWithFormat:@"%@/%@",month,date];
//            _txtBirthday.text = dateString;
//            
//            NSString *dummyDate = [NSString stringWithFormat:@"%@/%@/%@",month,date,@"1900"];
//            
//            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//            [dateFormatter setDateFormat:@"MM/dd/yyyy"];
//            NSDate *dateFromString = [[NSDate alloc] init];
//            dateFromString = [dateFormatter dateFromString:dummyDate];
//            [self.datePickerView setDate:dateFromString];
//            
//        }
//        else{
//            
//            NSString *dummyDate = @"01/01/1900";
//            
//            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//            [dateFormatter setDateFormat:@"MM/dd/yyyy"];
//            NSDate *dateFromString = [[NSDate alloc] init];
//            dateFromString = [dateFormatter dateFromString:dummyDate];
//            [self.datePickerView setDate:dateFromString];
//            
//        }
//    }
    
  
    
    if ([data objectForKeyNotNull:@"person_number"]) {
        
        self.userDetails.person_number = [data objectForKeyNotNull:@"person_number"];
        
        if ([[data objectForKeyNotNull:@"person_number"] length] > 0) {
            
        NSMutableString * str1 = [[NSMutableString alloc]initWithString:[data objectForKeyNotNull:@"person_number"]];
        [str1 replaceCharactersInRange:NSMakeRange([str1 length] - 1, 1) withString:@"*"];
        [str1 replaceCharactersInRange:NSMakeRange([str1 length] - 2, 1) withString:@"*"];
        [str1 replaceCharactersInRange:NSMakeRange([str1 length] - 3, 1) withString:@"*"];
        [str1 replaceCharactersInRange:NSMakeRange([str1 length] - 4, 1) withString:@"*"];
        
        _txtPersonNumbr.text = str1;
        
    }
       // _txtPersonNumbr.text = [data objectForKeyNotNull:@"person_number"];
        

    }
    if ([data objectForKeyNotNull:@"gender"]) {
        
        if ([[data objectForKeyNotNull:@"gender"] isEqualToString:@"M"]) {
            
            _txtGender.text = NSLocalizedString(@"Male", nil);
            self.userDetails.gender = @"M";

        }else if([[data objectForKeyNotNull:@"gender"] isEqualToString:@"F"]){
            _txtGender.text = NSLocalizedString(@"Female", nil);
            self.userDetails.gender = @"F";
            
        }else if ([[data objectForKeyNotNull:@"gender"] isEqualToString:@"K"]){
            _txtGender.text = NSLocalizedString(@"Female", nil);
            self.userDetails.gender = @"F";
        }else{
            _txtGender.text = nil;
        }
            }
    if ([[data objectForKeyNotNull:@"car_driver_license"] boolValue]) {
        _btndrivingLice.selected = YES;
    }else{
        _btndrivingLice.selected = NO;
    }
    
    //Set Rating for user
    if ([data objectForKeyNotNull:@"candidate_rating"]) {
        self.ratingView.hidden = NO;
        CGFloat rating = [[data objectForKeyNotNull:@"candidate_rating"] floatValue];
        [self.ratingView setRating:rating];
    }
}

-(void)passBankData:(NSDictionary *)data{
    
    if ([data objectForKeyNotNull:@"bank_name"]) {
        _txtBankname.text = [data objectForKeyNotNull:@"bank_name"];
    }
    if ([data objectForKeyNotNull:@"clearing_number"]) {
        _txtBankClearingNo.text = [data objectForKeyNotNull:@"clearing_number"];
    }
    if ([data objectForKeyNotNull:@"account"]) {
        
        if ([[data objectForKeyNotNull:@"account"] length] > 0) {
            
            self.userDetails.bankAccountNo = [data objectForKeyNotNull:@"account"];
            
            NSMutableString * str1 = [[NSMutableString alloc]initWithString:[data objectForKeyNotNull:@"account"]];
            [str1 replaceCharactersInRange:NSMakeRange([str1 length] - 1, 1) withString:@"*"];
            [str1 replaceCharactersInRange:NSMakeRange([str1 length] - 2, 1) withString:@"*"];
            [str1 replaceCharactersInRange:NSMakeRange([str1 length] - 3, 1) withString:@"*"];
            [str1 replaceCharactersInRange:NSMakeRange([str1 length] - 4, 1) withString:@"*"];
            
            _txtBankAccNo.text = str1;
        }
        
    }

}

-(NSString *)getCountryList{
    
    NSString *fileName = @"country";
    
    NSString * dbFile = [[NSBundle mainBundle] pathForResource:fileName ofType:@"json"];
    return [NSString stringWithContentsOfFile:dbFile encoding:NSASCIIStringEncoding error:nil];
}

-(void)loadProfilePic:(NSString *)imageURL{
    
    if ([imageURL isEqualToString:@""]) {
        
        return;
    }
    
    [_activityProfImage setHidden:NO];
    [_activityProfImage startAnimating];
    
    NSURL *url                  = [NSURL URLWithString:imageURL];
    NSURLRequest *request       = [NSURLRequest requestWithURL:url];
    UIImage *placeholderImage   = [UIImage imageNamed:@"profile_placeholder"];
    
    [_userImage setImageWithURLRequest:request
                      placeholderImage:placeholderImage
                               success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                   
                                   [_activityProfImage setHidden:YES];
                                   [_activityProfImage stopAnimating];
                                   _userImage.image = image;
                                   
                               } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                   
                                   [_activityProfImage setHidden:YES];
                                   [_activityProfImage stopAnimating];
                                   NSLog(@"response %@",response);
                                   NSLog(@"error %@",error);
                               }];
    
}

-(void)getDetailsAndLogin{
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:[NSDictionary dictionaryWithObject:@"cover,picture.type(large),id,name,first_name,last_name,gender,birthday,email,location,hometown,bio,photos" forKey:@"fields"]]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         if (!error) {
             
             NSLog(@"%@",result);
             NSString *userID = [[FBSDKAccessToken currentAccessToken] userID];
             NSString *userName = [result objectForKeyNotNull:@"name"];
             NSString *email =  [result objectForKeyNotNull:@"email"];
             NSString *userImageURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [[FBSDKAccessToken currentAccessToken] userID]];
             NSString *gender = [[result objectForKeyNotNull:@"gender"] uppercaseString];
             // NSString *DOB = [result objectForKeyNotNull:@"birthday"];
             
             [_activity startAnimating];
             [_activity setHidden:NO];
             
             NSString *pushToken;
             if ([[NSUserDefaults standardUserDefaults] stringForKey:@"pushDeviceToken"]) {
                 
                 pushToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"pushDeviceToken"];
             }else{
                 
                 pushToken = @"";
             }
             
             NSString *emails = (email) ? email : @"";
             
             //NSString *dobFB = (DOB) ? DOB : @"";
             
             NSString *dateString = [result objectForKeyNotNull:@"birthday"];
             NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
             [dateFormatter setDateFormat:@"MM/dd/yyyy"];
             NSDate *dateFromString = [dateFormatter dateFromString:dateString];
             dateFromString = [dateFormatter dateFromString:dateString];
             
             NSString *date;
             NSString *month;
             NSString *year;
             
             if (dateFromString) {
                 
                 NSDateFormatter *dateF1 = [[NSDateFormatter alloc] init];
                 [dateF1 setDateFormat:@"yyyy"];
                 year = [dateF1 stringFromDate:dateFromString];
                 
                 NSDateFormatter *dateF2 = [[NSDateFormatter alloc] init];
                 [dateF2 setDateFormat:@"MM"];
                 month = [dateF2 stringFromDate:dateFromString];
                 
                 NSDateFormatter *dateF3 = [[NSDateFormatter alloc] init];
                 [dateF3 setDateFormat:@"dd"];
                 date = [dateF3 stringFromDate:dateFromString];
             }
             else{
                 
                 NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                 [dateFormatter setDateFormat:@"MM/dd"];
                 NSDate *dateFromString = [[NSDate alloc] init];
                 dateFromString = [dateFormatter dateFromString:dateString];
                 
                 year = @"";
                 
                 NSDateFormatter *dateF2 = [[NSDateFormatter alloc] init];
                 [dateF2 setDateFormat:@"MM"];
                 month = [dateF2 stringFromDate:dateFromString];
                 
                 NSDateFormatter *dateF3 = [[NSDateFormatter alloc] init];
                 [dateF3 setDateFormat:@"dd"];
                 date = [dateF3 stringFromDate:dateFromString];
                 
             }
             
             NSString *dateX = (date) ? date : @"";
             NSString *monthX = (month) ? month : @"";
             NSString *yearX = (year) ? year : @"";
             
             NSString *genderFB;
             
             if ([gender isEqualToString:@"MALE"]) {
                 genderFB = @"M";
             }else if ([gender isEqualToString:@"FEMALE"]){
                 genderFB = @"F";
             }else{
                 genderFB = @"";
             }
             
             
             NSDictionary *params    = @{@"fbid": userID, @"name": userName, @"email":emails, @"day": dateX, @"month": monthX, @"year": yearX, @"gender": genderFB, @"accountType": [NSNumber numberWithInt:1], @"device_id":pushToken, @"device_type": @"2", @"image" :userImageURL};
             
             //             NSDictionary *params    = @{@"fbid": @"10156311001425287", @"name": @"Rasmus Solholm", @"email":@"solholmrasmus@gmail.com", @"day": dateX, @"month": monthX, @"year": yearX, @"gender": genderFB, @"accountType": [NSNumber numberWithInt:1], @"device_id":pushToken, @"device_type": @"2", @"image" :userImageURL};
             
             [User loginWithFacebookid:params completion:^(BOOL success, NSDictionary *data, NSError *error) {
                 
//                 [_activity stopAnimating];
//                 [_activity setHidden:YES];
                 
                 [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
                 
                 NSLog(@"success: %d", success);
                 NSLog(@"data: %@", data);
                 NSLog(@"Error: %@", error);
                 
                 if (success) {
                     
                     User *user         = [[User alloc] init];
                     user.userid        = [NSString stringWithFormat:@"%@",[data valueForKey:@"user_id"]];
                     user.token         = [NSString stringWithFormat:@"%@",[data valueForKey:@"token"]];
                     user.email         = email;
                     user.name          = userName;
                     user.userImageURL  = userImageURL;
                     //user.password    = password;
                     
                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                     [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:user] forKey:@"userObject"];
                     [defaults synchronize];
                     
                     [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                     
                     [self viewWillAppear:NO];
                 }
                 
                 else{
                     
                     if ([data valueForKey:@"message"]) {
                         NSString *status = [NSString stringWithFormat:@"%@",[data valueForKey:@"message"]];
                         [self showAlert:status];
                         
                     }else{
                         [self showAlert:NSLocalizedString(@"Could not connect to the server. Please try again", nil)];
                     }
                 }
                 
             }];
         }
         else{
             
             NSLog(@"%@",error.localizedDescription);
             [self showAlert:error.localizedDescription];
         }
     }];
    
}

-(void)loadInitialView{
    
    NSUserDefaults *defaults    = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedArray = [defaults objectForKey:@"userObject"];
    if (dataRepresentingSavedArray != nil)
    {
        _profileView.hidden = NO;
        _bankView.hidden    = YES;
        _infoView.hidden    = YES;
        _loginScroll.hidden   = YES;
        
    }else{
        _loginScroll.hidden   = NO;
        _profileView.hidden = YES;
        _bankView.hidden    = YES;
        _infoView.hidden    = YES;
        
    }
    
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    UIDeviceResolution valueDevice = [[UIDevice currentDevice] currentResolution];
    
    if (valueDevice == 2)
    {
        //iphone 4 & 4S
        _loginView.center = self.view.center;
    }
    else if (valueDevice == 4)
    {
        //ipad 2
        _loginView.center = self.view.center;
    }
    else if (valueDevice == 5)
    {
        //ipad 3 - retina display
        _loginView.center = self.view.center;
    }
    
    [self setScrollview];

}

#pragma mark Helpers

-(void)personalNumberTrim{
    
    if ([_txtPersonNumbr.text length] > 0) {
        
        NSMutableString * str1 = [[NSMutableString alloc]initWithString:_txtPersonNumbr.text];
        [str1 replaceCharactersInRange:NSMakeRange([str1 length] - 1, 1) withString:@"*"];
        [str1 replaceCharactersInRange:NSMakeRange([str1 length] - 2, 1) withString:@"*"];
        [str1 replaceCharactersInRange:NSMakeRange([str1 length] - 3, 1) withString:@"*"];
        [str1 replaceCharactersInRange:NSMakeRange([str1 length] - 4, 1) withString:@"*"];
        
        _txtPersonNumbr.text = str1;
    }
    
}

-(void)bankAccountTrim{
    
    if ([_txtBankAccNo.text length] > 0) {
        
        NSMutableString * str1 = [[NSMutableString alloc]initWithString:_txtBankAccNo.text];
        [str1 replaceCharactersInRange:NSMakeRange([str1 length] - 1, 1) withString:@"*"];
        [str1 replaceCharactersInRange:NSMakeRange([str1 length] - 2, 1) withString:@"*"];
        [str1 replaceCharactersInRange:NSMakeRange([str1 length] - 3, 1) withString:@"*"];
        [str1 replaceCharactersInRange:NSMakeRange([str1 length] - 4, 1) withString:@"*"];
        
        _txtBankAccNo.text = str1;
    }
    
    
}

-(void)setPitchText:(NSString *)textPitch{
    
    fromPitchPopup = YES;
    
    if ([textPitch isEqualToString:NSLocalizedString(@"Enter Pitch Text", nil)]) {
        
        [_btnPitch setTitle:NSLocalizedString(@"Enter Pitch Text", nil) forState:UIControlStateNormal];
        [_btnPitch setTitleColor:[UIColor colorWithRed:204.0f/255.0f green:204.0f/255.0f blue:204.0f/255.0f alpha:1.0] forState:UIControlStateNormal];
        _btnPitch.titleLabel.textAlignment = NSTextAlignmentCenter;
        
    }else{
        [_btnPitch setTitle:textPitch forState:UIControlStateNormal];
        [_btnPitch setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        _btnPitch.titleLabel.textAlignment = NSTextAlignmentCenter;
        
    }
    
}

#pragma mark imageUpload

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        [self TakePhotoWithCamera];
    }
    else if (buttonIndex == 1)
    {
        [self selectPhotoFromLibrary];
    }
    
    else if (buttonIndex == 2)
    {
        NSLog(@"cancel");
    }
}

-(void) TakePhotoWithCamera
{
    ipc = [[UIImagePickerController alloc] init];
    ipc.delegate = self;
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:ipc animated:YES completion:NULL];
        
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No Camera Available." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        alert = nil;
    }
   

}


-(void) selectPhotoFromLibrary
{
    ipc= [[UIImagePickerController alloc] init];
    ipc.delegate = self;
    ipc.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    [self presentViewController:ipc animated:YES completion:nil];
    
}

#pragma mark - ImagePickerController Delegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    fromCameraPopup = YES;
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        [picker dismissViewControllerAnimated:YES completion:nil];
    } else {
        [popover dismissPopoverAnimated:YES];
    }
    
    UIImage *currentImage = info[UIImagePickerControllerOriginalImage];
    UIImage *resized = [currentImage resizedImageWithContentMode:UIViewContentModeScaleAspectFill bounds:CGSizeMake(500, 500) interpolationQuality:kCGInterpolationLow];
    
    _userImage.image = resized;
    
    [self uploadUserImage:resized];
    
   // [_userImage setImage:[info objectForKey:UIImagePickerControllerOriginalImage] forState:UIControlStateNormal];

}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    fromCameraPopup = YES;
    [picker dismissViewControllerAnimated:YES completion:nil];
}


-(void)uploadUserImage:(UIImage *)userImage
{

    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSUserDefaults *defaults    = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedArray = [defaults objectForKey:@"userObject"];
    if (dataRepresentingSavedArray != nil)
    {
        User *user =  (User *)[NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
        
        NSString *url = [NSString stringWithFormat:@"%@user/upload_image", API_URL];
        NSLog(@"NN:URL: %@", url);
        

        
        NSData *imageData = UIImageJPEGRepresentation(userImage,1.0);
        NSDictionary *params = @{@"user_id": user.userid};
        
        
        NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            [formData appendPartWithFileData:imageData name:@"image_data" fileName:@"fileName.jpg" mimeType:@"image/jpeg"];
        } error:nil];
        
        AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
        
        [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [HTTPClient sharedClient].requestSerializer = requestSerializer;
        [HTTPClient sharedClient].responseSerializer.acceptableContentTypes = [[HTTPClient sharedClient].responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        NSURLSessionUploadTask *uploadTask;
        uploadTask = [manager
                      uploadTaskWithStreamedRequest:request
                      progress:^(NSProgress * _Nonnull uploadProgress) {
                          // This is not called back on the main queue.
                          // You are responsible for dispatching to the main queue for UI updates
                          dispatch_async(dispatch_get_main_queue(), ^{
                              //Update the progress view
//                              [progressView setProgress:uploadProgress.fractionCompleted];
                              NSLog(@"Total Conut: %lld / Progress :%f", uploadProgress.totalUnitCount, uploadProgress.fractionCompleted);
                          });
                      }
                      completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                          if (error) {
                              NSLog(@"Error: %@", error);
                              fromCameraPopup = NO;
                              [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                              
                              [self showAlert:@"Profile image upload failed."];
                          } else {
                              NSLog(@"%@ %@", response, responseObject);
                              fromCameraPopup = NO;
                              [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                          }
                      }];
        
        [uploadTask resume];
        
      }
    
}

#pragma mark - Textfield Delegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if (textField == self.txtGender){
        
        selectedPicker = @"Gender";
        self.pickerView.hidden = false;
        pickerArray = @[NSLocalizedString(@"Male", nil),NSLocalizedString(@"Female", nil)];
        if ([pickerArray containsObject:textField.text]) {
            [self.pickerView selectRow:[pickerArray indexOfObject:textField.text] inComponent:0 animated:NO];
        }
    }else if  (textField == self.txtCountry){
        selectedPicker = @"country";
        self.pickerView.hidden = false;
        pickerArray = [[NSArray alloc]initWithArray:countryArray];
        NSArray *arrayWithCountry = [pickerArray valueForKey:@"country_name"];
        if ([arrayWithCountry containsObject:textField.text]) {
            [self.pickerView selectRow:[arrayWithCountry indexOfObject:textField.text] inComponent:0 animated:NO];
        }
    } else if (textField == self.txtBankAccNo) {
        textField.text = self.userDetails.bankAccountNo;
    } else if (textField == self.txtPersonNumbr) {
        textField.text = self.userDetails.person_number;
    }
//    else if (textField == self.txtBirthday){
//        self.datePickerView.hidden = false;
//    }
    
    return true;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {

    if (textField == _txtBankAccNo) {
        self.userDetails.bankAccountNo = textField.text;
        [self bankAccountTrim];
    } else if (textField == self.txtPersonNumbr) {
        self.userDetails.person_number = textField.text;
    }
}

#pragma mark UITextview Delegates

-(BOOL)textView:(UITextView *)_textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    //[self adjustFrames];
    
    return YES;
}

-(void) adjustFrames
{
//    CGRect textFrame = _txtPitch.frame;
//    textFrame.size.height = [self textViewHeightForAttributedText];
//    self.txtViewbottomContraint.constant = bottomContraint + [self textViewHeightForAttributedText] - 30;
//    [self.view layoutIfNeeded];
//    _txtPitch.frame = textFrame;


}

//- (CGFloat)textViewHeightForAttributedText{
//    
//    NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:_txtPitch.text];
//    [hogan addAttribute:NSFontAttributeName
//                  value:[UIFont fontWithName:DEFAULT_FONT_NAME size:14.0]
//                  range:NSMakeRange(0, [_txtPitch.text length])];
//  
//    UITextView *calculationView = [[UITextView alloc] init];
//    [calculationView setAttributedText:hogan];
//    
//    CGSize size = [calculationView sizeThatFits:CGSizeMake(_txtPitch.frame.size.width, FLT_MAX)];
//    
//    int rows = size.height/30;
//
////    if (rows > 3) {
////        
////        return 3 * 30;
////    }
//    return size.height;
//}

//- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
//{
//    if ([_txtPitch.text isEqualToString:@"Enter Pitch Text"]) {
//        _txtPitch.textAlignment = NSTextAlignmentCenter;
//        _txtPitch.text = @"";
//        _txtPitch.textColor = [UIColor darkGrayColor];
//        return YES;
//    }
//   return YES;
//}
//
//-(void)textViewDidChange:(UITextView *)textView
//{
//    
//    if(_txtPitch.text.length == 0){
//        
//        _txtPitch.textAlignment = NSTextAlignmentCenter;
//        _txtPitch.textColor = [UIColor lightGrayColor];
//        _txtPitch.text = @"Enter Pitch Text";
//        [_txtPitch resignFirstResponder];
//    }
//}

-(void)showAlert:(NSString *)message{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             [self dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    [alert addAction:ok];
    
}


#pragma mark - PickerView Delegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if ([selectedPicker isEqualToString:@"Gender"]) {
        self.txtGender.text = pickerArray[row];
        if (row == 0) {
            self.userDetails.gender = @"M";
        } else if (row == 1) {
            self.userDetails.gender = @"F";
        }
    }else{
        NSDictionary *dic = pickerArray[row];
        selectedCountry = [dic objectForKeyNotNull:@"country_code"];
        self.txtCountry.text = [dic objectForKeyNotNull:@"country_name"];
    }
    
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return pickerArray.count;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if ([selectedPicker isEqualToString:@"Gender"]) {
        
        return pickerArray[row];
        
    }else{
        NSDictionary *dic = pickerArray[row];
        return [dic objectForKeyNotNull:@"country_name"];
    }
    
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([[segue identifier] isEqualToString:@"seguePitchEnter"]){
        
        EnterPitchViewController *detailController   = (EnterPitchViewController *)[segue destinationViewController];
        detailController.pitchDelegate               = self;
        detailController.pitchText                   = _btnPitch.titleLabel.text;
        detailController.placeholder = NSLocalizedString(@"Enter Pitch Text", nil);
    }
    else if ([[segue identifier] isEqualToString:@"segueSignupFromProfile"]){
        
        UINavigationController *navcontroller = (UINavigationController *)[segue destinationViewController];
        SignupViewController *signupVC   = [navcontroller.viewControllers objectAtIndex:0];
        signupVC.isFromTabbar            = YES;
    }
    else if ([[segue identifier] isEqualToString:@"segueSigninFromProfile"]){
        
        UINavigationController *navcontroller = (UINavigationController *)[segue destinationViewController];
        SigninViewController *signinVC   = [navcontroller.viewControllers objectAtIndex:0];
        signinVC.isFromTabbar            = YES;
    }
    
    
}


@end
