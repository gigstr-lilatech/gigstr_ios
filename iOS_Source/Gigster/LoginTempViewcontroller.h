//
//  LoginTempViewcontroller.h
//  Gigster
//
//  Created by EFutures on 3/18/16.
//  Copyright © 2016 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>


@class LoginTempViewcontroller;
@protocol LoginTempViewcontrollerDelegate<NSObject>

- (void)LoginTempViewcontroller:(LoginTempViewcontroller *)loginTempViewcontroller
   dismissedWithSuccess:(BOOL)success;

@end

@interface LoginTempViewcontroller : UIViewController

@property(assign)BOOL isFromApply;
@property (nonatomic, weak) id <LoginTempViewcontrollerDelegate> delegate;

@end


