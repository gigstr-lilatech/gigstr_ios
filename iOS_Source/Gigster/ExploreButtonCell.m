//
//  ExploreButtonCell.m
//  Gigster
//
//  Created by Rakitha Perera on 8/9/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import "ExploreButtonCell.h"

@implementation ExploreButtonCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
