//
//  LocationManager.h
//  Gigster
//
//  Created by Rakitha Perera on 2/26/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface LocationManager : NSObject <CLLocationManagerDelegate> {
    
}

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) NSDate *lastTimestamp;
@property (strong, nonatomic) CLLocation *currentLocation;
@property (nonatomic) CLAuthorizationStatus locationStatus;

+ (instancetype)sharedInstance;

- (void)startUpdatingLocation;
- (void)stopUpdatingLocation;
- (BOOL)isLocationAvilableForVC:(UIViewController *)viewController;

@end
