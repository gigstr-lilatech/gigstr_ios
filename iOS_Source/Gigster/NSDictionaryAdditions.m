//
//  NSDictionaryAdditions.m
//  Lalinks
//
//  Created by Jayampathy Balasuriya on 4/28/14.
//  Copyright (c) 2014 Elephanti Pte. Ltd. All rights reserved.
//

#import "NSDictionaryAdditions.h"

@implementation NSDictionary (NSDictionaryAdditions)

// in case of [NSNull null] values a nil is returned ...
- (id)objectForKeyNotNull:(id)key {
    id object = [self objectForKey:key];
    if (object == [NSNull null])
        return nil;
    
    return object;
}

-(void)addObject:(id)object forKey:(id)key{
    if(!key)
        return;
    
    if(object)
        [self setValue:object forKey:key];
    else
        [self setValue:@"" forKey:key];

    
}

-(void)addValue:(id)value forKey:(id)key{
    if(value)
        [self setValue:value forKey:key];
    else
        [self setValue:@"" forKey:key];

}
@end
