//
//  WebViewController.h
//  Gigster
//
//  Created by EFutures on 1/6/16.
//  Copyright © 2016 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface WebViewController : BaseViewController

@property(nonatomic,weak)IBOutlet UIWebView *webView;

@property(nonatomic,strong)NSURL *webURL;
@property BOOL isBoundsAdded;

@end
