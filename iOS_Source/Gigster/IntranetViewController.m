//
//  IntranetViewController.m
//  Gigster
//
//  Created by EFutures on 1/13/16.
//  Copyright © 2016 EFutures. All rights reserved.
//

#import "IntranetViewController.h"

@interface IntranetViewController ()

@property(nonatomic,weak)IBOutlet UIWebView *webView;
@property(nonatomic,weak)IBOutlet UIActivityIndicatorView *activity;


@end

@implementation IntranetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [_activity setHidden:NO];
    [_activity startAnimating];
    NSString *urlString = [NSString stringWithFormat:@"http://www.gogigstr.com/intra"];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [_activity setHidden:YES];
    [_activity stopAnimating];
    NSLog(@"Current URL = %@",webView.request.URL);
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
