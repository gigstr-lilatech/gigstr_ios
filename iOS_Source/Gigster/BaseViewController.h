//
//  BaseViewController.h
//  Gigster
//
//  Created by EFutures on 11/27/15.
//  Copyright © 2015 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

-(void)setupBackButton:(NSString *)title;

@end
