//
//  ForgotPasswordViewController.m
//  Gigster
//
//  Created by EFutures on 11/12/15.
//  Copyright © 2015 EFutures. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "User.h"
#import "Utility.h"

@interface ForgotPasswordViewController ()

@property(nonatomic,weak)IBOutlet UITextField *txtEmail;
@property(nonatomic,weak)IBOutlet UIButton *btnReset;
@property(nonatomic,weak)IBOutlet UILabel *lblText;

@property(nonatomic,weak)IBOutlet UIActivityIndicatorView *activity;

@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setTextfieldLayers:_txtEmail];
    
    [self setPlaceholderColor:_txtEmail];
    [self setButtonLayers:_btnReset];
    
    _activity.hidden = YES;
    _btnReset.enabled = NO;
    
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self setupBackButton:@"Back"];
    [self.navigationController setNavigationBarHidden:NO animated:animated];    
}

-(void)setTextfieldLayers:(UITextField *)textFld{
    
    [[textFld layer] setCornerRadius:5.0f];
    [[textFld layer] setMasksToBounds:YES];
    [[textFld layer] setBorderWidth:1.0f];
    [[textFld layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    
}

-(void)setButtonLayers:(UIButton *)button{
    
    [[button layer] setCornerRadius:5.0f];
    [[button layer] setMasksToBounds:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)resetClicked:(id)sender{
    
    [self.view endEditing:YES];
    NSString *email = _txtEmail.text;
    if (![Utility validateTextField:self.txtEmail]) {
        
        [self showAlert:@"Please enter your email address!" withTag:@"error"];
        
        return;
    }
    
    if (![Utility isValidEmail:email]) {
        
        [self showAlert:@"Please enter valid email!" withTag:@"error"];
        
        return;
    }
    
    _activity.hidden = NO;
    [_activity startAnimating];
    [self.view setUserInteractionEnabled:NO];
    
    [User resetPasswordwithEmail:email completion:^(BOOL success, NSDictionary *data, NSError *error) {
        
        [_activity stopAnimating];
        [_activity setHidden:YES];
        [self.view setUserInteractionEnabled:YES];
        
        NSLog(@"success: %d", success);
        NSLog(@"data: %@", data);
        NSLog(@"Error: %@", error);
        
        if (success) {
            
            _lblText.textColor = [UIColor yellowColor];
            _lblText.text = @"Password is reset. Please check your inbox";
            // [self showAlert:@"Password is reset. Please check your inbox" withTag:@"success"];
            

        }else{
            
            if ([data valueForKey:@"message"]) {
                NSString *status = [NSString stringWithFormat:@"%@",[data valueForKey:@"message"]];
                [self showAlert:status withTag:@""];
                
            }else{
                [self showAlert:NSLocalizedString(@"Could not connect to the server. Please try again", nil) withTag:@""];
            }
        }

        
    }];
}

-(void)showAlert:(NSString *)message withTag:(NSString *)tag{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             if ([tag isEqualToString:@"success"]) {
                                 
                                 [self performSelector:@selector(popBack) withObject:nil afterDelay:0.1];
                                 return ;
                             }
                             //Do some thing here
                             _txtEmail.text = @"";
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    [alert addAction:ok];
    
}

-(void)popBack{
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self validateForButton:newString];
    return YES;
}

-(void)validateForButton:(NSString *)text{
    
    if ([Utility isValidEmail:text]) {
        _btnReset.enabled = YES;
    }else{
        _btnReset.enabled = NO;
    }
}

-(void)setPlaceholderColor:(UITextField *)textField{
    
    if ([textField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor colorWithRed:246.0f/255.0f green:246.0f/255.0f blue:246.0f/255.0f alpha:1.0];
        textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
    } else {
        NSLog(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
        // TODO: Add fall-back code to set placeholder color.
    }
    
}

-(void)setupBackButton:(NSString *)title{
    
    UIImage* image = [UIImage imageNamed:@"back_arrow"];
    CGRect frame = CGRectMake(0, 0, 100, 50);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frame];
    [backButton setImage:image forState:UIControlStateNormal];
    [backButton setTitle:NSLocalizedString(title, nil) forState:UIControlStateNormal];
    CGFloat insetAmount = 4 / 2.0;
    backButton.imageEdgeInsets = UIEdgeInsetsMake(0, -insetAmount, 0, insetAmount);
    backButton.titleEdgeInsets = UIEdgeInsetsMake(0, insetAmount, 0, -insetAmount);
    backButton.contentEdgeInsets = UIEdgeInsetsMake(0, insetAmount - 50 , 0, insetAmount);
    [backButton addTarget:self action:@selector(popBack) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem* backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backButtonItem;
    
    self.navigationItem.titleView = nil;
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
