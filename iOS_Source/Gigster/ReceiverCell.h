//
//  ReceiverCell.h
//  Gigster
//
//  Created by EFutures on 12/13/15.
//  Copyright © 2015 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSWTappableLabel.h"
@interface ReceiverCell : UITableViewCell

@property(nonatomic,weak)IBOutlet ZSWTappableLabel *message;
@property(nonatomic,weak)IBOutlet UILabel *time;
@property(nonatomic,weak)IBOutlet UIImageView *receiverImage;

-(void)resizeHeight:(CGRect)rect;

@end
