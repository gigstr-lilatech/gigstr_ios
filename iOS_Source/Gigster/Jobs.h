//
//  Jobs.h
//  Gigster
//
//  Created by EFutures on 11/16/15.
//  Copyright © 2015 EFutures. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface Jobs : NSObject

@property NSString* jobID;
@property NSString* jobTitle;
@property NSString* jobDescription;
@property NSString* jobDescriptionLink;
@property NSString* jobSubtitle1;
@property NSString* jobSubtitle2;
@property BOOL      gigActive;
@property BOOL      applied;
@property NSString* companyID;
@property NSString* jobImageUrl;
@property UIImage * jobImage;



+ (void) listAllGigsforUserId:(NSString *) userID token:(NSString *) token completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion;

+ (void) listGig:(NSString *) userID token:(NSString *) token gigID:(NSString *) gigID completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion;

+ (void) applyForGigWithUser:(NSString *) userID token:(NSString *) token gigID:(NSString *) gigID completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion;



@end
