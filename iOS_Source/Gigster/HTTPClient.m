//
//  HTTPClient.m
//  Gigster
//
//  Created by Rakitha Perera on 5/4/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import "HTTPClient.h"

@implementation HTTPClient

+ (instancetype)sharedClient {
    static HTTPClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[HTTPClient alloc] initWithBaseURL:[NSURL URLWithString:API_URL]];
        _sharedClient.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        _sharedClient.uploadManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:API_URL]];
    });
    
    return _sharedClient;
}
    
-(void)initSessionConfiguration:(NSURLSessionConfiguration *)configuration {
        self.downloadManager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
}
    
#pragma mark- Handle background session completion
    
- (void)configureBackgroundSessionCompletion {
    if(!self.downloadManager){
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:SHIFT_IMAGE_UPLOAD_ID];
        configuration.HTTPMaximumConnectionsPerHost = 10;
        [self initSessionConfiguration:configuration];
        [self.downloadManager setDownloadTaskDidWriteDataBlock:^(NSURLSession *session, NSURLSessionDownloadTask *downloadTask, int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite){
            NSLog(@"i am downloading my id = %lu progress= %f",(unsigned long)downloadTask.taskIdentifier, totalBytesWritten*1.0/totalBytesExpectedToWrite);
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            
        }];
        [self.downloadManager setDownloadTaskDidFinishDownloadingBlock:^NSURL *(NSURLSession *session, NSURLSessionDownloadTask *downloadTask, NSURL *location){
            NSLog(@"download finished");
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            return location;
            
        }];
    }
    
    typeof(self) __weak weakSelf = self;
    
    [self.downloadManager setDidFinishEventsForBackgroundURLSessionBlock:^(NSURLSession *session) {
        
        if (weakSelf.backgroundSessionCompletionHandler) {
            weakSelf.backgroundSessionCompletionHandler();
            weakSelf.backgroundSessionCompletionHandler = nil;
        }
    }];
}

//- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
//didCompleteWithError:(nullable NSError *)error {
//
//
//}
//
//- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
//   didSendBodyData:(int64_t)bytesSent
//    totalBytesSent:(int64_t)totalBytesSent
//totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend {
//
//}



@end
