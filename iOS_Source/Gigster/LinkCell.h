//
//  LinkCell.h
//  Gigster
//
//  Created by Rakitha Perera on 8/16/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Explore.h"

@interface LinkCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *linkLabel;


- (void)configCellWithData:(Explore *)item;
@end
