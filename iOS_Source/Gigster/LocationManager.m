//
//  LocationManager.m
//  Gigster
//
//  Created by Rakitha Perera on 2/26/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import "LocationManager.h"
#import <CoreLocation/CoreLocation.h>

@implementation LocationManager 

+ (instancetype)sharedInstance
{
    static LocationManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[LocationManager alloc] init];
        // Do any other initialisation stuff here
        LocationManager *instance = sharedInstance;
        instance.locationManager = [[CLLocationManager alloc] init];
        instance.locationManager.delegate = instance;
        instance.locationManager.distanceFilter = kCLDistanceFilterNone;
        instance.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        instance.locationManager.pausesLocationUpdatesAutomatically = NO;
    });
    return sharedInstance;
}

- (void)startUpdatingLocation
{
    self.locationStatus = [CLLocationManager authorizationStatus];
    
    if (self.locationStatus == kCLAuthorizationStatusDenied)
    {
        NSLog(@"Location services are disabled in settings.");
    }
    else
    {
        // for iOS 8
        if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)])
        {
            [self.locationManager requestWhenInUseAuthorization];
        }
        // for iOS 9
//        if ([self.locationManager respondsToSelector:@selector(setAllowsBackgroundLocationUpdates:)])
//        {
//            [self.locationManager setAllowsBackgroundLocationUpdates:YES];
//        }
        
        [self.locationManager startUpdatingLocation];
    }
}

- (void)stopUpdatingLocation
{
    [self.locationManager stopUpdatingLocation];
}

- (BOOL)isLocationAvilableForVC:(UIViewController *)viewController {
    self.locationStatus = [CLLocationManager authorizationStatus];
    
    if (![CLLocationManager locationServicesEnabled])
    {
        NSLog(@"if user has not given permission to device");
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(@"Turn On Location Servces to Allow \"Gigstr\" to Determine Your Location", "")
                                      message:NSLocalizedString(@"Gigstr needs your location when checking in/out a shift. Your location is only used internally for administration purposes.", "")
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        [viewController presentViewController:alert animated:YES completion:nil];
        
        UIAlertAction* settings = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Settings", "")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
//                                       [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root=Privacy&path=LOCATION"]];
                                       [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                       
                                   }];
        [alert addAction:settings];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"Cancel", "")
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
        [alert addAction:cancel];
        return NO;

    }
    
    if (self.locationStatus == kCLAuthorizationStatusAuthorizedWhenInUse) {
        return YES;
    } else if (self.locationStatus == kCLAuthorizationStatusDenied || self.locationStatus == kCLAuthorizationStatusRestricted)
    {
        NSLog(@"Location services are disabled in settings.");
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(@"Turn On Location Servces to Allow \"Gigstr\" to Determine Your Location", "")
                                      message:NSLocalizedString(@"Gigstr needs your location when checking in/out a shift. Your location is only used internally for administration purposes.", "")
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        [viewController presentViewController:alert animated:YES completion:nil];
        
        UIAlertAction* settings = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"Settings", "")
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                 
                             }];
        [alert addAction:settings];
        UIAlertAction* cancel = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", "")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       
                                   }];
        [alert addAction:cancel];
        return NO;
    } else if (self.locationStatus == kCLAuthorizationStatusNotDetermined) {
        [self startUpdatingLocation];
    }
    
    return NO;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    if ([locations count] > 0) {
        CLLocation *loc = [locations lastObject];
        
        // Lat/Lon
        float latitudeMe = loc.coordinate.latitude;
        float longitudeMe = loc.coordinate.longitude;
        self.currentLocation = loc;
        NSLog(@"My Location %f %f", latitudeMe, longitudeMe);
    }
}

@end
