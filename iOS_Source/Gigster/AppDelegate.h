//
//  AppDelegate.h
//  Gigster
//
//  Created by EFutures on 10/29/15.
//  Copyright © 2015 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString * const kIsDeepLinkActiveKey = @"kIsDeepLinkActiveKey";
static NSString * const kOpenGigDeepLinkKey = @"kOpenGigDeepLinkKey";

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

