//
//  SocialViewController.m
//  Gigster
//
//  Created by Rakitha Perera on 4/28/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import "SocialViewController.h"

@interface SocialViewController ()<UIWebViewDelegate>

@property(nonatomic,weak)IBOutlet UIWebView *webView;
@property(nonatomic,weak)IBOutlet UIActivityIndicatorView *activity;

@end

@implementation SocialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_activity setHidden:NO];
    [_activity startAnimating];
    [self.webView loadRequest:[NSURLRequest requestWithURL:self.webURL]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self setupBackButton:@"Back"];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [_activity setHidden:YES];
    [_activity stopAnimating];
    NSLog(@"Current URL = %@",webView.request.URL);
    
    //-- Add further custom actions if needed
}


@end
