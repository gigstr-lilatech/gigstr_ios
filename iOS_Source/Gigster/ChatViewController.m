//
//  ChatViewController.m
//  Gigster
//
//  Created by EFutures on 12/13/15.
//  Copyright © 2015 EFutures. All rights reserved.
//

#import "ChatViewController.h"
#import "ReceiverCell.h"
#import "SenderCell.h"
#import "User.h"
#import "ZSWTappableLabel.h"
#import "WebViewController.h"
#import "UIImageView+AFNetworking.h"

static NSString *const TextCheckingResultAttributeName = @"TextCheckingResultAttributeName";

@interface ChatViewController () <ZSWTappableLabelTapDelegate> {
    
    User *user;
    NSTimer* timer;
    NSURL *webURL;
}

@property(nonatomic,weak)IBOutlet UITableView *chatTable;
@property(nonatomic,weak)IBOutlet UIButton *sendBtn;
@property(nonatomic,weak)IBOutlet UITextView *textView;
@property(nonatomic,strong)NSMutableArray *messageArry;
@property(nonatomic,weak)IBOutlet UIActivityIndicatorView *activity;


@end

@implementation ChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:self.userImageURL delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//    [alert show];
//    alert = nil;
    
    [self pushBadgeClear];
    
    _textView.textColor = [UIColor colorWithRed:204.0f/255.0f green:204.0f/255.0f blue:204.0f/255.0f alpha:1.0];
    _textView.text = NSLocalizedString(@"Type a message to Gigstr", nil);
    
    self.messageArry = [[NSMutableArray alloc]init];
    
    [self.chatTable setContentInset:UIEdgeInsetsMake(17, 0, 0, 0)];
    
    NSUserDefaults *defaults    = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedArray = [defaults objectForKey:@"userObject"];
    if (dataRepresentingSavedArray != nil)
    {
         user =  (User *)[NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
        
    }
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnTableView:)];
    [self.chatTable addGestureRecognizer:tap];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userSessionTimedOut)
                                                 name:@"UserSessionTimedOut"
                                               object:nil];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];

    [_activity setHidden:YES];
    [self loadChatMessages];
    
    timer = [NSTimer timerWithTimeInterval:5.0f target:self selector:@selector(refreshMessages) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [timer invalidate];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)userSessionTimedOut
{
   [timer invalidate];
}

-(void)pushBadgeClear{
    
    [User updatePushCountcompletion:^(BOOL success, NSDictionary *data, NSError *error) {
        
        [UIApplication sharedApplication].applicationIconBadgeNumber= 0;
        
        NSLog(@"success: %d", success);
        NSLog(@"data: %@", data);
        NSLog(@"Error: %@", error);
        
    }];
    
}
-(void)loadChatMessages{
    
//    [_activity setHidden:NO];
//    [_activity startAnimating];

    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
    [ChatDTO getChatMessageForUser:user.userid token:user.token chatID:self.chatDTO.chatID completion:^(BOOL success, NSDictionary *data, NSError *error) {
        
        NSLog(@"success: %d", success);
        NSLog(@"data: %@", data);
        NSLog(@"Error: %@", error);
        
//        [_activity setHidden:YES];
//        [_activity stopAnimating];
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if (success) {

            if (!self.messageArry) {
                
                self.messageArry = [[NSMutableArray alloc]init];
            }
            [self.messageArry removeAllObjects];
            [self passData:[data objectForKey:@"messages"]];
            
            if (_chatTable.contentSize.height > _chatTable.frame.size.height)
            {
                CGPoint offset = CGPointMake(0, _chatTable.contentSize.height - _chatTable.frame.size.height);
                [_chatTable setContentOffset:offset animated:YES];
            }
        }else{
            
            if ([data valueForKey:@"message"]) {
                NSString *status = [NSString stringWithFormat:@"%@",[data valueForKey:@"message"]];
                [self showAlert:status];
                
            }else{
                [self showAlert:NSLocalizedString(@"Could not connect to the server. Please try again", nil)];
            }
        }
      
        
    }];
}


-(void)showAlert:(NSString *)message{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             [self dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    [alert addAction:ok];
    
}


-(void)passData:(NSArray *)messages{
    
    for (int i=0; i<[messages count]; i++) {
        
        NSDictionary *dic = [messages objectAtIndex:i];
        ChatDTO *chatObj = [[ChatDTO alloc]init];
        chatObj.chatID = [dic valueForKey:@"chat_id"];
        chatObj.messageID = [dic valueForKey:@"id"];
        chatObj.message = [dic valueForKey:@"message"];
        chatObj.createdTime = [dic valueForKey:@"created"];
        chatObj.createdByID = [dic valueForKey:@"created_by"];
        
        [self.messageArry addObject:chatObj];
    }
    
        [_chatTable reloadData];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.messageArry count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ChatDTO *chat = [self.messageArry objectAtIndex:indexPath.row];
    
    if ([user.userid isEqualToString:chat.createdByID]) {
        //User Cell
        return [self calculateCellHeight:chat.message].size.height + 50;

    }
    else{
        //Gigster cell
        return [self calculateCellHeight:chat.message].size.height + 50;

    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ChatDTO *chat = [self.messageArry objectAtIndex:indexPath.row];
    
    if ([user.userid intValue] == [chat.createdByID intValue]) {
        
        static NSString *cellIdentifier = @"SenderCell";
        
        SenderCell *cell = [_chatTable dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (cell == nil) {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SenderCell" owner:self options:nil];
            cell = [topLevelObjects objectAtIndex:0];
            
        }

        cell.message.tapDelegate = self;
        cell.message.text = NSLocalizedString(chat.message,nil);
        
        if (chat.userImage) {
            cell.senderImage.image = chat.userImage;
        }else{
            
            __weak SenderCell *weakCell = cell;
            
            NSURL *url = [NSURL URLWithString:self.userImageURL];
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            UIImage *placeholderImage = [UIImage imageNamed:@"user_profile"];
            
            [weakCell.senderImage setImageWithURLRequest:request
                                     placeholderImage:placeholderImage
                                              success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                                  
                                                  chat.userImage = image;
                                                  weakCell.senderImage.image = image;
                                                //[weakCell.activity stopAnimating];
                                               // weakCell.activity.hidden = YES;
                                                  
                                                  //[weakCell setNeedsLayout];
                                                  
                                                  
                                              } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                                  
                                                  NSLog(@"response %@",response);
                                                  NSLog(@"error %@",error);
                                              }];
            
            
        }
        
        cell.time.text = [self formattedTime:chat.createdTime];
        [self setupDescription:cell.message];
        return cell;
        
    }else{
        
        static NSString *cellIdentifier = @"ReceiverCell";
        
        ReceiverCell *cell = [_chatTable dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (cell == nil) {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ReceiverCell" owner:self options:nil];
            cell = [topLevelObjects objectAtIndex:0];
            
        }
        cell.message.tapDelegate = self;
        cell.message.text = NSLocalizedString(chat.message,nil);

        
        cell.time.text = [self formattedTime:chat.createdTime];
        [self setupDescription:cell.message];
        return cell;
        
    }

}

-(CGRect)calculateCellHeight:(NSString *)message{
    
    NSMutableParagraphStyle *style =  [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    //[style setLineSpacing:2];
    style.alignment = NSTextAlignmentLeft;
    style.firstLineHeadIndent = 10.0f;
    style.headIndent = 10.0f;
    style.tailIndent = -10.0f;
    NSMutableAttributedString *attrText = [[NSMutableAttributedString alloc] initWithString:message attributes:@{ NSParagraphStyleAttributeName : style}];
    
    NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
    attributes[ZSWTappableLabelTappableRegionAttributeName] = @YES;
    attributes[ZSWTappableLabelHighlightedBackgroundAttributeName] = [UIColor clearColor];
    attributes[ZSWTappableLabelHighlightedForegroundAttributeName] = [UIColor blueColor];
    attributes[NSUnderlineStyleAttributeName] = @(NSUnderlineStyleSingle);
    attributes[NSForegroundColorAttributeName] = [UIColor colorWithRed:131.0f/255.0f green:213.0f/255.0f blue:210.0f/255.0f alpha:1.0];
    attributes[NSFontAttributeName] = [UIFont fontWithName:DEFAULT_FONT_NAME size:14.0];
    [attrText addAttributes:attributes range:NSMakeRange(0, message.length)];
    
    CGFloat width = self.view.frame.size.width - (112);
    
    CGRect rect = [attrText
                   boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX)
                   options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                   context:nil];
    
    CGRect frame = rect;
    frame.size.height = rect.size.height ;
    return frame;
}


-(void)setupDescription:(ZSWTappableLabel *)label{

    
    NSMutableParagraphStyle *style =  [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    style.alignment = NSTextAlignmentLeft;
    style.firstLineHeadIndent = 10.0f;
    style.headIndent = 10.0f;
    style.tailIndent = -10.0f;
    NSLog(@"%@",label.text);
    NSMutableAttributedString *attrText = [[NSMutableAttributedString alloc] initWithString:label.text attributes:@{ NSParagraphStyleAttributeName : style}];
    
    NSDataDetector *dataDetector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingAllSystemTypes error:NULL];
    [dataDetector enumerateMatchesInString:label.text options:0 range:NSMakeRange(0, label.text.length) usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
        NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
        attributes[TextCheckingResultAttributeName] = result;
        attributes[ZSWTappableLabelTappableRegionAttributeName] = @YES;
        attributes[ZSWTappableLabelHighlightedBackgroundAttributeName] = [UIColor clearColor];
        attributes[ZSWTappableLabelHighlightedForegroundAttributeName] = [UIColor blueColor];
        attributes[NSUnderlineStyleAttributeName] = @(NSUnderlineStyleSingle);
        attributes[NSForegroundColorAttributeName] = [UIColor colorWithRed:131.0f/255.0f green:213.0f/255.0f blue:210.0f/255.0f alpha:1.0];
        attributes[NSFontAttributeName] = [UIFont fontWithName:DEFAULT_FONT_NAME size:14.0];
        [attrText addAttributes:attributes range:result.range];
    }];
    
    label.attributedText = attrText ;
}

-(NSString *)formattedTime:(NSString *)time{
    
    //server Date
    NSString *dateServer = time;
    NSDateFormatter *dateF1 = [[NSDateFormatter alloc] init];
    [dateF1 setDateFormat:@"yyy-MM-dd HH:mm:ss"];
    dateF1.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateF1 dateFromString:dateServer];
    
    NSDateFormatter *dateF2 = [[NSDateFormatter alloc] init];
    dateF2.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:[NSTimeZone localTimeZone].secondsFromGMT];
    [dateF2 setDateFormat:@"HH:mm dd MMM"];
    NSString *stringFromServerDate = [dateF2 stringFromDate:dateFromString];
    
    
    //Current date
    NSDateFormatter *df1 = [[NSDateFormatter alloc] init];
    df1.dateFormat = @"HH:mm dd MMM";
    NSString *currentDate = [df1 stringFromDate:[NSDate date]];
    
    //comparison
    if ([currentDate isEqualToString:stringFromServerDate]) {
        
        NSString *dateString = time;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyy-MM-dd HH:mm:ss"];
        dateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        NSDate *dateFromString = [[NSDate alloc] init];
        dateFromString = [dateFormatter dateFromString:dateString];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:[NSTimeZone localTimeZone].secondsFromGMT];
        [formatter setDateFormat:@"HH:mm dd MMM"];
        NSString *stringFromDate = [formatter stringFromDate:dateFromString];
        
        //return time
        return stringFromDate;
        
    }else{
        //return Date
        return stringFromServerDate;
    }
    
}



-(void)refreshMessages{
    
    
    [ChatDTO getUnreadMessages:user.userid token:user.token chatID:self.chatDTO.chatID completion:^(BOOL success, NSDictionary *data, NSError *error) {
        
        NSLog(@"success: %d", success);
        NSLog(@"data: %@", data);
        NSLog(@"Error: %@", error);
        
        if (success) {
            
            if ([[data objectForKeyNotNull:@"messages"] count] > 0) {
                
                [self passData:[data objectForKeyNotNull:@"messages"]];
                [_chatTable reloadData];
                if (_chatTable.contentSize.height > _chatTable.frame.size.height)
                {
                    CGPoint offset = CGPointMake(0, _chatTable.contentSize.height -     _chatTable.frame.size.height);
                    [_chatTable setContentOffset:offset animated:YES];
                }
            }
        }
        
    }];
}

//-(void)addUnreadMessages:(NSArray *)messages{
//    
//    
//}
-(IBAction)sendMessage:(id)sender{
    
    if ([_textView.text isEqualToString:NSLocalizedString(@"Type a message to Gigstr", nil)] || [_textView.text isEqualToString:@""]) {
        
        return;
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    //-------- base64 encoding
    // Create NSData object
    NSData *nsdata = [_textView.text
                      dataUsingEncoding:NSUTF8StringEncoding];
    // Get NSString from NSData object in Base64
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    // -------------
    
    _sendBtn.enabled = NO;
    [ChatDTO sendMessage:user.userid token:user.token chatID:self.chatDTO.chatID message:base64Encoded completion:^(BOOL success, NSDictionary *data, NSError *error) {
        
        NSLog(@"success: %d", success);
        NSLog(@"data: %@", data);
        NSLog(@"Error: %@", error);
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        _sendBtn.enabled = YES;
        
        if (success) {
            
            NSLog(@"%@",[data objectForKey:@"data"]);
            [self addSenderMessageLocally:data withMessage:base64Encoded];
            
            _textView.text = @"";
            
            [_chatTable reloadData];
            if (_chatTable.contentSize.height > _chatTable.frame.size.height)
            {
                CGPoint offset = CGPointMake(0, _chatTable.contentSize.height -     _chatTable.frame.size.height);
                [_chatTable setContentOffset:offset animated:YES];
            }
        }else{
            
            if ([data valueForKey:@"message"]) {
                NSString *status = [NSString stringWithFormat:@"%@",[data valueForKey:@"message"]];
                [self showAlert:status];
                
            }else{
                [self showAlert:NSLocalizedString(@"Could not connect to the server. Please try again", nil)];
            }
        }
        
        
    }];
}

-(void)addSenderMessageLocally:(NSDictionary *)response withMessage:(NSString *)message{

    ChatDTO *chatObj = [[ChatDTO alloc]init];
    
        chatObj.createdByID = user.userid;
        chatObj.messageID = [response valueForKey:@"id"];
        chatObj.createdTime = [response valueForKey:@"created"];
        chatObj.chatID = self.chatDTO.chatID;
        chatObj.message = message;
    
    [self.messageArry addObject:chatObj];
}

-(void) didTapOnTableView:(UIGestureRecognizer*) recognizer {
 
    if(_textView.text.length == 0){
        _textView.textColor = [UIColor colorWithRed:204.0f/255.0f green:204.0f/255.0f blue:204.0f/255.0f alpha:1.0];
        _textView.text = NSLocalizedString(@"Type a message to Gigstr", nil);
        [_textView resignFirstResponder];
    }else {
        
        [_textView resignFirstResponder];
    }
    
    
}

#pragma mark UITextview Delegates

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if ([_textView.text isEqualToString:NSLocalizedString(@"Type a message to Gigstr", nil)]) {
        
        _textView.text = @"";
        _textView.textColor = [UIColor blackColor];
        
    }
    return YES;
    
}

-(void)textViewDidChange:(UITextView *)textView
{
    
    if(_textView.text.length == 0){
        _textView.textColor = [UIColor colorWithRed:204.0f/255.0f green:204.0f/255.0f blue:204.0f/255.0f alpha:1.0];
        _textView.text = NSLocalizedString(@"Type a message to Gigstr", nil);
        [_textView resignFirstResponder];
    }
}


- (BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"]){
       // [textView resignFirstResponder];
        [self sendMessage:nil];
        return NO;
    }else{
        return YES;
    }
}

#pragma mark - ZSWTappableLabelTapDelegate

- (void)tappableLabel:(ZSWTappableLabel *)tappableLabel tappedAtIndex:(NSInteger)idx withAttributes:(NSDictionary<NSString *,id> *)attributes {
    NSURL *URL;
    
    NSTextCheckingResult *result = attributes[TextCheckingResultAttributeName];
    if ([result isKindOfClass:[NSTextCheckingResult class]]) {
        switch (result.resultType) {
                
            case NSTextCheckingTypeLink:
                
                [_textView resignFirstResponder];
                URL = result.URL;
                NSLog(@"URLLLLL%@",URL);
                webURL = URL;
                [self performSegueWithIdentifier:@"pushWebview" sender:self];
                
                break;
                
            default:
                break;
        }
    }
    
//    if ([URL isKindOfClass:[NSURL class]]) {
//        if ([SFSafariViewController class] != nil && [@[ @"http", @"https"] containsObject:URL.scheme.lowercaseString]) {
//            [self showViewController:[[SFSafariViewController alloc] initWithURL:URL] sender:self];
//        } else {
//            [[UIApplication sharedApplication] openURL:URL];
//        }
//    }
}



#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"pushWebview"]){
        
        WebViewController *detailController   = (WebViewController *)[segue destinationViewController];
        detailController.webURL               = webURL;
    }
}

@end
