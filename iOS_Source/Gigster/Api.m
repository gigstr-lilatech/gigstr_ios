//
//  Api.m
//  ssssssssss
//
//  Created by Chathura Payagala on 12/8/14.
//  Copyright (c) 2014 Chathura Payagala. All rights reserved.
//

#import "Api.h"
#import "AFNetworking.h"
#import "AppDelegate.h"
#import "Reachability.h"
#import "User.h"
#import "Utility.h"
#import "HTTPClient.h"

@implementation Api


+(void)post:(NSDictionary *) data onCompletion:(void (^) (BOOL success, id response, NSError *error)) onCompletion {
    
//    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
   [requestSerializer setTimeoutInterval:5000.0f];
    [requestSerializer setValue:[[NSUserDefaults standardUserDefaults] valueForKey:kCountryCode] forHTTPHeaderField:kCountryCode];
    
    [requestSerializer setValue:[[NSUserDefaults standardUserDefaults] valueForKey:kLanguageCode] forHTTPHeaderField:kLanguageCode];
    [requestSerializer setValue:[Utility getDeviceId] forHTTPHeaderField:kDeviceID];

    
    NSUserDefaults *defaults    = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedArray = [defaults objectForKey:@"userObject"];
    if (dataRepresentingSavedArray != nil)
    {
        User *user =  (User *)[NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
        [requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
        [requestSerializer setValue:user.userid forHTTPHeaderField:@"user_id"];
    }

    [HTTPClient sharedClient].requestSerializer = requestSerializer;
    [HTTPClient sharedClient].responseSerializer = responseSerializer;
    NSLog(@"JSON: %@", [data objectForKey:@"parameters"]);
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (!(networkStatus == NotReachable)) {
      
        [[HTTPClient sharedClient] POST:[data valueForKey:@"url"] parameters:[data objectForKey:@"parameters"] progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            NSDictionary *response = (NSDictionary *) responseObject;
            long status = [[response objectForKey:@"status"] longValue];
            
            if (status == 0) {
                onCompletion(true, response, nil);
            } else if (status == 400) {
                //Handle the session log out error
                [self handleUserSessionTimeout];
            } else {
                //Need to change
                NSError *err = [[NSError alloc] initWithDomain:NSLocalizedString(@"Error for the status code",nil) code:-1 userInfo:nil];
                onCompletion(false, response, err);
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"Error: %@", error);
            onCompletion(false, nil, error);
        }];
        
    }else{
        NSError *err = [[NSError alloc] initWithDomain:NSLocalizedString(@"You appear to be offline. ssssssssss requires an Internet connection to access the image cloud service. Check your connection and try again",nil)  code:-122 userInfo:nil];
        onCompletion(false, nil, err);
    }
    
}

+(void)handleUserSessionTimeout {
    //Remove the existing user
    NSUserDefaults *defaults            = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedArray  = [defaults objectForKey:@"userObject"];
    User *user =  (User *)[NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
    if (user) {
        [defaults removeObjectForKey:@"userObject"];
        [defaults synchronize];
    }
    
    // Post a notification for session logged out
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UserSessionTimedOut" object:self];
    
    UIWindow *keyWindow = [[[UIApplication sharedApplication] delegate] window];
    UIViewController *previousRootViewController = keyWindow.rootViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UINavigationController *login = [storyboard instantiateViewControllerWithIdentifier:@"Loging_flow"];
    keyWindow.rootViewController = login;
    [self showAlert:NSLocalizedString(@"Your session has expired. Please log in again.",nil)];
    
    //Cleanup active view controllers
    for (UIView *subview in keyWindow.subviews) {
        if ([subview isKindOfClass:NSClassFromString(@"UITransitionView")]) {
            [subview removeFromSuperview];
        }
    }
    // Allow the view controller to be deallocated
    [previousRootViewController dismissViewControllerAnimated:NO completion:^{
        // Remove the root view in case its still showing
        [previousRootViewController.view removeFromSuperview];
    }];


}

+(void)showAlert:(NSString *)message{
    UIWindow *keyWindow = [[[UIApplication sharedApplication] delegate] window];
    id rootViewController=keyWindow.rootViewController;
    
    if([rootViewController isKindOfClass:[UINavigationController class]])
    {
        rootViewController=[((UINavigationController *)rootViewController).viewControllers objectAtIndex:0];
    }
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [rootViewController presentViewController:alert animated:YES completion:nil];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             [rootViewController dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    [alert addAction:ok];
    
}



@end
