//
//  ButtonCollectionViewCell.m
//  Gigster
//
//  Created by Rakitha Perera on 8/9/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import "ButtonCell.h"

@implementation ButtonCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configCellWithData:(Explore *)item {
    self.item = item;
    [self.button setTitle:item.title forState:UIControlStateNormal];
}
- (IBAction)buttonTapped:(id)sender {
    [self.delegate ButtonCell:self didTapOnButton:self.item];
}

@end
