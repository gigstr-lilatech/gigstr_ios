//
//  CarouselCollectionViewCell.m
//  Gigster
//
//  Created by Rakitha Perera on 8/9/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import "CarouselCollectionViewCell.h"
#import "UIImageView+AFNetworking.h"

@implementation CarouselCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configCellWithData:(CarouselItem *)item {
    self.cTitleLbel.text = item.title;
    self.cDescriptionLabel.text = item.item_description;
    [self.cImageView setImageWithURL:[NSURL URLWithString:item.image] placeholderImage:[UIImage imageNamed:@"explore_placeholder"]];
}

@end
