//
//  GigDetailsViewController.m
//  Gigster
//
//  Created by EFutures on 11/24/15.
//  Copyright © 2015 EFutures. All rights reserved.
//

#import "GigDetailsViewController.h"
#import "UIImageView+AFNetworking.h"
#import "User.h"
#import "ChatViewController.h"
#import "LoginTempViewcontroller.h"
#import "LoginFlowViewController.h"
#import "Utility.h"
#import "BranchUniversalObject.h"
#import "BranchLinkProperties.h"


@interface GigDetailsViewController ()<UIScrollViewDelegate, UIWebViewDelegate, LoginTempViewcontrollerDelegate>{
    
    NSString *chatID;
    NSString *userImageURL;
}

@property(nonatomic,weak)IBOutlet UIImageView *gigImageview;
@property(nonatomic,weak)IBOutlet UIScrollView *scrollview;

@property(nonatomic,weak)IBOutlet UILabel *lblTitle;
@property(nonatomic,weak)IBOutlet UILabel *lblSub1;
@property(nonatomic,weak)IBOutlet UILabel *lblDescription;
@property(nonatomic,weak)IBOutlet UIWebView *webview;

@property(nonatomic,weak)IBOutlet UIButton *btnApply;

@property(nonatomic,weak)IBOutlet UIActivityIndicatorView *activity;

@end

@implementation GigDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
        
    [_activity setHidden:YES];
    
    if (self.job.applied) {
        
        [self setAppliedButton];
    }
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ios_share"] style:UIBarButtonItemStylePlain target:self action:@selector(shareGig:)];
    
    [self.navigationItem setRightBarButtonItem:item];

    [self setButtonLayers:_btnApply];
    [_scrollview setContentSize:CGSizeMake(self.view.frame.size.width, _scrollview.frame.size.height + 1000)];
    
    _webview.scrollView.showsHorizontalScrollIndicator = NO;
    _webview.scrollView.showsVerticalScrollIndicator = NO;
    _webview.scrollView.delegate = self;
    

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self loadGigDetails];
}

- (UIImage*)imageWithBlurredImageWithImage:(UIImage*)image {
    
    CIImage *inputImage = [[CIImage alloc] initWithImage:image];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    
    //First, we'll use CIAffineClamp to prevent black edges on our blurred image
    //CIAffineClamp extends the edges off to infinity (check the docs, yo)
    CGAffineTransform transform = CGAffineTransformIdentity;
    CIFilter *clampFilter = [CIFilter filterWithName:@"CIAffineClamp"];
    [clampFilter setValue:inputImage forKeyPath:kCIInputImageKey];
    [clampFilter setValue:[NSValue valueWithBytes:&transform objCType:@encode(CGAffineTransform)] forKeyPath:@"inputTransform"];
    CIImage *clampedImage = [clampFilter outputImage];
    
    //Next, create some darkness
    CIFilter* blackGenerator = [CIFilter filterWithName:@"CIConstantColorGenerator"];
    CIColor* black = [CIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.5];
    [blackGenerator setValue:black forKey:@"inputColor"];
    CIImage* blackImage = [blackGenerator valueForKey:@"outputImage"];
    
    //Apply that black
    CIFilter *compositeFilter = [CIFilter filterWithName:@"CIMultiplyBlendMode"];
    [compositeFilter setValue:blackImage forKey:@"inputImage"];
    [compositeFilter setValue:clampedImage forKey:@"inputBackgroundImage"];
    CIImage *darkenedImage = [compositeFilter outputImage];
    
    //Third, blur the image
    CIFilter *blurFilter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [blurFilter setDefaults];
    [blurFilter setValue:@(3.0f) forKey:@"inputRadius"];
    [blurFilter setValue:darkenedImage forKey:kCIInputImageKey];
    CIImage *blurredImage = [blurFilter outputImage];
    
    CGImageRef cgimg = [context createCGImage:blurredImage fromRect:inputImage.extent];
    UIImage *blurredAndDarkenedImage = [UIImage imageWithCGImage:cgimg];
    CGImageRelease(cgimg);
    
    return blurredAndDarkenedImage;
}


-(UIView*)viewForZoomingInScrollView:(UIScrollView*)scrollView {
    return nil;
}

-(void)setJobImage{
    
    self.gigImageview.clipsToBounds = YES;
    if (self.job.jobImage) {
        self.gigImageview.image = [self imageWithBlurredImageWithImage:self.job.jobImage];
    }
    else{
        
        NSURL *url                  = [NSURL URLWithString:self.job.jobImageUrl];
        NSURLRequest *request       = [NSURLRequest requestWithURL:url];
        UIImage *placeholderImage   = [UIImage imageNamed:@"defaultImage"];
        
        [self.gigImageview setImageWithURLRequest:request
                                 placeholderImage:placeholderImage
                                          success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                              
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  self.job.jobImage = image;
                                                  self.gigImageview.image = [self imageWithBlurredImageWithImage:image];
                                              });
                                              
                                              
                                          } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                              
                                              NSLog(@"response %@",response);
                                              NSLog(@"error %@",error);
                                          }];
    }
    
    self.gigImageview.clipsToBounds = YES;
    
}


-(void)loadGigDetails{
    NSString *userID, *token;
    User *user = [Utility getLoggedUser];
    if (user) {
        userID = user.userid;
        token = user.token;
    } else {
        userID = @"";
        token = @"";
    }

        [Jobs listGig:userID token:token gigID:self.job.jobID completion:^(BOOL success, NSDictionary *data, NSError *error) {
            
            NSLog(@"success: %d", success);
            NSLog(@"data: %@", data);
            NSLog(@"Error: %@", error);
            
            if (success) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self passData:[data objectForKey:@"job"]];
                    chatID = [data objectForKey:@"chat_id"];
                    if (self.job.applied) {
                        [self setAppliedButton];
                    }
                });
                
            }
            else{
                if ([[data valueForKey:@"status"] integerValue] == 201) {
                    // invalid gig id
                    [self.navigationController popViewControllerAnimated:YES];
                } else if ([data valueForKey:@"message"]) {
                    NSString *status = [NSString stringWithFormat:@"%@",[data valueForKey:@"message"]];
                    [self showAlert:status];
                    
                }else{
                    [self showAlert:NSLocalizedString(@"Could not connect to the server. Please try again", nil)];
                }
            }
            
        }];
}

-(void)passData:(NSDictionary *) gigDictionary{
    
        Jobs *jobDTO            = [[Jobs alloc]init];
        jobDTO.jobID            = [gigDictionary valueForKey:@"id"];
        jobDTO.gigActive        = [[gigDictionary valueForKey:@"active"] boolValue];
        jobDTO.applied          = [[gigDictionary valueForKey:@"applied"] boolValue];
        jobDTO.jobTitle         = [gigDictionary valueForKey:@"title"];
        jobDTO.jobDescription   = [gigDictionary valueForKey:@"description"];
        jobDTO.jobDescriptionLink = [gigDictionary valueForKey:@"description_link"];
        jobDTO.jobSubtitle1     = [gigDictionary valueForKey:@"sub_title1"];
        jobDTO.jobSubtitle2     = [gigDictionary valueForKey:@"sub_title2"];
        jobDTO.jobImageUrl      = [gigDictionary valueForKey:@"gig_image"];
    
        userImageURL            = [gigDictionary valueForKey:@"user_image"];

        self.job                = jobDTO;
    
    if (self.job.applied) {
        
        [self setAppliedButton];
    }
    
    _lblTitle.text = self.job.jobTitle;
    _lblSub1.text  = self.job.jobSubtitle1;
    NSString *urlString = self.job.jobDescriptionLink;

    [self setJobImage];
    [self.webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
}

-(CGRect)frameForDescription{
    
    CGFloat width = self.lblDescription.frame.size.width; // whatever your desired width is
    CGRect rect = [self.lblDescription.attributedText boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    
    return rect;
}
-(void)showAlert:(NSString *)message{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [self.navigationController popViewControllerAnimated:YES];
                             [self dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    [alert addAction:ok];
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
 

    NSLog(@"Current URL = %@",webView.request.URL);
    
}


-(void)setButtonLayers:(UIButton *)button{
    
    [[button layer] setCornerRadius:5.0f];
    [[button layer] setMasksToBounds:YES];
}

-(IBAction)applyClicked:(id)sender{
    
    if (self.job.applied) {
        
        [self performSegueWithIdentifier:@"segueChatscreen" sender:self];
        return;
    }

    NSUserDefaults *defaults    = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedArray = [defaults objectForKey:@"userObject"];
    if (dataRepresentingSavedArray != nil)
    {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@""
                                      message:NSLocalizedString(@"Do you want to apply to this Gig?", nil)
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"OK", nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 
                                 [self dismissViewControllerAnimated:YES completion:nil];
                                 [self applyForGig];
                                 
                                 
                             }];
        
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     
                                     [self dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
        
        [alert addAction:ok];
        [alert addAction:cancel];

    }else{
        
          [self performSegueWithIdentifier:@"seguepromtLogin" sender:self];
    }

    
}

-(void)applyForGig{
    
    
    NSUserDefaults *defaults    = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedArray = [defaults objectForKey:@"userObject"];
    if (dataRepresentingSavedArray != nil)
    {
        [_activity setHidden:NO];
        [_activity startAnimating];
        
        User *user =  (User *)[NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
        
        [Jobs applyForGigWithUser:user.userid token:user.token gigID:self.job.jobID completion:^(BOOL success, NSDictionary *data, NSError *error) {
            
            NSLog(@"success: %d", success);
            NSLog(@"data: %@", data);
            NSLog(@"Error: %@", error);
            
            [_activity setHidden:YES];
            [_activity stopAnimating];
            
            if (success) {
                self.job.applied = 1;
                chatID = [data objectForKeyNotNull:@"chat_id"];
                [self setAppliedButton];
                [self performSegueWithIdentifier:@"segueChatscreen" sender:self];
            }
            else{
                
                if ([data valueForKey:@"message"]) {
                    NSString *status = [NSString stringWithFormat:@"%@",[data valueForKey:@"message"]];
                    [self showAlert:status];
                    
                }else{
                    [self showAlert:NSLocalizedString(@"Could not connect to the server. Please try again", nil)];
                }
            }
            
        }];
    }

}




-(void)setAppliedButton{
    
    [_btnApply setBackgroundColor:[UIColor colorWithRed:139.0f/255.0f green:192.0f/255.0f blue:80.0f/255.0f alpha:1.0]];
    [_btnApply setImage:[UIImage imageNamed:@"applied"] forState:UIControlStateNormal];
    [_btnApply setTitle:NSLocalizedString(@"APPLIED", nil) forState:UIControlStateNormal];
}

- (void)shareGig:(UIBarButtonItem *)sender {
    
    BranchUniversalObject *branchUniversalObject = [[BranchUniversalObject alloc] initWithCanonicalIdentifier:[NSString stringWithFormat:@"gig_share/%@", self.job.jobID]];
    [branchUniversalObject addMetadataKey:@"share_gig" value:self.job.jobID];
    

    BranchLinkProperties *linkProperties = [[BranchLinkProperties alloc] init];
    linkProperties.feature = @"sharing";
    [linkProperties addControlParam:@"$desktop_url" withValue:@"https://www.gigstr.com/"];
//    [linkProperties addControlParam:@"$ios_url" withValue:@"https://www.gigstr.com/"];
    [linkProperties addControlParam:@"$always_deeplink" withValue:@"true"];
    NSUserDefaults *defaults    = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedArray = [defaults objectForKey:@"userObject"];
    if (dataRepresentingSavedArray != nil)
    {
       //User logged
        User *user =  (User *)[NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
        [branchUniversalObject addMetadataKey:@"user_id" value:user.userid];
        
    }
    
    [branchUniversalObject showShareSheetWithLinkProperties:linkProperties
                                               andShareText:NSLocalizedString(@"Check this gig out in the gigstr app. I think you are going to like it!", nil)
                                         fromViewController:self
                                                 completion:^(NSString *activityType, BOOL completed) {
                                                     NSLog(@"finished presenting");
                                                 }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"segueChatscreen"]){
        
        ChatDTO *chat = [[ChatDTO alloc]init];
        chat.chatID   = chatID;
        ChatViewController *chatMessages   = (ChatViewController *)[segue destinationViewController];
        chatMessages.chatDTO        = chat;
        chatMessages.userImageURL   = userImageURL;
    } else if ([[segue identifier] isEqualToString:@"seguepromtLogin"]) {
        UINavigationController * navViewController = (UINavigationController *) [segue destinationViewController];
        LoginTempViewcontroller *loginTempVC=[navViewController viewControllers][0];
        loginTempVC.delegate = self;
    }
}

#pragma mark - LoginTempViewcontroller Delegate

- (void)LoginTempViewcontroller:(LoginTempViewcontroller *)loginTempViewcontroller
           dismissedWithSuccess:(BOOL)success {
    if (success) {
        NSString *userID, *token;
        User *user = [Utility getLoggedUser];
        if (user) {
            userID = user.userid;
            token = user.token;
        } else {
            userID = @"";
            token = @"";
        }
        
        [Jobs listGig:userID token:token gigID:self.job.jobID completion:^(BOOL success, NSDictionary *data, NSError *error) {
            
            NSLog(@"success: %d", success);
            NSLog(@"data: %@", data);
            NSLog(@"Error: %@", error);
            
            if (success) {
                
                [self passData:[data objectForKey:@"job"]];
                chatID = [data objectForKey:@"chat_id"];
                if (self.job.applied) {
                    
                    [self performSegueWithIdentifier:@"segueChatscreen" sender:self];
                } else {
                    [self applyForGig];
                }
            }
            else{
                if ([data valueForKey:@"message"]) {
                    NSString *status = [NSString stringWithFormat:@"%@",[data valueForKey:@"message"]];
                    [self showAlert:status];
                    
                }else{
                    [self showAlert:NSLocalizedString(@"Could not connect to the server. Please try again", nil)];
                }
            }
            
        }];
        
    }
}

@end
