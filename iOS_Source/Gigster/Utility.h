//
//  Utility.h
//  Gigster
//
//  Created by EFutures on 11/11/15.
//  Copyright © 2015 EFutures. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "User.h"
@interface Utility : NSObject

+ (BOOL)isValidEmail:(NSString *)email;

+(BOOL)validateTextField:(UITextField *)text;

+(User *)getLoggedUser;

+(NSArray *)getJSONArrayFromName:(NSString *)filename;

+ (UIImage *)imageFromColor:(UIColor *)color;

+ (NSString *)appVersion;

+ (NSString *)getDeviceId;

+ (UIImage*)imageWithBlurredImageWithImage:(UIImage*)image;
@end
