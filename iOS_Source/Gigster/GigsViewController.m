//
//  GigsViewController.m
//  Gigster
//
//  Created by EFutures on 11/16/15.
//  Copyright © 2015 EFutures. All rights reserved.
//

#import "GigsViewController.h"
#import "MPSkewedCell.h"
#import "MPSkewedParallaxLayout.h"
#import "Jobs.h"
#import "User.h"
#import "UIImageView+AFNetworking.h"
#import "AFNetworking.h"
#import "GigDetailsViewController.h"
#import "Utility.h"
#import "CountrySelectViewController.h"
#import "WhatsNewViewController.h"
#import "AppDelegate.h"
#import "Branch.h"
#import "MainTabbarViewController.h"

static NSString * const kCellId = @"cellId";

@interface GigsViewController ()<UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource, CountrySelectViewControllerDelegate>{
    
    NSTimer* timer;
}

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *gigsArray;
@property (nonatomic, strong) Jobs *selectedJob;

@property(nonatomic,weak)IBOutlet UIActivityIndicatorView *activity;

@end

@implementation GigsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.gigsArray = [[NSMutableArray alloc]init];
    [_activity setHidden:YES];
 
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"globe"] style:UIBarButtonItemStylePlain target:self action:@selector(showCountryPicker:)];
    
    [self.navigationItem setRightBarButtonItem:item];
    
    MPSkewedParallaxLayout *layout = [[MPSkewedParallaxLayout alloc] init];
    layout.lineSpacing = 2;
    layout.itemSize = CGSizeMake(CGRectGetWidth(self.view.bounds) - 100, 280);
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:layout];
    [self.collectionView setFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height + 98)];
    self.collectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [self.collectionView registerClass:[MPSkewedCell class] forCellWithReuseIdentifier:kCellId];
    [self.view addSubview:self.collectionView];
    [self.view bringSubviewToFront:self.activity];
    


}

-(void)viewWillAppear:(BOOL)animated {
    [self loadGigs];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    
    BOOL isWhatsNewShown = [[NSUserDefaults standardUserDefaults] boolForKey:khaveShownWhatsNewKey];
    BOOL isDeepLinkActive = [[NSUserDefaults standardUserDefaults] boolForKey:kIsDeepLinkActiveKey];
    if (SHOW_WHATS_NEW && !isDeepLinkActive && !isWhatsNewShown) {
        WhatsNewViewController *whatsNewVC = [[WhatsNewViewController alloc] initWithNibName:@"WhatsNewViewController" bundle:nil];
        
        [self presentViewController:whatsNewVC animated:YES completion:nil];
    }
    
    [self checkForDeepLinks];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [(MPSkewedParallaxLayout *)self.collectionView.collectionViewLayout setItemSize:CGSizeMake(CGRectGetWidth(self.view.bounds), 280)];
}

- (void)checkForDeepLinks {
    BOOL isDeepLinkActive = [[NSUserDefaults standardUserDefaults] boolForKey:kIsDeepLinkActiveKey];
    if (isDeepLinkActive) {
        [[NSUserDefaults standardUserDefaults] setBool:false forKey:kIsDeepLinkActiveKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSString *openGigID = [[NSUserDefaults standardUserDefaults] valueForKey:kOpenGigDeepLinkKey];
        Jobs *job = [[Jobs alloc]init];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        GigDetailsViewController *ivc = [storyboard instantiateViewControllerWithIdentifier:@"GigDetailsViewController"];
        job.jobID = openGigID;
        ivc.job = job;
        UITabBarController *tabbar = (UITabBarController *)self.tabBarController;
        
        if ([tabbar isKindOfClass:MainTabbarViewController.class] && [tabbar.viewControllers count] > 0) {
            [tabbar setSelectedIndex:2];
            UINavigationController *navigation = [tabbar.viewControllers objectAtIndex:1];
            [navigation pushViewController:ivc animated:YES];
        }
    }
}

- (void)showCountryPicker:(UIBarButtonItem *)sender {
    self.tabBarController.definesPresentationContext = YES;
    CountrySelectViewController *countryVC = [[CountrySelectViewController alloc] initWithNibName:@"CountrySelectViewController" bundle:nil];
    countryVC.delegate = self;
    countryVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self.tabBarController presentViewController:countryVC animated:YES completion:nil];

}

- (void)CountrySelectViewController:(CountrySelectViewController *)countrySelectViewController
                 didSelecectCountry:(NSDictionary *)country {
    
    [[NSUserDefaults standardUserDefaults] setValue:country[@"code"] forKey:kCountryCode];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self loadGigs];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.gigsArray count]; // random
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    Jobs *jobs = [self.gigsArray objectAtIndex:indexPath.item];
    MPSkewedCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellId forIndexPath:indexPath];
    cell.text = [jobs.jobTitle uppercaseString];
    cell.appliedStatus      = jobs.applied;
    cell.sub1Text           = [jobs.jobSubtitle1 uppercaseString];
    cell.sub2Text           = jobs.jobSubtitle2;
    
    [cell adjustAppliedUI];
    
    if (jobs.applied) {
        cell.appliedStamp.hidden = NO;
    }else{
        cell.appliedStamp.hidden = YES;
    }
        NSURL *url                  = [NSURL URLWithString:jobs.jobImageUrl];
        NSURLRequest *request       = [NSURLRequest requestWithURL:url];
        UIImage *placeholderImage   = [UIImage imageNamed:@"defaultImage"];
    
        __weak MPSkewedCell *weakCell = cell;
    
    if (jobs.jobImage) {
        
        cell.image = jobs.jobImage;
    }
    else{
        
        [weakCell.imageView setImageWithURLRequest:request
                                  placeholderImage:placeholderImage
                                           success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                    
                                               jobs.jobImage = image;
                                               weakCell.imageView.image = image;
                                               [weakCell.activity stopAnimating];
                                               weakCell.activity.hidden = YES;
                                               weakCell.image = image;
                                               
                                           } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                               
                                               NSLog(@"response %@",response);
                                               NSLog(@"error %@",error);
                                           }];
        
        cell.image = jobs.jobImage;
    }
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
        self.selectedJob = [self.gigsArray objectAtIndex:indexPath.row];
    
        [self performSegueWithIdentifier:@"segueGigDetails" sender:self];
    
    NSLog(@"%@ %zd", NSStringFromSelector(_cmd), indexPath.item);
}


-(void)loadGigs{
    
//    [_activity setHidden:NO];
//    [_activity startAnimating];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    self.collectionView.userInteractionEnabled = NO;
    NSString *userID, *token;
    User *user = [Utility getLoggedUser];
    if (user) {
        userID = user.userid;
        token = user.token;
    } else {
        userID = @"";
        token = @"";
    }
        
        [Jobs listAllGigsforUserId:userID token:token completion:^(BOOL success, NSDictionary *data, NSError *error) {
            
//            [_activity setHidden:YES];
//            [_activity stopAnimating];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            
            self.collectionView.userInteractionEnabled = YES;
            
            NSLog(@"success: %d", success);
            NSLog(@"data: %@", data);
            NSLog(@"Error: %@", error);
            
            if (success) {
                
                if ([[data objectForKeyNotNull:@"jobs"] count] > 0) {
                    
                    [self passData:[data objectForKey:@"jobs"]];
                }
                
            }
            else{
                
                if ([data valueForKey:@"message"]) {
                    
                    NSString *status = [NSString stringWithFormat:@"%@",[data valueForKey:@"message"]];
                    [self showAlert:status];
                    
                }else{
                    [self showAlert:NSLocalizedString(@"Could not connect to the server. Please try again", nil)];
                }
            }
            
        }];
    
}

-(void)passData:(NSMutableArray *) arrayGigs{
    
        [self.gigsArray removeAllObjects];
    
    for (int i=0; i<[arrayGigs count]; i++) {
        
        NSDictionary * gigDic   = [arrayGigs objectAtIndex:i];
        Jobs *jobDTO            = [[Jobs alloc]init];
        jobDTO.jobID            = [gigDic valueForKey:@"id"];
        jobDTO.applied          = [[gigDic valueForKey:@"applied"] boolValue];
        jobDTO.jobTitle         = [gigDic valueForKey:@"title"];
        jobDTO.jobDescription   = [gigDic valueForKey:@"description"];
        jobDTO.jobSubtitle1     = [gigDic valueForKey:@"sub_title1"];
        jobDTO.jobSubtitle2     = [gigDic valueForKey:@"sub_title2"];
        jobDTO.jobImageUrl         = [gigDic valueForKey:@"gig_image"];
        
        [self.gigsArray addObject:jobDTO];
    }
    [self.collectionView reloadData];
    
}

-(void)showAlert:(NSString *)message{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             [self dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    [alert addAction:ok];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"segueGigDetails"]){
        
           GigDetailsViewController *detailController   = (GigDetailsViewController *)[segue destinationViewController];
            detailController.job                        = self.selectedJob;
    }
}


@end
