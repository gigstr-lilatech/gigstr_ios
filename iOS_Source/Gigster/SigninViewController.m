//
//  SigninViewController.m
//  Gigster
//
//  Created by EFutures on 11/10/15.
//  Copyright © 2015 EFutures. All rights reserved.
//

#import "SigninViewController.h"
#import "User.h"
#import "Utility.h"
#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface SigninViewController ()<UITextFieldDelegate>

@property(nonatomic,weak)IBOutlet UITextField *txtEmail;
@property(nonatomic,weak)IBOutlet UITextField *txtPassword;

@property(nonatomic,weak)IBOutlet UIButton *btnSignin;
@property(nonatomic,weak)IBOutlet UIButton *fbSignup;

@property(nonatomic,weak)IBOutlet UIActivityIndicatorView *activity;

@end

@implementation SigninViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setTextfieldLayers:_txtEmail];
    [self setTextfieldLayers:_txtPassword];
    
    [self setPlaceholderColor:_txtEmail];
    [self setPlaceholderColor:_txtPassword];
    
    [[_fbSignup layer] setCornerRadius:5.0f];
    [[_fbSignup layer] setMasksToBounds:YES];
    
    [self setButtonLayers:_btnSignin];
    _btnSignin.enabled = NO;
    
    [_activity setHidden:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self setupBackButton:@"Back"];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
//    if (self.isFromTabbar) {
//        [self setupBackButton:@"Back"];
//    }else{
//        [self.navigationController setNavigationBarHidden:NO animated:animated];
//    }
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupBackButton:(NSString *)title{
    
    UIImage* image = [UIImage imageNamed:@"back_arrow"];
    CGRect frame = CGRectMake(0, 0, 100, 50);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frame];
    [backButton setImage:image forState:UIControlStateNormal];
    [backButton setTitle:NSLocalizedString(title, nil) forState:UIControlStateNormal];
    CGFloat insetAmount = 4 / 2.0;
    backButton.imageEdgeInsets = UIEdgeInsetsMake(0, -insetAmount, 0, insetAmount);
    backButton.titleEdgeInsets = UIEdgeInsetsMake(0, insetAmount, 0, -insetAmount);
    backButton.contentEdgeInsets = UIEdgeInsetsMake(0, insetAmount - 50 , 0, insetAmount);
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem* backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backButtonItem;
    
    self.navigationItem.titleView = nil;
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    
}

-(void)backAction{
        
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


-(void)setTextfieldLayers:(UITextField *)textFld{
    
    [[textFld layer] setCornerRadius:5.0f];
    [[textFld layer] setMasksToBounds:YES];
    [[textFld layer] setBorderWidth:1.0f];
    [[textFld layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    
}


-(void)setButtonLayers:(UIButton *)button{
    
    [[button layer] setCornerRadius:5.0f];
    [[button layer] setMasksToBounds:YES];
}

-(IBAction)fblogingClicked:(id)sender{
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    [login
     logInWithReadPermissions: @[@"public_profile",@"user_birthday",@"email"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error %@",error.description);
         } else if (result.isCancelled) {
             NSLog(@"%@",result);
             [self showAlert:NSLocalizedString(@"Couldn’t connect to Facebook. Please try again.", nil)];
         } else {
             NSLog(@"%@",result);
             NSLog(@"Logged in");
             [self getDetailsAndLogin];
         }
     }];
}

-(void)getDetailsAndLogin{
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:[NSDictionary dictionaryWithObject:@"cover,picture.type(large),id,name,first_name,last_name,gender,birthday,email,location,hometown,bio,photos" forKey:@"fields"]]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         if (!error) {
             
             NSLog(@"%@",result);
             NSString *userID = [[FBSDKAccessToken currentAccessToken] userID];
             NSString *userName = [result objectForKeyNotNull:@"name"];
             NSString *email =  [result objectForKeyNotNull:@"email"];
             NSString *userImageURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [[FBSDKAccessToken currentAccessToken] userID]];
             NSString *gender = [[result objectForKeyNotNull:@"gender"] uppercaseString];
             // NSString *DOB = [result objectForKeyNotNull:@"birthday"];
             
             [_activity startAnimating];
             [_activity setHidden:NO];
             
             NSString *pushToken;
             if ([[NSUserDefaults standardUserDefaults] stringForKey:@"pushDeviceToken"]) {
                 
                 pushToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"pushDeviceToken"];
             }else{
                 
                 pushToken = @"";
             }
             
             NSString *emails = (email) ? email : @"";
             
             //NSString *dobFB = (DOB) ? DOB : @"";
             
             NSString *dateString = [result objectForKeyNotNull:@"birthday"];
             NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
             [dateFormatter setDateFormat:@"MM/dd/yyyy"];
             NSDate *dateFromString = [dateFormatter dateFromString:dateString];
             dateFromString = [dateFormatter dateFromString:dateString];
             
             NSString *date;
             NSString *month;
             NSString *year;
             
             if (dateFromString) {
                 
                 NSDateFormatter *dateF1 = [[NSDateFormatter alloc] init];
                 [dateF1 setDateFormat:@"yyyy"];
                 year = [dateF1 stringFromDate:dateFromString];
                 
                 NSDateFormatter *dateF2 = [[NSDateFormatter alloc] init];
                 [dateF2 setDateFormat:@"MM"];
                 month = [dateF2 stringFromDate:dateFromString];
                 
                 NSDateFormatter *dateF3 = [[NSDateFormatter alloc] init];
                 [dateF3 setDateFormat:@"dd"];
                 date = [dateF3 stringFromDate:dateFromString];
             }
             else{
                 
                 NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                 [dateFormatter setDateFormat:@"MM/dd"];
                 NSDate *dateFromString = [[NSDate alloc] init];
                 dateFromString = [dateFormatter dateFromString:dateString];
                 
                 year = @"";
                 
                 NSDateFormatter *dateF2 = [[NSDateFormatter alloc] init];
                 [dateF2 setDateFormat:@"MM"];
                 month = [dateF2 stringFromDate:dateFromString];
                 
                 NSDateFormatter *dateF3 = [[NSDateFormatter alloc] init];
                 [dateF3 setDateFormat:@"dd"];
                 date = [dateF3 stringFromDate:dateFromString];
                 
             }
             
             NSString *dateX = (date) ? date : @"";
             NSString *monthX = (month) ? month : @"";
             NSString *yearX = (year) ? year : @"";
             
             NSString *genderFB;
             
             if ([gender isEqualToString:@"MALE"]) {
                 genderFB = @"M";
             }else if ([gender isEqualToString:@"FEMALE"]){
                 genderFB = @"F";
             }else{
                 genderFB = @"";
             }
             
             
             NSDictionary *params    = @{@"fbid": userID, @"name": userName, @"email":emails, @"day": dateX, @"month": monthX, @"year": yearX, @"gender": genderFB, @"accountType": [NSNumber numberWithInt:1], @"device_id":pushToken, @"device_type": @"2", @"image" :userImageURL};
             
             //             NSDictionary *params    = @{@"fbid": @"10156311001425287", @"name": @"Rasmus Solholm", @"email":@"solholmrasmus@gmail.com", @"day": dateX, @"month": monthX, @"year": yearX, @"gender": genderFB, @"accountType": [NSNumber numberWithInt:1], @"device_id":pushToken, @"device_type": @"2", @"image" :userImageURL};
             
             [User loginWithFacebookid:params completion:^(BOOL success, NSDictionary *data, NSError *error) {
                 
                 [_activity stopAnimating];
                 [_activity setHidden:YES];
                 
                 NSLog(@"success: %d", success);
                 NSLog(@"data: %@", data);
                 NSLog(@"Error: %@", error);
                 
                 if (success) {
                     
                     User *user         = [[User alloc] init];
                     user.userid        = [NSString stringWithFormat:@"%@",[data valueForKey:@"user_id"]];
                     user.token         = [NSString stringWithFormat:@"%@",[data valueForKey:@"token"]];
                     user.email         = email;
                     user.name          = userName;
                     user.userImageURL  = userImageURL;
                     //user.password    = password;
                     
                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                     [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:user] forKey:@"userObject"];
                     [defaults synchronize];
                     
                     UIStoryboard *storyboard    = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                     UITabBarController *tabbar  = [storyboard instantiateViewControllerWithIdentifier:@"Main_Tabbar"];//TabBarSID
                     tabbar.selectedIndex        = 0;
                     
                     AppDelegate *appDelegate                = [[UIApplication sharedApplication] delegate];
                     appDelegate.window.rootViewController   = tabbar;
                     [appDelegate.window makeKeyAndVisible];
                 }
                 
                 else{
                     
                     if ([data valueForKey:@"message"]) {
                         NSString *status = [NSString stringWithFormat:@"%@",[data valueForKey:@"message"]];
                         [self showAlert:status];
                         
                     }else{
                         [self showAlert:NSLocalizedString(@"Could not connect to the server. Please try again", nil)];
                     }
                 }
                 
             }];
         }
         else{
             
             NSLog(@"%@",error.localizedDescription);
             [self showAlert:error.localizedDescription];
         }
     }];
    
}


-(IBAction)signinClicked:(id)sender{
    
    [self.view endEditing:YES];
    
    NSString *email     = _txtEmail.text;
    NSString *password  = _txtPassword.text;
    int accountType     = 0;
    
    BOOL isValid = TRUE;
    
    isValid = [Utility validateTextField:self.txtEmail] * isValid;
    isValid = [Utility validateTextField:self.txtPassword] * isValid;
    
    if (!isValid) {
        
        [self showAlert:@"Required fields are missing!" withTitle:@""];
        
        return;
    }
    
    if (![Utility isValidEmail:email]) {
        
        [self showAlert:@"Please enter valid email!" withTitle:@""];
        
        return;
    }
    
    [_activity startAnimating];
    [_activity setHidden:NO];
    [self.view setUserInteractionEnabled:NO];
    
    [User loginWithEmail:email password:password accountType:accountType completion:^(BOOL success, NSDictionary *data, NSError *error) {
        
        [_activity stopAnimating];
        [_activity setHidden:YES];
        [self.view setUserInteractionEnabled:YES];
        
        NSLog(@"success: %d", success);
        NSLog(@"data: %@", data);
        NSLog(@"Error: %@", error);
        
        if (success) {
            
            User *user      = [[User alloc] init];
            user.userid     = [NSString stringWithFormat:@"%@",[data valueForKey:@"user_id"]];
            user.token      = [NSString stringWithFormat:@"%@",[data valueForKey:@"token"]];
            user.email      = email;
            user.password   = password;
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:user] forKey:@"userObject"];
            [defaults synchronize];
            
            if (self.isFromTabbar) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"dismissPopup" object:nil];
                
                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                
            }else{
                
                UIStoryboard *storyboard    = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                UITabBarController *tabbar  = [storyboard instantiateViewControllerWithIdentifier:@"Main_Tabbar"];//TabBarSID
                tabbar.selectedIndex        = 0;
                
                AppDelegate *appDelegate                = [[UIApplication sharedApplication] delegate];
                appDelegate.window.rootViewController   = tabbar;
                [appDelegate.window makeKeyAndVisible];
            }
           
        }
        
        else{
            
            if ([data valueForKey:@"message"]) {
                NSString *status = [NSString stringWithFormat:@"%@",[data valueForKey:@"message"]];
                [self showAlert:status withTitle:@""];
                
            }else{
                [self showAlert:NSLocalizedString(@"Could not connect to the server. Please try again", nil) withTitle:@""];
            }
            
            
//            if ([[NSString stringWithFormat:@"%@",[data valueForKey:@"status"]] isEqualToString:@"105"]) {
//                
//                NSString *status = [NSString stringWithFormat:@"%@",[data valueForKey:@"message"]];
//                [self showAlert:@"Incorrect email address or password. Please try again." withTitle:status];
//            }else{
//                
//                [self showAlert:[data valueForKey:@"message"] withTitle:@""];
//            }
           
        }
        
    }];
    
}

-(IBAction)forgotPasswordClicked:(id)sender{
    
    [self performSegueWithIdentifier:@"segueForgot" sender:self];
    
}

-(void)showAlert:(NSString *)message withTitle:(NSString *)title{

    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    [alert addAction:ok];

}

-(void)setPlaceholderColor:(UITextField *)textField{
    
    if ([textField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        //UIColor *color = [UIColor colorWithRed:224.0f/255.0f green:244.0f/255.0f blue:244.0f/255.0f alpha:1.0];
        UIColor *color = [UIColor colorWithRed:246.0f/255.0f green:246.0f/255.0f blue:246.0f/255.0f alpha:1.0];
        textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
    } else {
        NSLog(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
        // TODO: Add fall-back code to set placeholder color.
    }
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
     NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self validateForButton:newString];
    return YES;
}

-(void)validateForButton:(NSString *)text{
    
    BOOL isValid = TRUE;
    
    isValid = [Utility validateTextField:self.txtEmail] * isValid;
    isValid = [Utility isValidEmail:self.txtEmail.text] * isValid;
    isValid = [Utility validateTextField:self.txtPassword] * isValid;
    isValid = [self minimumCharacterValidation:text] * isValid;
    
    if (isValid) {
        _btnSignin.enabled = YES;
    }else{
        _btnSignin.enabled = NO;
    }
    
}

-(BOOL)minimumCharacterValidation:(NSString *)text{
    
    if ([text length] >= 6) {
        
        return TRUE;
    }else{
        
        return  FALSE;
    }
}

-(void)showAlert:(NSString *)message{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                         }];
    [alert addAction:ok];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
