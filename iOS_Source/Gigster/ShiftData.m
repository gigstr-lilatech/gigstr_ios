//
//  ShiftData.m
//  Gigster
//
//  Created by Rakitha Perera on 5/24/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import "ShiftData.h"

@implementation ShiftData

-(id)initWithDic:(NSDictionary *)data
{
    self = [super init];
    if (self) {
        self.field_id = data[@"field_id"];
        self.field_name = data[@"field_name"];
        self.field_value = data[@"field_value"];
        self.field_type = data[@"field_type"];
        if ([self.field_type isEqualToString:ShiftData_DROPDOWN]) {
            NSMutableArray * arr = [NSMutableArray arrayWithArray:data[@"field_data"]];
            
            // Add a empty value object
            NSDictionary *dict = @{
                                   @"label": @"Not set",
                                   @"value": @"UnvfEH9UVI"
                                   };
            [arr insertObject:dict atIndex:0];
            self.field_data = arr;
        }else if ([self.field_type isEqualToString:ShiftData_CHECKBOX]) {
            self.field_parent = data[@"field_parent"];
        } else {
            self.field_data = data[@"field_data"];
        }
        
        self.required = [data[@"required"] boolValue];
        self.errorMessage = @"";
    }
    return self;
}

- (id)toDictionary {
    return [NSDictionary dictionaryWithObjectsAndKeys:
            self.field_name, @"field_name",
            self.field_value, @"field_value",
            self.field_type, @"field_type",
            self.field_id, @"field_id",
            nil];
}

@end
