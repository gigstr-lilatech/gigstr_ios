//
//  Explore.m
//  Gigster
//
//  Created by Rakitha Perera on 8/8/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import "Explore.h"
#import "Api.h"

@implementation Explore

+ (void) listAllFeedForSection:(NSString *)section completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion{
    
    NSDictionary *params    = @{@"section": section};
    NSDictionary *data      = @{@"url": @"feed", @"parameters": params};
    
    [Api post:data onCompletion:^(BOOL success, NSDictionary *response, NSError *error) {
        NSLog(@"success: %d", success);
        NSLog(@"respose: %@", response);
        NSLog(@"Error: %@", error);
        
        if (success) {
            
        }
        
        completion(success, response, error);
        
    }];
}

-(id)initWithDic:(NSDictionary *)data
{
    self = [super init];
    if (self) {
        self.explore_id = data[@"id"];
        self.section = data[@"section"];
        self.component = data[@"component"];
        self.title = data[@"title"];
        self.action = data[@"action"];
        self.url = data[@"url"];

        if ([self.component isEqualToString:EXPLORE_CAROUSEL]) {
            NSMutableArray * dataArray = [NSMutableArray arrayWithArray:data[@"items"]];
            
            NSMutableArray *tempArr = NSMutableArray.new;
            for (NSDictionary *dataDic in dataArray) {
                CarouselItem *item = [[CarouselItem alloc] initWithDic:dataDic];
                [tempArr addObject:item];
            }
            
            self.items = [NSArray arrayWithArray:tempArr];
        }
    }
    return self;
}

+ (NSDictionary *)getExploreTestData {
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"our-world" ofType:@"json"];
    NSData *content = [[NSData alloc] initWithContentsOfFile:filePath];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:content options:kNilOptions error:nil];
    return json;
}

+ (NSDictionary *)getClientTestData {
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"client" ofType:@"json"];
    NSData *content = [[NSData alloc] initWithContentsOfFile:filePath];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:content options:kNilOptions error:nil];
    return json;
}

@end
