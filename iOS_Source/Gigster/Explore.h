//
//  Explore.h
//  Gigster
//
//  Created by Rakitha Perera on 8/8/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CarouselItem.h"

#define EXPLORE_CAROUSEL @"carousel"
#define EXPLORE_LINK @"link"
#define EXPLORE_BUTTON @"button"

#define ACTION_WEB @"web"
#define ACTION_YOUTUBE @"youtube"

@interface Explore : NSObject

@property (nonatomic, strong) NSNumber* explore_id;
@property (nonatomic, strong) NSString* section;
@property (nonatomic, strong) NSString* component;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* action;
@property (nonatomic, strong) NSString* url;
@property (nonatomic, strong) NSArray* items;

-(id)initWithDic:(NSDictionary *)data;
+ (NSDictionary *)getExploreTestData;
+ (NSDictionary *)getClientTestData;

+ (void) listAllFeedForSection:(NSString *)section completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion;
@end
