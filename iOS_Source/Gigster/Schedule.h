//
//  Schedule.h
//  Gigster
//
//  Created by Rakitha Perera on 7/6/16.
//  Copyright © 2016 EFutures. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ShiftData.h"

@interface Schedule : NSObject

@property NSString* scheduleId;
@property NSDate* date;
@property NSString* name;
@property NSDate* startTime;
@property NSDate* endTime;
@property NSString* details;
@property int status;
@property int gigstr_rating;
@property int internal_rating;
@property NSString* gigstr_rating_comment;
@property NSString* internal_rating_comment;
@property NSString* comment;
@property NSString* internal_user;
@property NSString* gigstr_name;
@property NSDate* gigstr_start;
@property NSDate* gigstr_stop;
@property NSString* gigstr_image;
@property NSArray* shift_data;
@property BOOL shift_data_editable;
@property int schedule_image_count;

-(void)addShiftData:(NSArray *)dataArray;
+ (NSDictionary *)getShceduleDetailsTestData;

+ (void) listSchedules:(NSString *) userID token:(NSString *) token pageID:(NSString *)pageID date:(NSString *)date completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion;

+ (void)loadMoreSchedules:(NSString *) userID token:(NSString *) token pageID:(int)pageID date:(NSString *)date lastScheduleID:(NSString *)lastID completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion;

+ (void)getScheduleDetails:(NSString *)scheduleId user:(NSString *) userID token:(NSString *) token completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion;

+ (void)checkINSchedule:(NSString *)scheduleId user:(NSString *) userID token:(NSString *) token
               latitude:(NSString *)latitude
             longtitude:(NSString *)longtitude completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion;

+ (void)checkOUTSchedule:(NSString *)scheduleId
                    user:(NSString *) userID
                   token:(NSString *)token
                   start:(NSDate *)startDate
                    stop:(NSDate *)stopDate
                 comment:(NSString *)comment
                latitude:(NSString *)latitude
              longtitude:(NSString *)longtitude
                shifData:(NSArray *)shiftData
              completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion;

+ (void)rateSchedule:(NSString *)scheduleID
               token:(NSString *)token
              userID:(NSString *)userID
                rate:(int)rate
            feedback:(NSString *)feedback
          completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion;
    
+ (void)editWorkLogSchedule:(NSString *)scheduleID token:(NSString *)token userID:(NSString *)userID comment:(NSString *)comment completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion;
    
+ (NSURLSessionUploadTask *)uploadImagesForSchedule:(NSString *)scheduleID userID:(NSString *)userID image:(UIImage *)image progress:(void (^_Nullable)(NSProgress * _Nonnull uploadProgress))progress completion:(void (^_Nullable)(NSURLResponse * _Nullable response, id _Nullable responseObject, NSError * _Nullable error))completion;

+ (void)uploadImagesForSchedule:(NSString *_Nullable)scheduleID userID:(NSString *_Nullable)userID imagePath:(NSString *_Nullable)imagePath image:(UIImage *_Nullable)image progress:(void (^_Nullable)(NSProgress * _Nonnull uploadProgress))progress completion:(void (^_Nullable)(NSURLResponse * _Nullable response, id _Nullable responseObject, NSError * _Nullable error))completion;

+ (void)editShiftDataForSchedule:(NSString *_Nullable)scheduleID token:(NSString *_Nullable)token userID:(NSString *_Nullable)userID shiftData:(NSArray *_Nullable)shiftData completion:(void (^_Nullable)(BOOL success, NSDictionary * _Nullable data, NSError * _Nullable error))completion;
    
@end
