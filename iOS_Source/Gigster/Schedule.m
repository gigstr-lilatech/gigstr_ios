//
//  Schedule.m
//  Gigster
//
//  Created by Rakitha Perera on 7/6/16.
//  Copyright © 2016 EFutures. All rights reserved.
//

#import "Schedule.h"
#import "Api.h"
#import "NSDateFormatter+Addtions.h"
#import "HTTPClient.h"

@implementation Schedule

-(void)addShiftData:(NSArray *)dataArray {
    NSMutableArray *tempArr = NSMutableArray.new;
    for (NSDictionary *shiftDataDic in dataArray) {
        ShiftData *shiftData = [[ShiftData alloc] initWithDic:shiftDataDic];
        [tempArr addObject:shiftData];
    }
    
    self.shift_data = [NSArray arrayWithArray:tempArr];
}

+ (void) listSchedules:(NSString *) userID token:(NSString *) token pageID:(NSString *)pageID date:(NSString *)date completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion{
    
    NSDictionary *params    = @{@"user_id": userID, @"token": token, @"page": pageID, @"date": date, @"device_type" : @"2"};
    NSDictionary *data      = @{@"url": @"schedule/list_all", @"parameters": params};
    
    [Api post:data onCompletion:^(BOOL success, NSDictionary *response, NSError *error) {
        NSLog(@"success: %d", success);
        NSLog(@"respose: %@", response);
        NSLog(@"Error: %@", error);
        completion(success, response, error);
        
    }];
    
}

+ (void)loadMoreSchedules:(NSString *) userID token:(NSString *) token pageID:(int)pageID date:(NSString *)date lastScheduleID:(NSString *)lastID completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion{
    
    NSDictionary *params    = @{@"user_id": userID, @"token": token, @"page": @(pageID), @"date": date, @"last_id" : lastID, @"device_type" : @"2"};
    NSDictionary *data      = @{@"url": @"schedule/list_all", @"parameters": params};
    
    [Api post:data onCompletion:^(BOOL success, NSDictionary *response, NSError *error) {
        NSLog(@"success: %d", success);
        NSLog(@"respose: %@", response);
        NSLog(@"Error: %@", error);
        completion(success, response, error);
        
    }];
    
}

+ (void)getScheduleDetails:(NSString *)scheduleId user:(NSString *) userID token:(NSString *) token completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion {
    
    NSDictionary *params    = @{@"user_id": userID, @"token": token, @"schedule_id": scheduleId, @"device_type" : @"2"};
//    NSDictionary *data      = @{@"url": @"schedule/scheduleDetail", @"parameters": params};
    NSDictionary *data      = @{@"url": @"schedule/scheduleDetail", @"parameters": params};
    
    [Api post:data onCompletion:^(BOOL success, NSDictionary *response, NSError *error) {
        NSLog(@"success: %d", success);
        NSLog(@"respose: %@", response);
        NSLog(@"Error: %@", error);
        completion(success, response, error);
        
    }];
}

+ (void)checkINSchedule:(NSString *)scheduleId user:(NSString *) userID token:(NSString *) token
               latitude:(NSString *)latitude
             longtitude:(NSString *)longtitude
             completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion{
    NSDictionary *params    = @{@"user_id": userID, @"token": token, @"schedule_id": scheduleId, @"device_type" : @"2",
                                @"lat" : latitude,
                                @"long" : longtitude};
    NSDictionary *data      = @{@"url": @"schedule/checkin", @"parameters": params};
    
    [Api post:data onCompletion:^(BOOL success, NSDictionary *response, NSError *error) {
        NSLog(@"success: %d", success);
        NSLog(@"respose: %@", response);
        NSLog(@"Error: %@", error);
        completion(success, response, error);
        
    }];
    
}

+ (void)checkOUTSchedule:(NSString *)scheduleId
                    user:(NSString *) userID
                   token:(NSString *)token
                   start:(NSDate *)startDate
                    stop:(NSDate *)stopDate
                 comment:(NSString *)comment
                latitude:(NSString *)latitude
              longtitude:(NSString *)longtitude
                shifData:(NSArray *)shiftData
              completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion {
    
   
    NSMutableArray *formatedArr = NSMutableArray.new;
    for (ShiftData *data in shiftData) {
        [formatedArr addObject:[data toDictionary]];
    }
    
    NSDictionary *params    = @{@"user_id": userID,
                                @"token": token,
                                @"schedule_id": scheduleId,
                                @"device_type" : @"2",
                                @"start" : [[NSDateFormatter serverDateFormatter] stringFromDate:startDate],
                                @"stop" : [[NSDateFormatter serverDateFormatter] stringFromDate:stopDate],
                                @"comment" : comment,
                                @"lat" : latitude,
                                @"long" : longtitude,
                                @"gig_data": formatedArr};
    NSDictionary *data      = @{@"url": @"schedule/checkout", @"parameters": params};
    
    [Api post:data onCompletion:^(BOOL success, NSDictionary *response, NSError *error) {
        NSLog(@"success: %d", success);
        NSLog(@"respose: %@", response);
        NSLog(@"Error: %@", error);
        completion(success, response, error);
        
    }];
    
}

+ (void)rateSchedule:(NSString *)scheduleID token:(NSString *)token userID:(NSString *)userID rate:(int)rate feedback:(NSString *)feedback completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion{
    
    NSDictionary *params    = @{@"gigstr_rating": @(rate), @"token": token, @"schedule_id": scheduleID, @"gigstr_rating_comment": feedback, @"user_id" : userID, @"device_type" : @"2"};
    NSDictionary *data      = @{@"url": @"schedule/rating", @"parameters": params};
    
    [Api post:data onCompletion:^(BOOL success, NSDictionary *response, NSError *error) {
        NSLog(@"success: %d", success);
        NSLog(@"respose: %@", response);
        NSLog(@"Error: %@", error);
        completion(success, response, error);
        
    }];
    
}

+ (void)editWorkLogSchedule:(NSString *)scheduleID token:(NSString *)token userID:(NSString *)userID comment:(NSString *)comment completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion{
    
    NSDictionary *params    = @{@"token": token, @"schedule_id": scheduleID, @"comment": comment, @"user_id" : userID, @"device_type" : @"2"};
    NSDictionary *data      = @{@"url": @"schedule/updateCheckout", @"parameters": params};
    
    [Api post:data onCompletion:^(BOOL success, NSDictionary *response, NSError *error) {
        NSLog(@"success: %d", success);
        NSLog(@"respose: %@", response);
        NSLog(@"Error: %@", error);
        completion(success, response, error);
        
    }];
    
}

+ (NSURLSessionUploadTask *)uploadImagesForSchedule:(NSString *)scheduleID userID:(NSString *)userID image:(UIImage *)image progress:(void (^_Nullable)(NSProgress * _Nonnull uploadProgress))progress completion:(void (^_Nullable)(NSURLResponse * _Nullable response, id _Nullable responseObject, NSError * _Nullable error))completion{
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *url = [NSString stringWithFormat:@"%@schedule-image/add", API_URL];
    
    NSData *imageData = UIImageJPEGRepresentation(image,1.0);
    NSDictionary *params = @{@"user_id": userID,
                             @"schedule_id": scheduleID};
    
    
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imageData name:@"image_data" fileName:@"fileName.jpg" mimeType:@"image/jpeg"];
    } error:nil];
    
    AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
    
    [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [HTTPClient sharedClient].uploadManager.requestSerializer = requestSerializer;
    [HTTPClient sharedClient].uploadManager.responseSerializer.acceptableContentTypes = [[HTTPClient sharedClient].uploadManager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    //
    NSURLSessionUploadTask *uploadTask;
    __block BOOL completeBlockCalled = NO;
    __block NSError *errorTask;
    
    NSURLSessionUploadTask *task = [[[HTTPClient sharedClient] uploadManager] uploadTaskWithStreamedRequest:request progress:^(NSProgress * _Nonnull uploadProgress) {
        // This is not called back on the main queue.
        // You are responsible for dispatching to the main queue for UI updates
        dispatch_async(dispatch_get_main_queue(), ^{
            progress(uploadProgress);
            NSLog(@"Total Conut: %lld / Progress :%f", uploadProgress.totalUnitCount, uploadProgress.fractionCompleted);
        });
    } completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        errorTask = error;
        completeBlockCalled = YES;
        if (error) {
            NSLog(@"Error: %@", error);
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        } else {
            NSLog(@"%@ %@", response, responseObject);
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }
        completion(response, responseObject, error);
        
    }];
    
    [task resume];
    
    return uploadTask;
}

+ (void)uploadImagesForSchedule:(NSString *)scheduleID userID:(NSString *)userID imagePath:(NSString *)imagePath image:(UIImage *)image progress:(void (^_Nullable)(NSProgress * _Nonnull uploadProgress))progress completion:(void (^_Nullable)(NSURLResponse * _Nullable response, id _Nullable responseObject, NSError * _Nullable error))completion{
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *url = [NSString stringWithFormat:@"%@schedule-image/add", API_URL];
    NSData *imageData = UIImageJPEGRepresentation(image,1.0);
    NSDictionary *params = @{@"user_id": userID,
                             @"schedule_id": scheduleID};
    
    NSString *tempFile = [NSTemporaryDirectory() stringByAppendingPathComponent:imagePath];
    NSURL *filePathtemp = [NSURL fileURLWithPath:tempFile];
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imageData name:@"image_data" fileName:@"fileName.jpg" mimeType:@"image/jpeg"];
    } error:nil];

    
    AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
    
    [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
//    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [[HTTPClient sharedClient] configureBackgroundSessionCompletion];
    });
    
    __block BOOL completeBlockCalled = NO;
    __block NSError *errorTask;
    [[AFHTTPRequestSerializer serializer] requestWithMultipartFormRequest:request writingStreamContentsToFile:filePathtemp completionHandler:^(NSError * _Nullable error) {
        NSURLSessionUploadTask *task = [[HTTPClient sharedClient].downloadManager uploadTaskWithRequest:request fromFile:filePathtemp progress:^(NSProgress * _Nonnull uploadProgress) {
            dispatch_async(dispatch_get_main_queue(), ^{
                progress(uploadProgress);
                NSLog(@"Total Conut: %lld / Progress :%f", uploadProgress.totalUnitCount, uploadProgress.fractionCompleted);
            });

        } completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
            errorTask = error;
            completeBlockCalled = YES;
            if (error) {
                NSLog(@"Error: %@", error);
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            } else {
                NSLog(@"%@ %@", response, responseObject);
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            }
            completion(response, responseObject, error);

        }];
        [task resume];
    }];
    
    //    [[HTTPClient sharedClient] configureBackgroundSessionCompletion];
        [HTTPClient sharedClient].backgroundSessionCompletionHandler = ^{
    
            NSLog(@"===== background session completed in ViewController ===== ");
    
//            UILocalNotification* localNotification = [[UILocalNotification alloc] init];
//            localNotification.alertBody = @"Download Complete!";
//            localNotification.alertAction = @"Background Transfer Download!";
//    
//            //On sound
//            localNotification.soundName = UILocalNotificationDefaultSoundName;
//    
//            //increase the badge number of application plus 1
//            localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
//    
//            [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
    
        };
}

+ (void)editShiftDataForSchedule:(NSString *)scheduleID token:(NSString *)token userID:(NSString *)userID shiftData:(NSArray *)shiftData completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion{
    
    NSMutableArray *formatedArr = NSMutableArray.new;
    for (ShiftData *data in shiftData) {
        [formatedArr addObject:[data toDictionary]];
    }
    NSDictionary *params    = @{@"token": token, @"schedule_id": scheduleID, @"gig_data": formatedArr, @"user_id" : userID, @"device_type" : @"2"};
    NSDictionary *data      = @{@"url": @"schedule/updateShiftData", @"parameters": params};
    
    [Api post:data onCompletion:^(BOOL success, NSDictionary *response, NSError *error) {
        NSLog(@"success: %d", success);
        NSLog(@"respose: %@", response);
        NSLog(@"Error: %@", error);
        completion(success, response, error);
        
    }];
    
}
                  
- (NSString *)documentsPathForFileName:(NSString *)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    
    return [documentsPath stringByAppendingPathComponent:name];
}
    
+ (NSDictionary *)getShceduleDetailsTestData {
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"ScheduleShiftTestData" ofType:@"json"];
    NSData *content = [[NSData alloc] initWithContentsOfFile:filePath];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:content options:kNilOptions error:nil];
    return json;
}
                                    
@end
