//
//  WhatsNewViewController.h
//  Gigster
//
//  Created by Rakitha Perera on 2/1/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString * const khaveShownWhatsNewKey = @"khaveShownWhatsNewKey";
static NSString * const kLastShowedVersionKey = @"kLastShowedVersionKey";

@interface WhatsNewViewController : UIViewController

@end
