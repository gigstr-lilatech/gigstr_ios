//
//  NSDateFormatter+Addtions.m
//  Gigster
//
//  Created by Rakitha Perera on 7/6/16.
//  Copyright © 2016 EFutures. All rights reserved.
//

#import "NSDateFormatter+Addtions.h"

@implementation NSDateFormatter (Addtions)

+ (NSDateFormatter*) serverDateFormatter{
    static NSDateFormatter *_serverDateFormatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _serverDateFormatter = [[NSDateFormatter alloc] init];
//        NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"Europe/Stockholm"];
//        [_serverDateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        
        [_serverDateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        [_serverDateFormatter setDateFormat:@"yyy-MM-dd HH:mm:ss"];
//        _serverDateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
//        NSString * deviceLanguage = [[NSLocale preferredLanguages] objectAtIndex:0];
//        NSLocale * locale = [[NSLocale alloc] initWithLocaleIdentifier:deviceLanguage];
//        [_serverDateFormatter setLocale:locale];
        
        [_serverDateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
        
    });
    return _serverDateFormatter;
}

+ (NSDateFormatter*) dafaultDateFormatter {
    static NSDateFormatter *_dafaultDateFormatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _dafaultDateFormatter = [[NSDateFormatter alloc] init];
        [_dafaultDateFormatter setDateFormat:@"EEEE"];
        NSString * deviceLanguage = [[NSLocale preferredLanguages] objectAtIndex:0];
        NSLocale * locale = [[NSLocale alloc] initWithLocaleIdentifier:deviceLanguage];
        [_dafaultDateFormatter setLocale:locale];
//        [_dafaultDateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
//        _dafaultDateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:[NSTimeZone localTimeZone].secondsFromGMT];
        //Force to use sweden timezone
        _dafaultDateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:3600];
        
    });
    
    return _dafaultDateFormatter;
}



@end
