//
//  SignupViewController.m
//  Gigster
//
//  Created by EFutures on 11/10/15.
//  Copyright © 2015 EFutures. All rights reserved.
//

#import "SignupViewController.h"
#import "User.h"
#import "Utility.h"
#import "AppDelegate.h"

@interface SignupViewController ()<UITextFieldDelegate>

@property(nonatomic,weak)IBOutlet UITextField *txtName;
@property(nonatomic,weak)IBOutlet UITextField *txtEmail;
@property(nonatomic,weak)IBOutlet UITextField *txtPw;
@property(nonatomic,weak)IBOutlet UITextField *txtRptPw;

@property(nonatomic,weak)IBOutlet UILabel *lbl;

@property(nonatomic,weak)IBOutlet UIButton *btnCreate;

@property(nonatomic,weak)IBOutlet UIActivityIndicatorView *activity;


@end

NSString *tempTxtFldValue;

@implementation SignupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self setTextfieldLayers:_txtName];
    [self setTextfieldLayers:_txtEmail];
    [self setTextfieldLayers:_txtPw];
    [self setTextfieldLayers:_txtRptPw];

    [self setButtonLayers:_btnCreate];
    _btnCreate.enabled = NO;
    
    [self setPlaceholderColor:_txtName];
    [self setPlaceholderColor:_txtEmail];
    [self setPlaceholderColor:_txtPw];
    [self setPlaceholderColor:_txtRptPw];
    
    [_activity setHidden:YES];
   
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self setupBackButton:@"Back"];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    
//    if (self.isFromTabbar) {
//        
//    }else{
//     
//    }
}

-(void)setupBackButton:(NSString *)title{
    
    UIImage* image = [UIImage imageNamed:@"back_arrow"];
    CGRect frame = CGRectMake(0, 0, 100, 50);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frame];
    [backButton setImage:image forState:UIControlStateNormal];
    [backButton setTitle:NSLocalizedString(title, nil) forState:UIControlStateNormal];
    CGFloat insetAmount = 4 / 2.0;
    backButton.imageEdgeInsets = UIEdgeInsetsMake(0, -insetAmount, 0, insetAmount);
    backButton.titleEdgeInsets = UIEdgeInsetsMake(0, insetAmount, 0, -insetAmount);
    backButton.contentEdgeInsets = UIEdgeInsetsMake(0, insetAmount - 50 , 0, insetAmount);
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem* backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backButtonItem;
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    
}

-(void)backAction{
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

-(void) viewWillDisappear:(BOOL)animated {
    
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
       
        _txtPw.delegate = nil;
    }
    [super viewWillDisappear:animated];
}


-(void)setTextfieldLayers:(UITextField *)textFld{
    
    [[textFld layer] setCornerRadius:5.0f];
    [[textFld layer] setMasksToBounds:YES];
    [[textFld layer] setBorderWidth:1.0f];
    [[textFld layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    
}

-(void)setButtonLayers:(UIButton *)button{
    
    [[button layer] setCornerRadius:5.0f];
    [[button layer] setMasksToBounds:YES];
}

-(IBAction)signupClicked:(id)sender{
    
    [self.view endEditing:YES];
    
    NSString *name      = _txtName.text;
    NSString *email     = _txtEmail.text;
    NSString *password  = _txtPw.text;
    int accountType     = 0;
    
    BOOL isValid = TRUE;
    
    isValid = [Utility validateTextField:self.txtName] * isValid;
    isValid = [Utility validateTextField:self.txtEmail] * isValid;
    isValid = [Utility validateTextField:self.txtPw] * isValid;
    isValid = [Utility validateTextField:self.txtRptPw] * isValid;

    if (!isValid) {
        
        [self showAlert:@"Required fields are missing!"];
        
        return;
    }
    
    if (![Utility isValidEmail:email]) {
        
        [self showAlert:@"Please enter valid email!"];
        
        return;
    }
    
    if (![_txtPw.text isEqualToString:_txtRptPw.text]) {
        
        [self showAlert:@"These passwords don’t match. Please try again"];
    
        return;
    }
    
    [_activity startAnimating];
    [_activity setHidden:NO];
    [self.view setUserInteractionEnabled:NO];
    
    [User userRegisterWithName:name email:email password:password accountType:accountType completion:^(BOOL success, NSDictionary *data, NSError *error) {
        
        [_activity stopAnimating];
        [_activity setHidden:YES];
        [self.view setUserInteractionEnabled:YES];
        
        if (success) {
            
            User *user      = [[User alloc] init];
            user.userid     = [NSString stringWithFormat:@"%@",[data valueForKey:@"user_id"]];
            user.token      = [NSString stringWithFormat:@"%@",[data valueForKey:@"token"]];
            user.email      = email;
            user.name       = name;
            user.password   = password;
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:user] forKey:@"userObject"];
            [defaults synchronize];
            
            if (self.isFromTabbar) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"dismissPopup" object:nil];
                
                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                
            }else{
                UIStoryboard *storyboard    = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                UITabBarController *tabbar  = [storyboard instantiateViewControllerWithIdentifier:@"Main_Tabbar"];//TabBarSID
                tabbar.selectedIndex        = 0;
                
                AppDelegate *appDelegate                = [[UIApplication sharedApplication] delegate];
                appDelegate.window.rootViewController   = tabbar;
                [appDelegate.window makeKeyAndVisible];
                
            }
        }
        
        else{
            
            if ([data valueForKey:@"message"]) {
                NSString *status = [NSString stringWithFormat:@"%@",[data valueForKey:@"message"]];
                [self showAlert:status];
                _txtPw.text = @"";
                _txtRptPw.text = @"";
                
                [self validateForButton:nil];
                
            }else{
                [self showAlert:NSLocalizedString(@"Could not connect to the server. Please try again", nil)];
            }
        }
        
        
        NSLog(@"success: %d", success);
        NSLog(@"data: %@", data);
        NSLog(@"Error: %@", error);
        
        
    }];

}

-(BOOL)alphaNumericValidation:(NSString *)password{
    
    if ([password rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]].location != NSNotFound) {
       
        return YES;
    }else{
        
        return NO;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    if (textField == _txtPw) {
        
        if ([textField.text length] < 6) {
            
            [self showAlert:@"Password should be at least 6 characters long"];
            return NO;

        }

    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
   // NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (textField == _txtName) {
        
        // Prevent crashing undo bug – see note below.
        if(range.length + range.location > textField.text.length)
        {
            [self validateForButton:nil];
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        [self validateForButton:nil];
        return newLength <= 20;
    }
    else if (textField == _txtRptPw){
        
        [self validateForButton:_txtRptPw];
        return YES;
        
    }
    [self validateForButton:nil];
    return YES;
}

-(void)setPlaceholderColor:(UITextField *)textField{
    
    if ([textField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor colorWithRed:246.0f/255.0f green:246.0f/255.0f blue:246.0f/255.0f alpha:1.0];
        textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
    } else {
        NSLog(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
        // TODO: Add fall-back code to set placeholder color.
    }
    
}



-(void)showAlert:(NSString *)message{
    
    
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                         }];
    [alert addAction:ok];
    
}

-(void)validateForButton:(UITextField *)textField{
    
    BOOL isValid = TRUE;
    
    isValid = [Utility validateTextField:self.txtName] * isValid;
    isValid = [Utility validateTextField:self.txtEmail] * isValid;
    isValid = [Utility validateTextField:self.txtPw] * isValid;
    
    if (textField == _txtRptPw) {
        
        isValid = 1 * isValid;
    }else{
        isValid = [Utility validateTextField:self.txtRptPw] * isValid;
    }
    
    isValid = [Utility isValidEmail:self.txtEmail.text] * isValid;

  
    if (isValid) {
        
        _btnCreate.enabled = YES;
    }
    else{
        
        _btnCreate.enabled = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
