//
//  ChatDTO.m
//  Gigster
//
//  Created by EFutures on 12/10/15.
//  Copyright © 2015 EFutures. All rights reserved.
//

#import "ChatDTO.h"
#import "Api.h"


@implementation ChatDTO

@synthesize unreadMessage = _unreadMessage, message = _message;

- (NSString *)unreadMessage {
    return _unreadMessage;
}

- (void)setUnreadMessage:(NSString *)newValue {
    NSData *nsdataFromBase64String = [[NSData alloc]
                                      initWithBase64EncodedString:newValue options:0];
    // Decoded NSString from the NSData
    NSString *base64Decoded = [[NSString alloc]
                               initWithData:nsdataFromBase64String encoding:NSUTF8StringEncoding];
    base64Decoded = [base64Decoded stringByReplacingOccurrencesOfString:@"</br>"
                                                             withString:@"\n"];
    base64Decoded = [base64Decoded stringByReplacingOccurrencesOfString:@"<br>"
                                                             withString:@"\n"];
    _unreadMessage = base64Decoded;
}

- (NSString *)message {
    return _message;
}

- (void)setMessage:(NSString *)newValue {
    NSData *nsdataFromBase64String = [[NSData alloc]
                                      initWithBase64EncodedString:newValue options:0];
    // Decoded NSString from the NSData
    NSString *base64Decoded = [[NSString alloc]
                               initWithData:nsdataFromBase64String encoding:NSUTF8StringEncoding];
    base64Decoded = [base64Decoded stringByReplacingOccurrencesOfString:@"</br>"
                                                             withString:@"\n"];
    base64Decoded = [base64Decoded stringByReplacingOccurrencesOfString:@"<br>"
                                                           withString:@"\n"];
    _message = base64Decoded;
}

+ (void) getChatLiostforUser:(NSString *)userID token:(NSString *)token completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion{
    
    NSDictionary *params    = @{@"user_id": userID, @"token": token, @"device_type": @"2"};
    NSDictionary *data      = @{@"url": @"chat/list_all", @"parameters": params};
    
    [Api post:data onCompletion:^(BOOL success, NSDictionary *response, NSError *error) {
        NSLog(@"success: %d", success);
        NSLog(@"respose: %@", response);
        NSLog(@"Error: %@", error);
        
        if (success) {
            
            
        }
        
        completion(success, response, error);
        
    }];
    
}

+ (void) getChatMessageForUser:(NSString *)userID token:(NSString *)token chatID:(NSString *)chatID completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion{
    
    NSDictionary *params    = @{@"user_id": userID, @"token": token, @"chat_id": chatID, @"device_type": @"2"};
    NSDictionary *data      = @{@"url": @"chat/list_chat", @"parameters": params};
    
    [Api post:data onCompletion:^(BOOL success, NSDictionary *response, NSError *error) {
        NSLog(@"success: %d", success);
        NSLog(@"respose: %@", response);
        NSLog(@"Error: %@", error);
        
        if (success) {
            
            
        }
        
        completion(success, response, error);
        
    }];
}

+ (void) sendMessage:(NSString *)userID token:(NSString *)token chatID:(NSString *)chatID message:(NSString *)message completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion{
    
    NSDictionary *params    = @{@"user_id": userID, @"token": token, @"chat_id": chatID, @"message": message};
    NSDictionary *data      = @{@"url": @"chat/send", @"parameters": params};
    
    [Api post:data onCompletion:^(BOOL success, NSDictionary *response, NSError *error) {
        NSLog(@"success: %d", success);
        NSLog(@"respose: %@", response);
        NSLog(@"Error: %@", error);
        
        if (success) {
            
            
        }
        
        completion(success, response, error);
        
    }];
}

+ (void) getUnreadMessages:(NSString *)userID token:(NSString *)token chatID:(NSString *)chatID completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion{
    
    NSDictionary *params    = @{@"user_id": userID, @"token": token, @"chat_id": chatID, @"device_type": @"2"};
    NSDictionary *data      = @{@"url": @"chat/get_unread", @"parameters": params};
    
    [Api post:data onCompletion:^(BOOL success, NSDictionary *response, NSError *error) {
        NSLog(@"success: %d", success);
        NSLog(@"respose: %@", response);
        NSLog(@"Error: %@", error);
        
        if (success) {
            
            
        }
        
        completion(success, response, error);
        
    }];
    
}
@end
