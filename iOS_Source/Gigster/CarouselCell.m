//
//  CarouselCell.m
//  Gigster
//
//  Created by Rakitha Perera on 8/9/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import "CarouselCell.h"
#import "CarouselCollectionViewCell.h"
#import "CarouselItem.h"


@implementation CarouselCell 

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.collectionView registerNib:[UINib nibWithNibName:@"CarouselCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CarouselCollectionViewCell"];

    self.items = NSArray.new;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
}

- (void)configCellWithData:(Explore *)item{
    self.items = item.items;
    self.carouselTitleLabel.text = item.title;
    [self.collectionView reloadData];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.items.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CarouselItem * item = self.items[indexPath.row];
    
    CarouselCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CarouselCollectionViewCell" forIndexPath:indexPath];
    [cell configCellWithData:item];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(161, 178);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
     CarouselItem * item = self.items[indexPath.row];
    [self.delegate CarouselCell:self didSelecectItem:item];
   

}

@end
