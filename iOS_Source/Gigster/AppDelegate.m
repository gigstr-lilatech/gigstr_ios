//
//  AppDelegate.m
//  Gigster
//
//  Created by EFutures on 10/29/15.
//  Copyright © 2015 EFutures. All rights reserved.
//

#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "GigDetailsViewController.h"
#import "ChatViewController.h"
#import "Jobs.h"
#import "ChatDTO.h"
#import "User.h"
#import "OnboardingViewController.h"
#import "OnboardingContentViewController.h"
#import "UIDevice+Resolutions.h"
#import "Utility.h"
#import "WhatsNewViewController.h"
#import "Branch.h"
#import "MainTabbarViewController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "IQKeyboardManager.h"
#import "UserProfileViewController.h"
#import "HTTPClient.h"


static NSString * const kUserHasOnboardedKey = @"user_has_onboarded";

@interface AppDelegate ()
@property BOOL isFullScreen;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
   
    [Fabric with:@[[Crashlytics class], [Branch class]]];

    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];

    // determine if the user has onboarded yet or not
    BOOL userHasOnboarded = [[NSUserDefaults standardUserDefaults] boolForKey:kUserHasOnboardedKey];
    
    //Checking for version change
    NSString *currentVersion = [Utility appVersion];
    NSString *whatsNewShownVersion = [[NSUserDefaults standardUserDefaults] stringForKey:kLastShowedVersionKey];
    BOOL isNewer = ([currentVersion compare:whatsNewShownVersion options:NSNumericSearch] == NSOrderedDescending);
    if (isNewer) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:khaveShownWhatsNewKey];
        [[NSUserDefaults standardUserDefaults] setValue:currentVersion forKey:kLastShowedVersionKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    [[UITabBar appearance] setBarStyle:UIBarStyleDefault];
    [[UITabBar appearance] setTintColor:[UIColor colorWithRed:131.0/255.0 green:213.0/255.0 blue:210.0/255.0 alpha:1.0]];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:131.0/255.0 green:213.0/255.0 blue:210.0/255.0 alpha:1.0]];
    [[UINavigationBar appearance] setTranslucent:NO];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc]init] forBarMetrics:UIBarMetricsDefault];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [self setupBranchWithOptions:launchOptions];
//    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]){
//        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
//    }
    
    // Detect user country if the country is not selected
    if (![[NSUserDefaults standardUserDefaults] valueForKey:kCountryCode]) {
        NSString *countryCode = [[NSLocale currentLocale] objectForKey: NSLocaleCountryCode];
        [User getCountryListOncompletion:^(BOOL success, NSDictionary *data, NSError *error) {
            NSArray *countries = [data valueForKeyPath:@"countries"];
            int row = 0;
            BOOL countryFound = NO;
            for (NSDictionary *country in countries) {
                    if ([country[@"code"] isEqualToString:countryCode]) {
                        [[NSUserDefaults standardUserDefaults] setValue:countryCode forKey:kCountryCode];
                        countryFound = YES;
                        break;
                    }
                row ++;
            }
            if (!countryFound) {
                 [[NSUserDefaults standardUserDefaults] setValue:@"SE" forKey:kCountryCode];
            }
            [[NSUserDefaults standardUserDefaults] synchronize];
        }];
    }
    
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSDictionary *languageDic = [NSLocale componentsFromLocaleIdentifier:language];
    NSString *languageCode = [languageDic objectForKey:@"kCFLocaleLanguageCodeKey"];
    
    [[NSUserDefaults standardUserDefaults] setValue:languageCode forKey:kLanguageCode];
    [[NSUserDefaults standardUserDefaults] synchronize];
    // if the user has already onboarded, just set up the normal root view controller
    // for the application
    if (userHasOnboarded) {
        [self setupNormalRootViewController:application];
    }else{
        self.window.rootViewController = [self generateFirstDemoVC];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willEnterFullScreen:)
                                                 name:@"MediaEnterFullScreen"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willExitFullScreen:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:nil];
    
    return YES;
}

- (void)setupBranchWithOptions:(NSDictionary *)launchOptions {

    Branch *branch = [Branch getInstance];
//    [branch setDebug];
    [branch initSessionWithLaunchOptions:launchOptions andRegisterDeepLinkHandler:^(NSDictionary *params, NSError *error) {
        if (!error && params) {
            // params are the deep linked params associated with the link that the user clicked -> was re-directed to this app
            // params will be empty if no data found
            // ... insert custom logic here ...
            NSLog(@"params: %@", params.description);
            
            if ([params objectForKey:@"user_id"]) {
                
                NSString *userId = [params objectForKey:@"user_id"];
                [User registerDeviceWithInviter:userId Completion:^(BOOL success, NSDictionary *data, NSError *error) {
                }];
                

            }
            
            if ([params objectForKey:@"share_gig"]) {
                Jobs *job = [[Jobs alloc]init];
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                GigDetailsViewController *ivc = [storyboard instantiateViewControllerWithIdentifier:@"GigDetailsViewController"];
                job.jobID = [params objectForKey:@"share_gig"];
                ivc.job = job;
                UITabBarController *tabbar = (UITabBarController *)self.window.rootViewController;
                
                if ([tabbar isKindOfClass:MainTabbarViewController.class] && [tabbar.viewControllers count] > 0) {
                    [tabbar setSelectedIndex:2];
                    UINavigationController *navigation = [tabbar.viewControllers objectAtIndex:1];
                    [navigation pushViewController:ivc animated:YES];
                } else {
                    [[NSUserDefaults standardUserDefaults] setBool:true forKey:kIsDeepLinkActiveKey];
                    [[NSUserDefaults standardUserDefaults] setValue:[params objectForKey:@"share_gig"] forKey:kOpenGigDeepLinkKey];
                    [[NSUserDefaults standardUserDefaults] synchronize];

                }
            }
        }
    }];
}

-(void)setupNormalRootViewController:(UIApplication *)application{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    
    UITabBarController *tabbar = [storyboard instantiateViewControllerWithIdentifier:@"Main_Tabbar"];
    tabbar.selectedIndex       = 0;
    tabbar.tabBar.barTintColor = [UIColor whiteColor];
    UIColor* color_green = [UIColor colorWithRed:203/255.0 green:237/255.0 blue:235/255.0 alpha:1.0];
    tabbar.tabBar.layer.borderWidth = 0.50;
    tabbar.tabBar.layer.borderColor = color_green.CGColor;
    
    self.window.rootViewController = tabbar;
    [self.window makeKeyAndVisible];
}

- (void)handleOnboardingCompletion {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kUserHasOnboardedKey];
    
    // transition to the main application
    [self setupNormalRootViewController:[UIApplication sharedApplication]];
}

-(void)pushEnable {
    
    UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                    UIUserNotificationTypeBadge |
                                                    UIUserNotificationTypeSound);
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                             categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidBecomeActiveNotification
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification * _Nonnull note) {
                                                      if ([[UIApplication sharedApplication] isRegisteredForRemoteNotifications]) {
                                                          NSLog(@"ABC");
                                                      }
                                                      else{
                                                          NSLog(@"DEF");
                                                      }
                                                  }];
    
    
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    [[Branch getInstance] handleDeepLink:url];
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation
            ];
}

#pragma mark - APN

-(void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}


-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    NSString * token = [NSString stringWithFormat:@"%@", deviceToken];
    //Format token as you need:
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
    
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"pushDeviceToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    NSLog(@"%@",token);
    //[self showAlert:token];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:token forKey:@"deviceID"];
    [prefs synchronize];
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"APN MSG:%@",userInfo);
    UIApplicationState state = [application applicationState];
    [[Branch getInstance] handlePushNotification:userInfo];
    
    if (state == UIApplicationStateActive) {
        
        return;
        //app is in foreground
        //the push is in your control
    } else {
                
        NSDictionary *dic = [userInfo objectForKeyNotNull:@"aps"];
        
        [UIApplication sharedApplication].applicationIconBadgeNumber = [[dic objectForKeyNotNull:@"badge"] intValue];
        
        if ([[dic objectForKey:@"type"] isEqualToString:@"1"]) {
            
            Jobs *job = [[Jobs alloc]init];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            GigDetailsViewController *ivc = [storyboard instantiateViewControllerWithIdentifier:@"GigDetailsViewController"];
            job.jobID = [dic objectForKey:@"gig_id"];
            ivc.job = job;
            UITabBarController *tabbar = (UITabBarController *)self.window.rootViewController;
            
            if ([tabbar.viewControllers count] > 0) {
                
                [tabbar setSelectedIndex:2];
            }
            
            UINavigationController *navigation = [tabbar.viewControllers objectAtIndex:1];
            [navigation pushViewController:ivc animated:YES];
        }else{
            
            ChatDTO *chatObj = [[ChatDTO alloc]init];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            ChatViewController *ivc = [storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];
            chatObj.chatID = [dic objectForKey:@"chat_id"];
            ivc.chatDTO = chatObj;
            ivc.userImageURL = [dic objectForKey:@"user_image"];
            UITabBarController *tabbar = (UITabBarController *)self.window.rootViewController;
            [tabbar setSelectedIndex:1];
            UINavigationController *navigation = [tabbar.viewControllers objectAtIndex:0];
            [navigation pushViewController:ivc animated:YES];
        }

        
        [self pushBadgeClear];
        
        //app is in background:
        //iOS is responsible for displaying push alerts, banner etc..
    }
    
}

-(void)showAlert:(NSString *)message{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Device Token"
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             [self.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    [alert addAction:ok];
    
}

-(void)pushBadgeClear{
    
    [User updatePushCountcompletion:^(BOOL success, NSDictionary *data, NSError *error) {
        
        [UIApplication sharedApplication].applicationIconBadgeNumber= 0;
        
        NSLog(@"success: %d", success);
        NSLog(@"data: %@", data);
        NSLog(@"Error: %@", error);
        
    }];
  
}

- (OnboardingViewController *)generateFirstDemoVC {
    UIDeviceResolution valueDevice = [[UIDevice currentDevice] currentResolution];
    CGFloat topPadding = 0;
    if (valueDevice == 2)
    {
        //iphone 4 & 4S
        topPadding = 50;
    }
    else if (valueDevice == 3)
    {
        //iphone 5
        topPadding = 210;
    }
    else if (valueDevice == 4)
    {
        //ipad 2
        topPadding = 50;
    }
    else if (valueDevice == 5)
    {
        //ipad 3 - retina display
        topPadding = 50;
    }
    
//    UIColor *fontColor = [UIColor colorWithRed:175.0/255.0 green:171/255.0 blue:171/255.0 alpha:1.0];
    UIColor *fontColor = [UIColor whiteColor];
    OnboardingContentViewController *firstPage = [[OnboardingContentViewController alloc] initWithTitle:NSLocalizedString(@"Welcome to Gigstr", nil) body:NSLocalizedString(@"Discover a new way of working",nil) image:[UIImage imageNamed:@"step_1"] buttonText:nil action:^{
        [self handleOnboardingCompletion];
    }];
    firstPage.topPadding = topPadding;
    firstPage.underIconPadding = 10;
    firstPage.underTitlePadding = 10;
    firstPage.titleTextColor = fontColor;
    //firstPage.titleFontName = @"SFOuterLimitsUpright";
    firstPage.bodyTextColor = fontColor;
    //firstPage.bodyFontName = @"NasalizationRg-Regular";
    firstPage.buttonTextColor = fontColor;
    firstPage.bodyFontSize = 19;
    
    OnboardingContentViewController *secondPage = [[OnboardingContentViewController alloc] initWithTitle:NSLocalizedString(@"Create your profile and search for jobs",nil) body:NSLocalizedString(@"See, follow and apply for jobs with your profile",nil) image:[UIImage imageNamed:@"step_2"] buttonText:nil action:nil];
    //secondPage.titleFontName = @"SFOuterLimitsUpright";
    secondPage.topPadding = topPadding + 25;
    secondPage.underIconPadding = 10;
    secondPage.underTitlePadding = 10;
    secondPage.titleTextColor = fontColor;
    secondPage.bodyTextColor = fontColor;
    //secondPage.bodyFontName = @"NasalizationRg-Regular";
    secondPage.bodyFontSize = 19;
    
    OnboardingContentViewController *thirdPage = [[OnboardingContentViewController alloc] initWithTitle:NSLocalizedString(@"Get chosen and coached",nil) body:NSLocalizedString(@"Gigstr helps you with everything you need before and during your job", nil) image:[UIImage imageNamed:@"step_3"] buttonText:nil action:^{
        [self handleOnboardingCompletion];
    }];
    thirdPage.topPadding = topPadding;
    thirdPage.underIconPadding = 10;
    thirdPage.underTitlePadding = 10;
    //thirdPage.bottomPadding = -10;
    //thirdPage.titleFontName = @"SFOuterLimitsUpright";
    thirdPage.titleTextColor = fontColor;
    thirdPage.bodyTextColor = fontColor;
   // thirdPage.buttonTextColor = [UIColor colorWithRed:239/255.0 green:88/255.0 blue:35/255.0 alpha:1.0];
    //thirdPage.bodyFontName = @"NasalizationRg-Regular";
    thirdPage.bodyFontSize = 19;
    //thirdPage.buttonFontName = @"SpaceAge";
    //thirdPage.buttonFontSize = 17;
    
    OnboardingContentViewController *fourthPage = [[OnboardingContentViewController alloc] initWithTitle:NSLocalizedString(@"Flexibility",nil) body:NSLocalizedString(@"Take jobs where you live, study and travel", nil) image:[UIImage imageNamed:@"step_4"] buttonText:nil action:^{
        [self handleOnboardingCompletion];
    }];
    fourthPage.topPadding = topPadding;
    fourthPage.underIconPadding = 10;
    fourthPage.underTitlePadding = 10;
    //fourthPage.bottomPadding = -10;
    //thirdPage.titleFontName = @"SFOuterLimitsUpright";
    fourthPage.titleTextColor = fontColor;
    fourthPage.bodyTextColor = fontColor;
    //thirdPage.buttonTextColor = [UIColor colorWithRed:239/255.0 green:88/255.0 blue:35/255.0 alpha:1.0];
    //thirdPage.bodyFontName = @"NasalizationRg-Regular";
    fourthPage.bodyFontSize = 19;
    //thirdPage.buttonFontName = @"SpaceAge";
   // thirdPage.buttonFontSize = 17;
    
    OnboardingContentViewController *fifthPage = [[OnboardingContentViewController alloc] initWithTitle:NSLocalizedString(@"Stay up to date",nil) body:NSLocalizedString(@"Don’t miss any new jobs or information from us by turning on notifications", nil) image:[UIImage imageNamed:@"step_5"] buttonText:NSLocalizedString(@"Get started",nil) action:^{
        [self pushEnable];
        [self handleOnboardingCompletion];
        
    }];
    fifthPage.topPadding = topPadding;
    fifthPage.underIconPadding = 10;
    fifthPage.underTitlePadding = 10;
    fifthPage.titleTextColor = fontColor;
    fifthPage.bodyTextColor = fontColor;
    fifthPage.buttonTextColor = fontColor;
    fifthPage.bodyFontSize = 19;

    
    OnboardingViewController *onboardingVC = [OnboardingViewController onboardWithBackgroundImage:[UIImage imageNamed:@"gigstr_background"] contents:@[firstPage, secondPage, thirdPage, fourthPage, fifthPage]];
    onboardingVC.shouldFadeTransitions = YES;
    onboardingVC.fadePageControlOnLastPage = YES;
//    [onboardingVC.skipButton setTitleColor:[UIColor colorWithRed:175.0/255.0 green:171/255.0 blue:171/255.0 alpha:1.0] forState:UIControlStateNormal];
    onboardingVC.shouldMaskBackground = NO;
    onboardingVC.fadeSkipButtonOnLastPage = YES;
    
    // If you want to allow skipping the onboarding process, enable skipping and set a block to be executed
    // when the user hits the skip button.
    onboardingVC.allowSkipping = NO;
//    onboardingVC.skipHandler = ^{
//        [self handleOnboardingCompletion];
//    };
    
    return onboardingVC;
}



-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
   // [self showAlert:error.description];
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
     [self pushBadgeClear];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

// Respond to Universal Links
- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray *restorableObjects))restorationHandler {
    BOOL handledByBranch = [[Branch getInstance] continueUserActivity:userActivity];
    
    return handledByBranch;
}
    
-(void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler {
        
        NSLog(@"===== background session completed in AppDelegate ===== ");
        
    if([identifier isEqualToString:SHIFT_IMAGE_UPLOAD_ID]){
        [HTTPClient sharedClient].backgroundSessionCompletionHandler = completionHandler;
    }
}

- (void)willEnterFullScreen:(NSNotification *)notification {
    self.isFullScreen = YES;
}

- (void)willExitFullScreen:(NSNotification *)notification {
    self.isFullScreen = NO;
}

-(UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    return self.isFullScreen == YES ? UIInterfaceOrientationMaskAll : UIInterfaceOrientationMaskPortrait;
}

@end
