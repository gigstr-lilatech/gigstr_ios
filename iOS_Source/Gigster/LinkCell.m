//
//  LinkCell.m
//  Gigster
//
//  Created by Rakitha Perera on 8/16/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import "LinkCell.h"

@implementation LinkCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configCellWithData:(Explore *)item{
    self.linkLabel.text = item.title;
    
}

@end
