//
//  EnterPitchViewController.h
//  Gigster
//
//  Created by EFutures on 1/22/16.
//  Copyright © 2016 EFutures. All rights reserved.
//

#import "BaseViewController.h"
#import "XCDFormInputAccessoryView.h"
@protocol UpdatePichTextProtocol <NSObject>

-(void)setPitchText:(NSString *)textPitch;

@end


@interface EnterPitchViewController : BaseViewController

@property(nonatomic,strong)NSString *pitchText;
@property(nonatomic,unsafe_unretained) id<UpdatePichTextProtocol> pitchDelegate;

@property(nonatomic,strong) NSString *placeholder;
@property(nonatomic,strong) NSString *viewTitle;

@end
