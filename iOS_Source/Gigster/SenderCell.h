//
//  SenderCell.h
//  Gigster
//
//  Created by EFutures on 12/13/15.
//  Copyright © 2015 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSWTappableLabel.h"
@interface SenderCell : UITableViewCell

@property(nonatomic,weak)IBOutlet ZSWTappableLabel *message;
@property(nonatomic,weak)IBOutlet UILabel *time;
@property(nonatomic,weak)IBOutlet UIImageView *senderImage;
@property(nonatomic,weak)IBOutlet UIActivityIndicatorView *activity;



-(void)resizeHeight:(CGRect)rect;


@end
