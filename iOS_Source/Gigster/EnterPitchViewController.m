//
//  EnterPitchViewController.m
//  Gigster
//
//  Created by EFutures on 1/22/16.
//  Copyright © 2016 EFutures. All rights reserved.
//

#import "EnterPitchViewController.h"
#import "Utility.h"

@interface EnterPitchViewController ()<UITextViewDelegate>

@property(nonatomic,weak)IBOutlet UITextView *txtPitch;
@property (nonatomic, strong) XCDFormInputAccessoryView *inputAccessoryView;
@property (weak, nonatomic) IBOutlet UILabel *titleViewLbl;
@property (nonatomic, assign) CGRect oldRect;
@property (nonatomic, strong) NSTimer *caretVisibilityTimer;

@end


@implementation EnterPitchViewController

@synthesize pitchDelegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar setHidden:NO];

    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    [self.navigationController.navigationBar setBackgroundImage:[Utility imageFromColor:[UIColor colorWithRed:131.0/255.0 green:213.0/255.0 blue:210.0/255.0 alpha:1.0]]
                                                  forBarMetrics:UIBarMetricsDefault];
    
//    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init]
//                                      forBarPosition:UIBarPositionAny
//                                          barMetrics:UIBarMetricsDefault];
    
    
    

//    self.navigationController.navigationBar.translucent = NO;
    
    
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnTextView)];
//    [self.txtPitch addGestureRecognizer:tap];
    

    if (_viewTitle.length != 0) {
        _titleViewLbl.text = _viewTitle;
    }
    
    
    self.inputAccessoryView = [XCDFormInputAccessoryView new];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardHide)
                                                 name:@"HideKeyboard" object:nil];
    
    if ([_pitchText isEqualToString:_placeholder]) {

                    _txtPitch.textColor = [UIColor lightGrayColor];
                    _txtPitch.textAlignment = NSTextAlignmentLeft;
                    _txtPitch.text = _placeholder;
                    [_txtPitch resignFirstResponder];
    }else{

                    _txtPitch.textColor = [UIColor darkGrayColor];
                    _txtPitch.textAlignment = NSTextAlignmentLeft;
                    _txtPitch.text      = _pitchText;
    }
    
//    self.txtPitch.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_keyboardWillShowNotification:) name:UIKeyboardWillShowNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_keyboardWillHideNotification:) name:UIKeyboardWillHideNotification object:nil];
    
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.navigationController.navigationBar setHidden:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)addPitchText:(id)sender{

    [pitchDelegate setPitchText:_txtPitch.text];

    [self.navigationController popViewControllerAnimated:YES];
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    return YES;
}


- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if ([_txtPitch.text isEqualToString:_placeholder]) {
        _txtPitch.textAlignment = NSTextAlignmentLeft;
        _txtPitch.text = @"";
        _txtPitch.textColor = [UIColor darkGrayColor];
        return YES;
    }
    return YES;
}

-(void)textViewDidChange:(UITextView *)textView
{
    
    if(_txtPitch.text.length == 0){
        
        _txtPitch.textAlignment = NSTextAlignmentLeft;
        _txtPitch.textColor = [UIColor lightGrayColor];
        _txtPitch.text = _placeholder;
        [_txtPitch resignFirstResponder];
    }
}

-(void)didTapOnTextView{
    
    if(_txtPitch.text.length == 0){
        _txtPitch.textColor = [UIColor colorWithRed:204.0f/255.0f green:204.0f/255.0f blue:204.0f/255.0f alpha:1.0];
        _txtPitch.text = _placeholder;
        [_txtPitch resignFirstResponder];
    }else {
        
        [_txtPitch resignFirstResponder];
    }
    
}

-(void)keyboardHide{
    
    if(_txtPitch.text.length == 0){
        _txtPitch.textColor = [UIColor colorWithRed:204.0f/255.0f green:204.0f/255.0f blue:204.0f/255.0f alpha:1.0];
        _txtPitch.text = _placeholder;
        [_txtPitch resignFirstResponder];
    }
    
}


- (void)_keyboardWillShowNotification:(NSNotification*)notification
{
    UIEdgeInsets insets = self.txtPitch.contentInset;
    insets.bottom += [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    self.txtPitch.contentInset = insets;
    
    insets = self.txtPitch.scrollIndicatorInsets;
    insets.bottom += [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    self.txtPitch.scrollIndicatorInsets = insets;
}

- (void)_keyboardWillHideNotification:(NSNotification*)notification
{
    UIEdgeInsets insets = self.txtPitch.contentInset;
    insets.bottom -= [notification.userInfo[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
    self.txtPitch.contentInset = insets;
    
    insets = self.txtPitch.scrollIndicatorInsets;
    insets.bottom -= [notification.userInfo[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
    self.txtPitch.scrollIndicatorInsets = insets;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    _oldRect = [self.txtPitch caretRectForPosition:self.txtPitch.selectedTextRange.end];
    
    _caretVisibilityTimer = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(_scrollCaretToVisible) userInfo:nil repeats:YES];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [_caretVisibilityTimer invalidate];
    _caretVisibilityTimer = nil;
}

- (void)_scrollCaretToVisible
{
    //This is where the cursor is at.
    CGRect caretRect = [self.txtPitch caretRectForPosition:self.txtPitch.selectedTextRange.end];
    
    if(CGRectEqualToRect(caretRect, _oldRect))
        return;
    
    _oldRect = caretRect;
    
    //This is the visible rect of the textview.
    CGRect visibleRect = self.txtPitch.bounds;
    visibleRect.size.height -= (self.txtPitch.contentInset.top + self.txtPitch.contentInset.bottom);
    visibleRect.origin.y = self.txtPitch.contentOffset.y;
    
    //We will scroll only if the caret falls outside of the visible rect.
    if(!CGRectContainsRect(visibleRect, caretRect))
    {
        CGPoint newOffset = self.txtPitch.contentOffset;
        
        newOffset.y = MAX((caretRect.origin.y + caretRect.size.height) - visibleRect.size.height + 5, 0);
        
        [self.txtPitch setContentOffset:newOffset animated:YES];
    }
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
