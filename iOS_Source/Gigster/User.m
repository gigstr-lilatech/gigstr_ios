//
//  User.m
//  Gigster
//
//  Created by EFutures on 11/11/15.
//  Copyright © 2015 EFutures. All rights reserved.
//

#import "User.h"
#import "Api.h"

@implementation User

@synthesize userid,name,email,password,token,accountType,fbID,mobile,address,gender,person_number,userImageURL;

+ (void) loginWithEmail:(NSString *) email password:(NSString *) password accountType:(int)accountType completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion{
    
    NSString *pushToken;
    if ([[NSUserDefaults standardUserDefaults] stringForKey:@"pushDeviceToken"]) {
        
        pushToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"pushDeviceToken"];
    }else{
        
        pushToken = @"";
    }
    
    NSDictionary *params    = @{@"email": email, @"password": password, @"accountType": [NSNumber numberWithInt:accountType], @"device_id":pushToken, @"device_type": @"2"};
    NSDictionary *data      = @{@"url": @"user/login", @"parameters": params};
    
    [Api post:data onCompletion:^(BOOL success, NSDictionary *response, NSError *error) {
        NSLog(@"success: %d", success);
        NSLog(@"respose: %@", response);
        NSLog(@"Error: %@", error);
        
        if (success) {
            
        }
        
        completion(success, response, error);
        
    }];
    
}

+ (void) userRegisterWithName:(NSString *)name email:(NSString *)email password:(NSString *)password accountType:(int)accountType completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion{
    
    NSString *pushToken;
    if ([[NSUserDefaults standardUserDefaults] stringForKey:@"pushDeviceToken"]) {
        
        pushToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"pushDeviceToken"];
    }else{
        
        pushToken = @"";
    }
    NSDictionary *params    = @{@"name": name, @"email": email, @"password": password, @"accountType": [NSNumber numberWithInt:accountType], @"device_id":pushToken, @"device_type": @"2"};
    NSDictionary *data      = @{@"url": @"user/add", @"parameters": params};
    
    [Api post:data onCompletion:^(BOOL success, NSDictionary *response, NSError *error) {
        NSLog(@"success: %d", success);
        NSLog(@"respose: %@", response);
        NSLog(@"Error: %@", error);
        
        if (success) {
            
        }
        
        completion(success, response, error);
        
    }];

    
}

+ (void) resetPasswordwithEmail:(NSString *)email completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion{
    
    NSDictionary *params    = @{@"email": email};
    NSDictionary *data      = @{@"url": @"user/reset_password", @"parameters": params};
    
    [Api post:data onCompletion:^(BOOL success, NSDictionary *response, NSError *error) {
        NSLog(@"success: %d", success);
        NSLog(@"respose: %@", response);
        NSLog(@"Error: %@", error);
        
        if (success) {

            
        }
        
        completion(success, response, error);
        
    }];

    
}

+ (void) loginWithFacebookid:(NSDictionary *)params completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion{
    
    
    NSDictionary *data      = @{@"url": @"user/login", @"parameters": params};
    
    
    [Api post:data onCompletion:^(BOOL success, NSDictionary *response, NSError *error) {
        NSLog(@"success: %d", success);
        NSLog(@"respose: %@", response);
        NSLog(@"Error: %@", error);

        completion(success, response, error);
        
    }];
    
}



+ (void) getPersonnelInfoForUserID:(NSString *)userID token:(NSString *)token completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion{

    NSDictionary *params    = @{@"user_id": userID, @"token": token};
    NSDictionary *data      = @{@"url": @"user/get_personal", @"parameters": params};
    
    [Api post:data onCompletion:^(BOOL success, NSDictionary *response, NSError *error) {
        NSLog(@"success: %d", success);
        NSLog(@"respose: %@", response);
        NSLog(@"Error: %@", error);
        
        if (success) {
            
            
        }
        
        completion(success, response, error);
        
    }];
    
}

+ (void) getAccountInfoForUserID:(NSString *)userID token:(NSString *)token completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion{
    
    NSDictionary *params    = @{@"user_id": userID, @"token": token};
    NSDictionary *data      = @{@"url": @"user/get_bank", @"parameters": params};
    
    [Api post:data onCompletion:^(BOOL success, NSDictionary *response, NSError *error) {
        NSLog(@"success: %d", success);
        NSLog(@"respose: %@", response);
        NSLog(@"Error: %@", error);
        
        if (success) {
            
            
        }
        
        completion(success, response, error);
        
    }];
    
}


+ (void)savePersonnelData:(NSMutableDictionary *)parameters completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion{
    
    NSDictionary *data      = @{@"url": @"user/update_personal", @"parameters": parameters};
    
    [Api post:data onCompletion:^(BOOL success, NSDictionary *response, NSError *error) {
        NSLog(@"success: %d", success);
        NSLog(@"respose: %@", response);
        NSLog(@"Error: %@", error);
        
        if (success) {
            
            
        }
        
        completion(success, response, error);
        
    }];
    
}

+ (void) saveBanklData:(NSMutableDictionary *)parameters completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion{
    
    
    NSDictionary *data      = @{@"url": @"user/update_bank", @"parameters": parameters};
    
    [Api post:data onCompletion:^(BOOL success, NSDictionary *response, NSError *error) {
        NSLog(@"success: %d", success);
        NSLog(@"respose: %@", response);
        NSLog(@"Error: %@", error);
        
        if (success) {
            
            
        }
        
        completion(success, response, error);
        
    }];
    
}

+ (void) logoutWithUserID:(NSDictionary *)params completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion{
    
    NSDictionary *data      = @{@"url": @"user/logout", @"parameters": params};
    
    [Api post:data onCompletion:^(BOOL success, NSDictionary *response, NSError *error) {
        NSLog(@"success: %d", success);
        NSLog(@"respose: %@", response);
        NSLog(@"Error: %@", error);
        
        if (success) {
            
            
        }
        
        completion(success, response, error);
        
    }];
    
}

+ (void)updatePushCountcompletion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion{
    
    NSUserDefaults *defaults    = [NSUserDefaults standardUserDefaults];
    
    NSString *pushToken;
    if ([defaults stringForKey:@"pushDeviceToken"]) {
        
        pushToken = [defaults stringForKey:@"pushDeviceToken"];
    }else{
        
        pushToken = @"";
    }

    NSData *dataRepresentingSavedArray = [defaults objectForKey:@"userObject"];
    if (dataRepresentingSavedArray != nil)
    {
        User *user =  (User *)[NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
        
        NSDictionary *params    = @{@"user_id": user.userid, @"device_token": pushToken};
        NSDictionary *data      = @{@"url": @"user/update_push_count", @"parameters": params};
        
        [Api post:data onCompletion:^(BOOL success, NSDictionary *response, NSError *error) {
            NSLog(@"success: %d", success);
            NSLog(@"respose: %@", response);
            NSLog(@"Error: %@", error);
            completion(success, response, error);
        }];
    }
}

+ (void)getCountryListOncompletion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion {
    NSDictionary *data      = @{@"url": @"user/country", @"parameters": @{}};
    
    [Api post:data onCompletion:^(BOOL success, NSDictionary *response, NSError *error) {
        NSLog(@"success: %d", success);
        NSLog(@"respose: %@", response);
        NSLog(@"Error: %@", error);
        
        if (success) {
        }
        
        completion(success, response, error);
    }];
}

+ (void)setCountry:(NSDictionary *)params Oncompletion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion {
    NSDictionary *data      = @{@"url": @"user/update_country", @"parameters": params};
    
    [Api post:data onCompletion:^(BOOL success, NSDictionary *response, NSError *error) {
        NSLog(@"success: %d", success);
        NSLog(@"respose: %@", response);
        NSLog(@"Error: %@", error);
        
        if (success) {
            
            
        }
        
        completion(success, response, error);
        
    }];
}

+ (void)registerDeviceWithInviter:(NSString *)userID Completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion{
    
    NSDictionary *params    = @{@"userId": userID};
    NSDictionary *data      = @{@"url": @"referal/add", @"parameters": params};
    
    [Api post:data onCompletion:^(BOOL success, NSDictionary *response, NSError *error) {
        NSLog(@"success: %d", success);
        NSLog(@"respose: %@", response);
        NSLog(@"Error: %@", error);
        completion(success, response, error);
    }];
}

- (id) initWithCoder: (NSCoder *)coder
{
    if (self = [super init])
    {
        self.userid             = [coder decodeObjectForKey:@"userid"];
        self.name               = [coder decodeObjectForKey:@"name"];
        self.email              = [coder decodeObjectForKey:@"email"];
        self.password           = [coder decodeObjectForKey:@"password"];
        self.token              = [coder decodeObjectForKey:@"token"];
        self.accountType        = [coder decodeObjectForKey:@"accountType"];
        self.fbID               = [coder decodeObjectForKey:@"fbID"];
        self.mobile             = [coder decodeObjectForKey:@"mobile"];
        self.address            = [coder decodeObjectForKey:@"address"];
        self.gender             = [coder decodeObjectForKey:@"gender"];
        self.person_number      = [coder decodeObjectForKey:@"person_number"];
        self.userImageURL       = [coder decodeObjectForKey:@"userImageURL"];
    }
       return self;
}

- (void) encodeWithCoder: (NSCoder *)coder
{
    [coder encodeObject:userid forKey:@"userid"];
    [coder encodeObject:name forKey:@"name"];
    [coder encodeObject:password forKey:@"password"];
    [coder encodeObject:token forKey:@"token"];
    [coder encodeObject:accountType forKey:@"accountType"];
    [coder encodeObject:fbID forKey:@"fbID"];
    [coder encodeObject:mobile forKey:@"mobile"];
    [coder encodeObject:address forKey:@"address"];
    [coder encodeObject:gender forKey:@"gender"];
    [coder encodeObject:person_number forKey:@"person_number"];
    [coder encodeObject:userImageURL forKey:@"userImageURL"];
    
}


@end
