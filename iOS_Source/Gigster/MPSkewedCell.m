//
//  MPSkewedCell.m
//  MPSkewed
//
//  Created by Alex Manzella on 17/05/15.
//  Copyright (c) 2015 Alex Manzella. All rights reserved.
//

#import "MPSkewedCell.h"
#import <QuartzCore/QuartzCore.h>
#import "MPSkewedCollectionViewLayoutAttributes.h"

@interface MPSkewedCell ()

@property (nonatomic, weak) UILabel *titleLbl;
@property (nonatomic, assign) BOOL applied;

@property (nonatomic, weak) CAGradientLayer *gradient;

// Layout customizations
@property (nonatomic, assign) CGFloat parallaxValue; //from 0 to 1
@property (nonatomic, assign) CGFloat lineSpacing;

@end

@implementation MPSkewedCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.clipsToBounds = YES;
        self.backgroundColor = [UIColor clearColor];
        
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        _imageView.backgroundColor = [UIColor clearColor];
        _imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        [_imageView sizeToFit];
        _imageView.layer.mask = [CAShapeLayer layer];
        [self addSubview:_imageView];
        
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = self.imageView.bounds;
       // gradient.colors = @[(id)[UIColor colorWithWhite:0 alpha:.85].CGColor,(id)[UIColor clearColor].CGColor];
        gradient.colors = @[(id)[UIColor colorWithWhite:0 alpha:1.0].CGColor,(id)[UIColor clearColor].CGColor];
        gradient.startPoint = CGPointMake(0.5, 1.0);
        gradient.endPoint = CGPointMake(0.5, .4);
        [self.imageView.layer addSublayer:gradient];
        self.gradient = gradient;

        CGFloat realH = CGRectGetHeight(self.bounds);
        CGFloat latoA = realH / 3;
        CGFloat width = CGRectGetWidth(self.bounds);
        

        UIImageView *gradientImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, width, realH - 102)];
        gradientImage.image = [UIImage imageNamed:@"gradient_background"];
        gradientImage.backgroundColor = [UIColor clearColor];
        gradientImage.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        gradientImage.contentMode = UIViewContentModeScaleAspectFill;
        gradientImage.clipsToBounds = YES;
        //[self addSubview:gradientImage];
        
        
        UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, latoA + 33 - 5, width - 20, 20)];
        textLabel.backgroundColor = [UIColor clearColor];
        textLabel.layer.anchorPoint = CGPointMake(.5, .5);
       // textLabel.font = [UIFont boldSystemFontOfSize:20];
        textLabel.font = [UIFont fontWithName:BOLD_FONT_NAME size:18];
        textLabel.numberOfLines = 1;
        textLabel.textColor = [UIColor whiteColor];
        //textLabel.shadowColor = [UIColor blackColor];
        //textLabel.shadowOffset = CGSizeMake(1, 1);
        //textLabel.transform = CGAffineTransformMakeRotation(-(asin(latoA/(sqrt(pow(width, 2) + pow(latoA, 2))))));
        [self addSubview:textLabel];
        self.titleLbl = textLabel;
        
        
        
        UILabel *textLabel2;
        
        if (!self.applied) {
            textLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(10, latoA + 55 - 5, width - 22, 20)];
        }else{
            textLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(10, latoA + 55 - 5, width - 92, 20)];
        }
        
        textLabel2.backgroundColor = [UIColor clearColor];
        textLabel2.layer.anchorPoint = CGPointMake(.5, .5);
        //textLabel2.font = [UIFont systemFontOfSize:2];
        textLabel2.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:14];
        textLabel2.numberOfLines = 1;
        textLabel2.textColor = [UIColor whiteColor];
        //textLabel.shadowColor = [UIColor blackColor];
        //textLabel.shadowOffset = CGSizeMake(1, 1);
        //textLabel.transform = CGAffineTransformMakeRotation(-(asin(latoA/(sqrt(pow(width, 2) + pow(latoA, 2))))));
        [self addSubview:textLabel2];
        self.subttl1 = textLabel2;


        
        UILabel *textLabel3;
        if (!self.applied) {
            textLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(10, latoA + 71 - 5, width - 22, 20)];
        }else{
            textLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(10, latoA + 71 - 5, width - 92, 20)];
        }
        
        textLabel3.backgroundColor = [UIColor clearColor];
        textLabel3.layer.anchorPoint = CGPointMake(.5, .5);
        //textLabel3.font = [UIFont systemFontOfSize:3];
        textLabel3.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:13];
        textLabel3.numberOfLines = 1;
        textLabel3.textColor = [UIColor colorWithRed:255.0f/255.0f green:206.0f/255.0f blue:86.0f/255.0f alpha:1.0];
        //textLabel.shadowColor = [UIColor blackColor];
        //textLabel.shadowOffset = CGSizeMake(1, 1);
        //textLabel.transform = CGAffineTransformMakeRotation(-(asin(latoA/(sqrt(pow(width, 2) + pow(latoA, 2))))));
        [self addSubview:textLabel3];
        self.subttl2 = textLabel3;
        
        
        CGFloat spacing = 5;
        
        self.appliedStamp           =  [UIButton buttonWithType:UIButtonTypeCustom];
        self.appliedStamp.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [self.appliedStamp setFrame:CGRectMake(width - 90, latoA + 67 - 5, 80, 22)];
        [self.appliedStamp setImage:[UIImage imageNamed:@"applied_icon"] forState:UIControlStateNormal];
        [self.appliedStamp setTitle:NSLocalizedString(@"Applied", nil) forState:UIControlStateNormal];
        [self.appliedStamp setTitleColor:[UIColor colorWithRed:131.0/255.0 green:213.0/255.0 blue:210.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        [self.appliedStamp.titleLabel setFont:[UIFont fontWithName:DEFAULT_FONT_NAME size:14]];
        self.appliedStamp.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, spacing);
        self.appliedStamp.titleEdgeInsets = UIEdgeInsetsMake(0, spacing, 0, 0);
        [self addSubview:self.appliedStamp];
        
        UIActivityIndicatorView *activityIndi = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndi.center =  CGPointMake(self.frame.size.width/2, self.frame.size.height/3);
        //[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.bounds.size.width/2, self.bounds.size.height/2, 20.0, 20.0)];
    
        [activityIndi setHidden:NO];
        [activityIndi startAnimating];
        [self addSubview:activityIndi];
        self.activity = activityIndi;

    }
    
    return self;
}
-(void)adjustAppliedUI{
    
    CGFloat realH = CGRectGetHeight(self.bounds);
    CGFloat latoA = realH / 3;
    CGFloat width = CGRectGetWidth(self.bounds);
    
    if (!self.applied) {
        [self.subttl1 setFrame:CGRectMake(10, latoA + 55 - 5, width - 22, 20)];
    }else{
        [self.subttl1 setFrame:CGRectMake(10, latoA + 55 - 5, width - 92, 20)];
    }
    
    if (!self.applied) {
        [self.subttl2 setFrame:CGRectMake(10, latoA + 71 - 5, width - 22, 20)];
    }else{
        [self.subttl2 setFrame:CGRectMake(10, latoA + 71 - 5, width - 92, 20)];
    }
    
}
- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat offset = self.frame.origin.y - [(UICollectionView *)[self superview] contentOffset].y;
    CGFloat parallaxValue = offset/self.superview.frame.size.height;
    self.imageView.frame = CGRectInset(self.bounds, 0,  0);//self.imageView.frame = CGRectInset(self.bounds, 0,  -CGRectGetHeight(self.bounds) / 3);
    self.gradient.frame = self.imageView.bounds;
    
    self.parallaxValue = parallaxValue;
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    CGFloat realH = CGRectGetHeight(self.bounds) * 2 / 3 - self.lineSpacing;
    CGFloat width = CGRectGetWidth(self.bounds);
    UIBezierPath* realCellArea = [UIBezierPath bezierPath];
    [realCellArea moveToPoint:CGPointMake(0,0)];
    [realCellArea addLineToPoint:CGPointMake(width, 0)];
    [realCellArea addLineToPoint:CGPointMake(width, realH)];
    [realCellArea addLineToPoint:CGPointMake(0, realH)];
    [realCellArea closePath];

    return [realCellArea containsPoint:point];
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    [self updateImageViewMask];
}

- (void)applyLayoutAttributes:(MPSkewedCollectionViewLayoutAttributes *)layoutAttributes {
    [super applyLayoutAttributes:layoutAttributes];
    self.lineSpacing = layoutAttributes.lineSpacing;
    self.parallaxValue = layoutAttributes.parallaxValue;
}


- (void)setParallaxValue:(CGFloat)parallaxValue {
    _parallaxValue = parallaxValue;
    CGFloat height = CGRectGetHeight(self.bounds);
    CGFloat maxOffset = -height / 3 - height / 3;
    
    CGRect frame = self.imageView.frame;
    frame.origin.y = maxOffset * parallaxValue;
    self.imageView.frame = frame;
    
    [self updateImageViewMask];
}

#pragma mark - Skewed Mask

- (void)updateImageViewMask {
    CAShapeLayer *layer = (CAShapeLayer *)self.imageView.layer.mask;
    layer.frame = self.imageView.bounds;
    layer.path = [self maskPath];
}

- (CGPathRef)maskPath {
    CGFloat realH = CGRectGetHeight(self.bounds) * 2 / 3 - self.lineSpacing;
    CGFloat width = CGRectGetWidth(self.bounds);
    CGFloat imageViewX = CGRectGetMinX(self.imageView.frame);
    CGFloat imageViewY = CGRectGetMinY(self.imageView.frame);
    
//    NSLog(@"realH %f",realH);
//    NSLog(@"width %f",width);
//    NSLog(@"imageViewX %f",imageViewX);
//    NSLog(@"imageViewY %f",imageViewY);
//    
    
    UIBezierPath* path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(imageViewX, -imageViewY)];
    [path addLineToPoint:CGPointMake(width , -imageViewY)];
    [path addLineToPoint:CGPointMake(width , -imageViewY + realH)];
    [path addLineToPoint:CGPointMake(imageViewX, -imageViewY + realH)];
    [path closePath];
    
    return path.CGPath;
}

#pragma mark - Styling

- (UIImage *)image {
    return self.imageView.image;
}

- (void)setImage:(UIImage *)image {
    self.imageView.image = image;
}

- (NSString *)text {
    return self.titleLbl.text;
}

- (BOOL)appliedStatus {
    return self.applied;
}

- (NSString *)sub1Text {
    return self.subttl1.text;
}

- (NSString *)sub2Text {
    return self.subttl2.text;
}

- (void)setText:(NSString *)text {
    self.titleLbl.text = text;
}

- (void)setAppliedStatus:(BOOL)applied{
    self.applied = applied;
}

- (void)setSub1Text:(NSString *)text {
    self.subttl1.text = text;
}

- (void)setSub2Text:(NSString *)text {
    self.subttl2.text = text;
}

#pragma mark - LabelSetup

- (void)setupLabelIfNeeded:(NSString *)text {
//    if (text) {
//        if (!self.gradient) {
//            CAGradientLayer *gradient = [CAGradientLayer layer];
//            gradient.frame = self.imageView.bounds;
//            gradient.colors = @[(id)[UIColor colorWithWhite:0 alpha:.85].CGColor,(id)[UIColor clearColor].CGColor];
//            gradient.startPoint = CGPointMake(.0, .5);
//            gradient.endPoint = CGPointMake(.5, .35);
//            [self.imageView.layer addSublayer:gradient];
//            self.gradient = gradient;
//
//        }
//        
//        if (!self.titleLbl) {
//            CGFloat realH = CGRectGetHeight(self.bounds);
//            CGFloat latoA = realH / 3;
//            CGFloat width = CGRectGetWidth(self.bounds);
//            
//            UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, latoA + 20, width - 20, 20)];
//            textLabel.backgroundColor = [UIColor redColor];
//            textLabel.layer.anchorPoint = CGPointMake(.5, .5);
//            textLabel.font = [UIFont boldSystemFontOfSize:13];
//            textLabel.numberOfLines = 1;
//            textLabel.textColor = [UIColor whiteColor];
//            //textLabel.shadowColor = [UIColor blackColor];
//            //textLabel.shadowOffset = CGSizeMake(1, 1);
//            //textLabel.transform = CGAffineTransformMakeRotation(-(asin(latoA/(sqrt(pow(width, 2) + pow(latoA, 2))))));
//            [self addSubview:textLabel];
//            self.titleLbl = textLabel;
//
//        }
//        if (!self.subttl1) {
//            CGFloat realH = CGRectGetHeight(self.bounds);
//            CGFloat latoA = realH / 3;
//            CGFloat width = CGRectGetWidth(self.bounds);
//            
//            UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, latoA + 20 + 15, width - 20, 20)];
//            textLabel.backgroundColor = [UIColor redColor];
//            textLabel.layer.anchorPoint = CGPointMake(.5, .5);
//            textLabel.font = [UIFont systemFontOfSize:12];
//            textLabel.numberOfLines = 1;
//            textLabel.textColor = [UIColor whiteColor];
//            //textLabel.shadowColor = [UIColor blackColor];
//            //textLabel.shadowOffset = CGSizeMake(1, 1);
//            //textLabel.transform = CGAffineTransformMakeRotation(-(asin(latoA/(sqrt(pow(width, 2) + pow(latoA, 2))))));
//            [self addSubview:textLabel];
//            self.subttl1 = textLabel;
//            
//        }
//        
//        if (!self.subttl2) {
//            CGFloat realH = CGRectGetHeight(self.bounds);
//            CGFloat latoA = realH / 3;
//            CGFloat width = CGRectGetWidth(self.bounds);
//            
//            UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, latoA + 20 + 15 + 15, width - 20, 20)];
//            textLabel.backgroundColor = [UIColor redColor];
//            textLabel.layer.anchorPoint = CGPointMake(.5, .5);
//            textLabel.font = [UIFont systemFontOfSize:12];
//            textLabel.numberOfLines = 1;
//            textLabel.textColor = [UIColor orangeColor];
//            //textLabel.shadowColor = [UIColor blackColor];
//            //textLabel.shadowOffset = CGSizeMake(1, 1);
//            //textLabel.transform = CGAffineTransformMakeRotation(-(asin(latoA/(sqrt(pow(width, 2) + pow(latoA, 2))))));
//            [self addSubview:textLabel];
//            self.subttl2 = textLabel;
//
//        }
//
//        
//    } else {
//        [self.gradient removeFromSuperlayer];
//        [self.titleLbl removeFromSuperview];
//        [self.subttl1 removeFromSuperview];
//        [self.subttl2 removeFromSuperview];
//    }
}

@end
