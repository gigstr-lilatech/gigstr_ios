//
//  ShiftData.h
//  Gigster
//
//  Created by Rakitha Perera on 5/24/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ShiftData_DROPDOWN @"DROPDOWN"
#define ShiftData_TITLE @"TITLE"
#define ShiftData_CHECKBOX @"CHECKBOX"
#define ShiftData_STRING @"STRING"
#define ShiftData_NUMBER @"NUMBER"

@interface ShiftData : NSObject

@property (nonatomic, strong) NSNumber* field_id;
@property (nonatomic, strong) NSString* field_name;
@property (nonatomic, strong) NSString* field_value;
@property (nonatomic, strong) NSString* field_type;
@property (nonatomic, strong) NSString* field_parent;
@property (nonatomic, strong) NSArray* field_data;
@property (nonatomic) BOOL required;
    
@property (nonatomic, strong) NSString* errorMessage;

-(id)initWithDic:(NSDictionary *)data;
- (id)toDictionary;
@end
