//
//  SenderCell.m
//  Gigster
//
//  Created by EFutures on 12/13/15.
//  Copyright © 2015 EFutures. All rights reserved.
//

#import "SenderCell.h"

static NSString *const TextCheckingResultAttributeName = @"TextCheckingResultAttributeName";

@implementation SenderCell

- (void)awakeFromNib {
    
    self.senderImage.layer.cornerRadius = self.senderImage.frame.size.width / 2;
    self.senderImage.clipsToBounds = YES;
    
    [[self.message layer] setCornerRadius:5.0f];
    [[self.message layer] setMasksToBounds:YES];
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)resizeHeight:(CGRect)rect{
    
//    [self.message setFrame:rect];
    
    NSMutableParagraphStyle *style =  [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    style.alignment = NSTextAlignmentLeft;
    style.firstLineHeadIndent = 10.0f;
    style.headIndent = 10.0f;
    style.tailIndent = -8.0f;
    
    NSMutableAttributedString *attrText = [[NSMutableAttributedString alloc] initWithString:self.message.text attributes:@{ NSParagraphStyleAttributeName : style}];
    
    NSDataDetector *dataDetector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingAllSystemTypes error:NULL];
    [dataDetector enumerateMatchesInString:self.message.text options:0 range:NSMakeRange(0, self.message.text.length) usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
        NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
        attributes[ZSWTappableLabelTappableRegionAttributeName] = @YES;
        attributes[ZSWTappableLabelHighlightedBackgroundAttributeName] = [UIColor clearColor];
        attributes[ZSWTappableLabelHighlightedForegroundAttributeName] = [UIColor blueColor];
        attributes[NSUnderlineStyleAttributeName] = @(NSUnderlineStyleSingle);
        attributes[NSForegroundColorAttributeName] = [UIColor colorWithRed:131.0f/255.0f green:213.0f/255.0f blue:210.0f/255.0f alpha:1.0];
        attributes[NSFontAttributeName] = [UIFont fontWithName:DEFAULT_FONT_NAME size:14.0];
        attributes[TextCheckingResultAttributeName] = result;
        [attrText addAttributes:attributes range:result.range];
    }];
    
    self.message.attributedText = attrText;
    
}


@end
