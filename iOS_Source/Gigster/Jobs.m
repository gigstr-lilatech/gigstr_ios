//
//  Jobs.m
//  Gigster
//
//  Created by EFutures on 11/16/15.
//  Copyright © 2015 EFutures. All rights reserved.
//

#import "Jobs.h"
#import "Api.h"

@implementation Jobs


+ (void) listAllGigsforUserId:(NSString *) userID token:(NSString *) token completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion{
    
    NSDictionary *params    = @{@"user_id": userID, @"token": token};
    NSDictionary *data      = @{@"url": @"gig/list_all", @"parameters": params};
    
    [Api post:data onCompletion:^(BOOL success, NSDictionary *response, NSError *error) {
        NSLog(@"success: %d", success);
        NSLog(@"respose: %@", response);
        NSLog(@"Error: %@", error);
        
        if (success) {
            
        }
        
       completion(success, response, error);
        
    }];
}

+ (void) listGig:(NSString *) userID token:(NSString *) token gigID:(NSString *) gigID completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion{
   
    NSDictionary *params    = @{@"user_id": userID, @"token": token, @"gig_id": gigID};
    NSDictionary *data      = @{@"url": @"gig/list_gig", @"parameters": params};
    
    [Api post:data onCompletion:^(BOOL success, NSDictionary *response, NSError *error) {
        NSLog(@"success: %d", success);
        NSLog(@"respose: %@", response);
        NSLog(@"Error: %@", error);
        
        
        if (success) {
            
            
        }
        
        completion(success, response, error);
        
    }];
    
}

+ (void) applyForGigWithUser:(NSString *) userID token:(NSString *) token gigID:(NSString *) gigID completion:(void (^)(BOOL success, NSDictionary *data, NSError *error))completion{
    
    NSDictionary *params    = @{@"user_id": userID, @"token": token, @"gig_id": gigID};
    NSDictionary *data      = @{@"url": @"gig/apply", @"parameters": params};
    
    [Api post:data onCompletion:^(BOOL success, NSDictionary *response, NSError *error) {
        NSLog(@"success: %d", success);
        NSLog(@"respose: %@", response);
        NSLog(@"Error: %@", error);
        
        if (success) {
            
            
        }
        
        completion(success, response, error);
        
    }];
}
@end
