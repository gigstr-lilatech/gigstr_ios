//
//  ExploreViewController.h
//  Gigster
//
//  Created by Rakitha Perera on 8/2/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MXSegmentedPagerController.h"


@interface ExploreViewController : MXSegmentedPagerController

@end
