//
//  CountrySelectViewController.m
//  Gigster
//
//  Created by Rakitha Perera on 12/22/16.
//  Copyright © 2016 EFutures. All rights reserved.
//

#import "CountrySelectViewController.h"
#import "SelectionCell.h"
#import "User.h"
#import <QuartzCore/QuartzCore.h>

@interface CountrySelectViewController () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *topTtitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *countries;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIButton *backgroundButton;
@property (strong, nonatomic) NSIndexPath *selectedIndex;

@end

@implementation CountrySelectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.countries = NSArray.new;
    // Do any additional setup after loading the view from its nib.
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.okButton setEnabled:NO];
    self.okButton.alpha = 0.5f;
    [self.contentView.layer setCornerRadius:10.0f];
    [self.contentView.layer setMasksToBounds:YES];
    [self.okButton.layer setCornerRadius:5.0f];
    [self.okButton.layer setMasksToBounds:YES];
    
    [self.topTtitleLabel setText:NSLocalizedString(@"Select Country", nil)];
    [self.okButton setTitle:NSLocalizedString(@"OK", nil) forState:UIControlStateNormal];
    
    self.backgroundButton.alpha = 0.0f;
    
    [User getCountryListOncompletion:^(BOOL success, NSDictionary *data, NSError *error) {
        self.countries = [data valueForKeyPath:@"countries"];
        int row = 0;
        for (NSDictionary *country in self.countries) {
            if ([[NSUserDefaults standardUserDefaults] valueForKey:kCountryCode]) {
                if ([country[@"code"] isEqualToString:[[NSUserDefaults standardUserDefaults] valueForKey:kCountryCode]]) {
                    self.selectedIndex = [NSIndexPath indexPathForRow:row inSection:0];
                    [self.okButton setEnabled:YES];
                    self.okButton.alpha = 1.0f;
                }
            }
            row ++;
        }
        [self.tableView reloadData];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated {

    [UIView animateWithDuration:0.3f animations:^{
        self.backgroundButton.alpha = 1.0f;
    }];
}
- (IBAction)okClicked:(id)sender {
    
    if (self.selectedIndex == nil) { return; }
    [UIView animateWithDuration:0.5f animations:^{
        [self.backgroundButton setAlpha:0.0f];
    } completion:^(BOOL finished) {
        [self.delegate CountrySelectViewController:self didSelecectCountry:self.countries[self.selectedIndex.row]];
        
        NSUserDefaults *defaults            = [NSUserDefaults standardUserDefaults];
        NSData *dataRepresentingSavedArray  = [defaults objectForKey:@"userObject"];
        if (dataRepresentingSavedArray != nil)
        {
            User *user =  (User *)[NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
            
            NSMutableDictionary *params    = [[NSMutableDictionary alloc]init];
            [params setObject:user.userid forKey:@"user_id"];
            [User setCountry:params Oncompletion:^(BOOL success, NSDictionary *data, NSError *error) {
                
            }];
        }
        
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.countries.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"SelectionCell";
    
    SelectionCell *cell = (SelectionCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SelectionCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        
    }
    NSDictionary *country = [self.countries objectAtIndex:indexPath.row];
    cell.selectionLabel.text = country[@"name"];
    
    if (self.selectedIndex != nil && self.selectedIndex.row == indexPath.row) {
        [cell.selectionImageView setImage:[UIImage imageNamed:@"selected_country"]];
    } else {
        [cell.selectionImageView setImage:nil];
    }
    
    

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.selectedIndex = indexPath;
    [self.okButton setEnabled:YES];
    self.okButton.alpha = 1.0f;
    [self.tableView reloadData];
}

@end
