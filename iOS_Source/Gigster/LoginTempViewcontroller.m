//
//  LoginTempViewcontroller.m
//  Gigster
//
//  Created by EFutures on 3/18/16.
//  Copyright © 2016 EFutures. All rights reserved.
//

#import "LoginTempViewcontroller.h"
#import "SignupViewController.h"
#import "SigninViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "User.h"

@interface LoginTempViewcontroller ()

@property(nonatomic,weak)IBOutlet UIButton *bgButton;
@property(nonatomic,weak)IBOutlet UIView *loginViewWhite;
@property(nonatomic,weak)IBOutlet UIButton *fbSignup;
@property(nonatomic,weak)IBOutlet UIButton *emailSignup;
@end

@implementation LoginTempViewcontroller

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.bgButton setAlpha:0.0f];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                                   selector:@selector(dismissPopup)
                                                       name:@"dismissPopup" object:nil];
    
    [[_fbSignup layer] setCornerRadius:5.0f];
    [[_fbSignup layer] setMasksToBounds:YES];
    
    [[_emailSignup layer] setCornerRadius:5.0f];
    [[_emailSignup layer] setMasksToBounds:YES];
    
    [[_loginViewWhite layer]setCornerRadius:5.0f];
    [[_loginViewWhite layer] setMasksToBounds:YES];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self fadeIn];
}


-(IBAction)fblogingClicked:(id)sender{
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    [login
     logInWithReadPermissions: @[@"public_profile",@"user_birthday",@"email"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error %@",error.description);
         } else if (result.isCancelled) {
             NSLog(@"%@",result);
             [self showAlert:NSLocalizedString(@"Could not connect to the server. Please try again", nil)];
         } else {
             NSLog(@"%@",result);
             NSLog(@"Logged in");
             
             [self getDetailsAndLogin];
         }
     }];
}

-(void)getDetailsAndLogin{
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:[NSDictionary dictionaryWithObject:@"cover,picture.type(large),id,name,first_name,last_name,gender,birthday,email,location,hometown,bio,photos" forKey:@"fields"]]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         if (!error) {
             
             NSLog(@"%@",result);
             NSString *userID = [[FBSDKAccessToken currentAccessToken] userID];
             NSString *userName = [result objectForKeyNotNull:@"name"];
             NSString *email =  [result objectForKeyNotNull:@"email"];
             NSString *userImageurl = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [[FBSDKAccessToken currentAccessToken] userID]];
             NSString *gender = [[result objectForKeyNotNull:@"gender"] uppercaseString];
             // NSString *DOB = [result objectForKeyNotNull:@"birthday"];
             
             
             NSString *pushToken;
             if ([[NSUserDefaults standardUserDefaults] stringForKey:@"pushDeviceToken"]) {
                 
                 pushToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"pushDeviceToken"];
             }else{
                 
                 pushToken = @"";
             }
             
             NSString *emails = (email) ? email : @"";
             
             //NSString *dobFB = (DOB) ? DOB : @"";
             
             NSString *dateString = [result objectForKeyNotNull:@"birthday"];
             NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
             [dateFormatter setDateFormat:@"MM/dd/yyyy"];
             NSDate *dateFromString = [dateFormatter dateFromString:dateString];
             dateFromString = [dateFormatter dateFromString:dateString];
             
             NSString *date;
             NSString *month;
             NSString *year;
             
             if (dateFromString) {
                 
                 NSDateFormatter *dateF1 = [[NSDateFormatter alloc] init];
                 [dateF1 setDateFormat:@"yyyy"];
                 year = [dateF1 stringFromDate:dateFromString];
                 
                 NSDateFormatter *dateF2 = [[NSDateFormatter alloc] init];
                 [dateF2 setDateFormat:@"MM"];
                 month = [dateF2 stringFromDate:dateFromString];
                 
                 NSDateFormatter *dateF3 = [[NSDateFormatter alloc] init];
                 [dateF3 setDateFormat:@"dd"];
                 date = [dateF3 stringFromDate:dateFromString];
             }
             else{
                 
                 NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                 [dateFormatter setDateFormat:@"MM/dd"];
                 NSDate *dateFromString = [[NSDate alloc] init];
                 dateFromString = [dateFormatter dateFromString:dateString];
                 
                 year = @"";
                 
                 NSDateFormatter *dateF2 = [[NSDateFormatter alloc] init];
                 [dateF2 setDateFormat:@"MM"];
                 month = [dateF2 stringFromDate:dateFromString];
                 
                 NSDateFormatter *dateF3 = [[NSDateFormatter alloc] init];
                 [dateF3 setDateFormat:@"dd"];
                 date = [dateF3 stringFromDate:dateFromString];
                 
             }
             
             NSString *dateX = (date) ? date : @"";
             NSString *monthX = (month) ? month : @"";
             NSString *yearX = (year) ? year : @"";
             
             NSString *genderFB;
             
             if ([gender isEqualToString:@"MALE"]) {
                 genderFB = @"M";
             }else if ([gender isEqualToString:@"FEMALE"]){
                 genderFB = @"F";
             }else{
                 genderFB = @"";
             }
             
             
             NSDictionary *params    = @{@"fbid": userID, @"name": userName, @"email":emails, @"day": dateX, @"month": monthX, @"year": yearX, @"gender": genderFB, @"accountType": [NSNumber numberWithInt:1], @"device_id":pushToken, @"device_type": @"2", @"image" :userImageurl};
             
             //             NSDictionary *params    = @{@"fbid": @"10156311001425287", @"name": @"Rasmus Solholm", @"email":@"solholmrasmus@gmail.com", @"day": dateX, @"month": monthX, @"year": yearX, @"gender": genderFB, @"accountType": [NSNumber numberWithInt:1], @"device_id":pushToken, @"device_type": @"2", @"image" :userImageURL};
             
             [User loginWithFacebookid:params completion:^(BOOL success, NSDictionary *data, NSError *error) {
                 
                 //                 [_activity stopAnimating];
                 //                 [_activity setHidden:YES];
                 
                 [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
                 
                 NSLog(@"success: %d", success);
                 NSLog(@"data: %@", data);
                 NSLog(@"Error: %@", error);
                 
                 if (success) {
                     
                     User *user         = [[User alloc] init];
                     user.userid        = [NSString stringWithFormat:@"%@",[data valueForKey:@"user_id"]];
                     user.token         = [NSString stringWithFormat:@"%@",[data valueForKey:@"token"]];
                     user.email         = email;
                     user.name          = userName;
                     user.userImageURL  = userImageurl;
                     //user.password    = password;
                     
                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                     [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:user] forKey:@"userObject"];
                     [defaults synchronize];
                    self.isFromApply = YES;
                     [[NSNotificationCenter defaultCenter] postNotificationName:@"dismissPopup" object:nil];
                     
                     [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                     
                 }
                 
                 else{
                     
                     if ([data valueForKey:@"message"]) {
                         NSString *status = [NSString stringWithFormat:@"%@",[data valueForKey:@"message"]];
                         [self showAlert:status];
                         
                     }else{
                         [self showAlert:NSLocalizedString(@"Could not connect to the server. Please try again", nil)];
                     }
                 }
                 
             }];
         }
         else{
             
             NSLog(@"%@",error.localizedDescription);
             [self showAlert:error.localizedDescription];
         }
     }];
    
}


- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:(BOOL)animated];
    // Call the super class implementation.
    // Usually calling super class implementation is done before self class implementation, but it's up to your application.
}

-(void)dismissPopup{
    
    if (self.isFromApply) {
        
        [self bgCLicked:nil];
        if (self.delegate && [self.delegate respondsToSelector:@selector(LoginTempViewcontroller:dismissedWithSuccess:)]) {
            [self.delegate LoginTempViewcontroller:self dismissedWithSuccess:YES];
        }
    }
    
}
-(void)fadeIn{
    //fade in
    [UIView animateWithDuration:0.5f animations:^{
        
        [self.bgButton setAlpha:1.0f];
        
    } completion:nil];
    
}


-(void)showAlert:(NSString *)message{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [UIView animateWithDuration:0.5f animations:^{
                                 [self.bgButton setAlpha:0.0f];
                             } completion:^(BOOL finished) {
                                 [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                             }];
                             
                         }];
    [alert addAction:ok];
    
}



-(IBAction)bgCLicked:(id)sender{
    //fade out
    [UIView animateWithDuration:0.5f animations:^{
        [self.bgButton setAlpha:0.0f];
    } completion:^(BOOL finished) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }];
}


#pragma mark - Navigation


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    if ([[segue identifier] isEqualToString:@"segueSignupApply"]){
        
        self.isFromApply = YES;
        
        UINavigationController *navcontroller = (UINavigationController *)[segue destinationViewController];
        SignupViewController *signupVC   = [navcontroller.viewControllers objectAtIndex:0];
        signupVC.isFromTabbar            = YES;
    }
    else if ([[segue identifier] isEqualToString:@"segueSigninApply"]){
        
        self.isFromApply = YES;
        
        UINavigationController *navcontroller = (UINavigationController *)[segue destinationViewController];
        SigninViewController *signinVC   = [navcontroller.viewControllers objectAtIndex:0];
        signinVC.isFromTabbar            = YES;
    }
}


@end
