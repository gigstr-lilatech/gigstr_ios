//
//  WebViewController.m
//  Gigster
//
//  Created by EFutures on 1/6/16.
//  Copyright © 2016 EFutures. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()<UIWebViewDelegate>


@property(nonatomic,weak)IBOutlet UIActivityIndicatorView *activity;



@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_activity setHidden:NO];
    [_activity startAnimating];
    [self.navigationController setNavigationBarHidden:NO];
    //NSString *urlString = [NSString stringWithFormat:@"http://dev-gigster.efserver.net/pages/terms"];
    if (self.isBoundsAdded) {
        [[self.webView scrollView] setContentInset:UIEdgeInsetsMake(5, 20, 5, 20)];
    }
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:self.webURL]];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [_activity setHidden:YES];
    [_activity stopAnimating];
    NSLog(@"Current URL = %@",webView.request.URL);

    
    //-- Add further custom actions if needed
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
