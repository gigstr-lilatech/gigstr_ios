//
//  ConsumerViewController.m
//  Gigster
//
//  Created by Rakitha Perera on 8/2/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import "ConsumerViewController.h"
#import "Explore.h"
#import "CarouselCell.h"
#import "ButtonCell.h"
#import "LinkCell.h"
#import <XCDYouTubeKit/XCDYouTubeKit.h>
#import "WebViewController.h"

@interface ConsumerViewController ()<CarouselCellDelegate,ButtonCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *exploreItems;

@end

@implementation ConsumerViewController

static NSString *carouselCellIdentifier = @"CarouselCell";
static NSString *buttonCellIdentifier = @"ButtonCell";
static NSString *linkCellIdentifier = @"LinkCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.exploreItems = NSArray.new;
    [self.tableView registerNib:[UINib nibWithNibName:carouselCellIdentifier bundle:nil] forCellReuseIdentifier:carouselCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:buttonCellIdentifier bundle:nil] forCellReuseIdentifier:buttonCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:linkCellIdentifier bundle:nil] forCellReuseIdentifier:linkCellIdentifier];
    self.tableView.contentInset = UIEdgeInsetsMake( 0, 0, 40, 0);
    
}
    
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self fetchData];
}
    


-(void)fetchData {
//    NSDictionary * data = [Explore getExploreTestData];
    
    [Explore listAllFeedForSection:@"client" completion:^(BOOL success, NSDictionary *data, NSError *error) {
        NSArray *exArr = data[@"feed"];
        NSMutableArray *tempArr = NSMutableArray.new;
        for (NSDictionary *dict in exArr) {
            Explore *explore = [[Explore alloc] initWithDic:dict];
            [tempArr addObject:explore];
        }
        self.exploreItems = [NSArray arrayWithArray:tempArr];
        [self.tableView reloadData];

    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.exploreItems.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Explore *explore = self.exploreItems[indexPath.row];
    if ([explore.component isEqualToString:EXPLORE_CAROUSEL]) {
        return 233.0f;
    } else {
        return 73.0f;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Explore *explore = self.exploreItems[indexPath.row];
    if ([explore.component isEqualToString:EXPLORE_CAROUSEL]) {
        CarouselCell *cell = [tableView dequeueReusableCellWithIdentifier:carouselCellIdentifier];
        [cell configCellWithData:explore];
        cell.delegate = self;
        return cell;
    } else if ([explore.component isEqualToString:EXPLORE_LINK]) {
        LinkCell *cell = [tableView dequeueReusableCellWithIdentifier:linkCellIdentifier];
         [cell configCellWithData:explore];
        return cell;
    } else {
        ButtonCell *cell = [tableView dequeueReusableCellWithIdentifier:buttonCellIdentifier];
        [cell configCellWithData:explore];
        cell.delegate = self;
        return cell;
    }
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    Explore *explore = self.exploreItems[indexPath.row];
    if ([explore.action isEqualToString:ACTION_WEB]) {
        WebViewController *detailController   = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];;
        detailController.webURL               = [NSURL URLWithString:explore.url];
        [self.navigationController pushViewController:detailController animated:YES];
    }  else if ([explore.action isEqualToString:ACTION_YOUTUBE]) {
        if([explore.url isKindOfClass:[NSNull class]] || [explore.url length] == 0)
        {
            [self showAlert:@"Invalid Link" withTitle:@"Error"];
            return;
        }
        XCDYouTubeVideoPlayerViewController *videoPlayerViewController = [[XCDYouTubeVideoPlayerViewController alloc] initWithVideoIdentifier:explore.url];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayerPlaybackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:videoPlayerViewController.moviePlayer];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MediaEnterFullScreen" object:nil userInfo:nil];
        
        [self presentMoviePlayerViewControllerAnimated:videoPlayerViewController];
    }
    
    
}

- (void)CarouselCell:(CarouselCell *)cell
     didSelecectItem:(CarouselItem *)carouselItem {
    
    if ([carouselItem.action isEqualToString:ACTION_WEB]) {
        WebViewController *detailController   = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];;
        detailController.webURL               = [NSURL URLWithString:carouselItem.url];
        [self.navigationController pushViewController:detailController animated:YES];
    } else if ([carouselItem.action isEqualToString:ACTION_YOUTUBE]) {
        if([carouselItem.url isKindOfClass:[NSNull class]] || [carouselItem.url length] == 0)
        {
            [self showAlert:@"Invalid Link" withTitle:@"Error"];
            return;
        }
        XCDYouTubeVideoPlayerViewController *videoPlayerViewController = [[XCDYouTubeVideoPlayerViewController alloc] initWithVideoIdentifier:carouselItem.url];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayerPlaybackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:videoPlayerViewController.moviePlayer];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MediaEnterFullScreen" object:nil userInfo:nil];
        
        [self presentMoviePlayerViewControllerAnimated:videoPlayerViewController];
    }
    
   }

- (void)ButtonCell:(ButtonCell *)cell didTapOnButton:(Explore *)item {
    if ([item.action isEqualToString:ACTION_WEB]) {
        WebViewController *detailController   = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];;
        detailController.webURL               = [NSURL URLWithString:item.url];
        [self.navigationController pushViewController:detailController animated:YES];
    } else if ([item.action isEqualToString:ACTION_YOUTUBE]) {
        if([item.url isKindOfClass:[NSNull class]] || [item.url length] == 0)
        {
            [self showAlert:@"Invalid Link" withTitle:@"Error"];
            return;
        }
        XCDYouTubeVideoPlayerViewController *videoPlayerViewController = [[XCDYouTubeVideoPlayerViewController alloc] initWithVideoIdentifier:item.url];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayerPlaybackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:videoPlayerViewController.moviePlayer];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MediaEnterFullScreen" object:nil userInfo:nil];
        
        [self presentMoviePlayerViewControllerAnimated:videoPlayerViewController];
    }
}

- (void) moviePlayerPlaybackDidFinish:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:notification.object];
    MPMovieFinishReason finishReason = [notification.userInfo[MPMoviePlayerPlaybackDidFinishReasonUserInfoKey] integerValue];
    if (finishReason == MPMovieFinishReasonPlaybackError)
    {
        NSError *error = notification.userInfo[XCDMoviePlayerPlaybackDidFinishErrorUserInfoKey];
        // Handle error
    }
    
}

-(void)showAlert:(NSString *)message withTitle:(NSString *)title{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    [alert addAction:ok];
    
}


@end
