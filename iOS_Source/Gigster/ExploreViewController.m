//
//  ExploreViewController.m
//  Gigster
//
//  Created by Rakitha Perera on 8/2/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import "ExploreViewController.h"
#import "ConsumerViewController.h"
#import "ClientViewController.h"

@interface ExploreViewController () <MXSegmentedPagerDelegate>
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *headerLogoImageView;

@end

@implementation ExploreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = UIColor.whiteColor;
    // Parallax Header
    self.segmentedPager.parallaxHeader.view = self.headerView;
    self.segmentedPager.parallaxHeader.mode = MXParallaxHeaderModeFill;
    self.segmentedPager.parallaxHeader.height = 150;
    self.segmentedPager.parallaxHeader.minimumHeight = 20;
    
    // Segmented Control customization
    self.segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;

    self.segmentedPager.segmentedControl.backgroundColor = [UIColor whiteColor];
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.alignment                = NSTextAlignmentLeft;
    self.segmentedPager.segmentedControl.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithRed: 93.0/255.0 green: 93.0/255.0 blue: 93.0/255.0 alpha: 1.0],
                                                                 NSFontAttributeName : [UIFont fontWithName:DEFAULT_FONT_NAME size:13.0],NSParagraphStyleAttributeName:paragraphStyle};
    self.segmentedPager.segmentedControl.selectedTitleTextAttributes = @{
                                                                         NSForegroundColorAttributeName : [UIColor colorWithRed: 93.0/255.0 green: 93.0/255.0 blue: 93.0/255.0 alpha: 1.0],
                                                                         NSFontAttributeName : [UIFont fontWithName:BOLD_FONT_NAME size:13.0],
                                                                         NSParagraphStyleAttributeName:paragraphStyle};
    self.segmentedPager.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    self.segmentedPager.segmentedControl.segmentWidthStyle = HMSegmentedControlSegmentWidthStyleFixed;
    self.segmentedPager.segmentedControl.selectionIndicatorColor = [UIColor colorWithRed: 129.0/255.0 green: 144.0/255.0 blue: 165.0/255.0 alpha: 1.0];
    self.segmentedPager.segmentedControl.selectionIndicatorHeight = 4;
    self.segmentedPager.segmentedControlEdgeInsets = UIEdgeInsetsMake(12, 12, 12, 12);
    self.segmentedPager.delegate = self;
    
    UIView *viewBarStatus = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 20)];
    viewBarStatus.backgroundColor = [UIColor colorWithRed: 131.0/255.0 green: 213.0/255.0 blue: 210.0/255.0 alpha: 1.0];
    
    [self.view addSubview:viewBarStatus];
    
    self.headerDescriptionLabel.text = @"Apply for gigs, get job coaching, browse our community events, find offers from our friends.";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(MXPageSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"Consumer"])
    {
        // Get reference to the destination view controller
        ConsumerViewController *vc = [segue destinationViewController];
        
//        // Pass any objects to the view controller here, like...
//        [vc setMyObjectHere:object];
    } else if ([[segue identifier] isEqualToString:@"Client"])
    {
        // Get reference to the destination view controller
        ClientViewController *vc = [segue destinationViewController];
        
        //        // Pass any objects to the view controller here, like...
        //        [vc setMyObjectHere:object];
    }
}

#pragma mark <MXSegmentedPagerControllerDataSource>

- (NSInteger)numberOfPagesInSegmentedPager:(MXSegmentedPager *)segmentedPager {
    return 2;
}

- (NSString *)segmentedPager:(MXSegmentedPager *)segmentedPager titleForSectionAtIndex:(NSInteger)index {
    return [@[@"OUR WORLD", @"COMPANY"] objectAtIndex:index];
}

- (NSString *)segmentedPager:(MXSegmentedPager *)segmentedPager segueIdentifierForPageAtIndex:(NSInteger)index {
    if (index == 0) {
        return @"Consumer";
    } else {
        return @"Client";
    }
    
}

- (void)segmentedPager:(MXSegmentedPager *)segmentedPager didSelectViewWithIndex:(NSInteger)index {

    if (index == 0) {
        [UIView animateWithDuration:0.5 animations:^{
            self.headerView.backgroundColor = [UIColor colorWithRed: 131.0/255.0 green: 213.0/255.0 blue: 210.0/255.0 alpha: 1.0];

        } completion:nil];
        
        [UIView transitionWithView:self.headerDescriptionLabel duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            self.headerDescriptionLabel.textColor = [UIColor whiteColor];
            self.headerDescriptionLabel.text = @"Apply for gigs, get job coaching, browse our community events, find offers from our friends.";

        } completion:^(BOOL finished) {
        }];
        [UIView transitionWithView:self.headerLogoImageView duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            [self.headerLogoImageView setImage:[UIImage imageNamed:@"logo_white"]];
            
        } completion:^(BOOL finished) {
        }];
        
    } else {
        [UIView animateWithDuration:0.5 animations:^{
            
            self.headerView.backgroundColor = [UIColor whiteColor];
            
        } completion:nil];
        [UIView transitionWithView:self.headerDescriptionLabel duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            self.headerDescriptionLabel.textColor = [UIColor colorWithRed: 93.0/255.0 green: 93.0/255.0 blue: 93.0/255.0 alpha: 1.0];
            self.headerDescriptionLabel.text = @"Gigstr is super easy and our on demand teams are the best way to staff your organisation.";
        } completion:^(BOOL finished) {
        }];
        [UIView transitionWithView:self.headerLogoImageView duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            [self.headerLogoImageView setImage:[UIImage imageNamed:@"logo_turquoise"]];
            
        } completion:^(BOOL finished) {
        }];
        
    }
}


@end
