//
//  NSDateFormatter+Addtions.h
//  Gigster
//
//  Created by Rakitha Perera on 7/6/16.
//  Copyright © 2016 EFutures. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateFormatter (Addtions)

+ (NSDateFormatter*) serverDateFormatter;

+ (NSDateFormatter*) dafaultDateFormatter;

@end
