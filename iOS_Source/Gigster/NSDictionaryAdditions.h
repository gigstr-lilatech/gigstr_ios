//
//  NSDictionaryAdditions.h
//  Lalinks
//
//  Category to return nil value instead of NSNull objects.
//
//  Created by Jayampathy Balasuriya on 4/28/14.
//  Copyright (c) 2014 Elephanti Pte. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (NSDictionaryAdditions)

// returns nil value incase of NSNull object
-(id)objectForKeyNotNull:(id)key;
-(void)addObject:(id)object forKey:(id)key;
-(void)addValue:(id)value forKey:(id)key;
@end
