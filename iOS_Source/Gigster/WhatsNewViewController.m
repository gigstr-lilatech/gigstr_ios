//
//  WhatsNewViewController.m
//  Gigster
//
//  Created by Rakitha Perera on 2/1/17.
//  Copyright © 2017 EFutures. All rights reserved.
//

#import "WhatsNewViewController.h"
#import "SocialViewController.h"
#import "WebViewController.h"


@interface WhatsNewViewController ()
    @property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation WhatsNewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [[_doneButton layer] setCornerRadius:5.0f];
    [[_doneButton layer] setMasksToBounds:YES];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:khaveShownWhatsNewKey];
    self.textView.scrollEnabled = NO;
    
    if (IS_IPHONE_5_OR_LESS) {
        [self.textView setFont:[UIFont fontWithName:HELVETICA_MEDIUM_FONT size:15.0f]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.textView.scrollEnabled = YES;
}
    
- (IBAction)doneClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)facebookClicked:(id)sender {
    UIStoryboard *stroryB = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SocialViewController *detailController   = [stroryB instantiateViewControllerWithIdentifier:@"SocialViewController"];
    detailController.webURL               = [NSURL URLWithString:FACEBOOK_URL];
    UINavigationController *navC = [[UINavigationController alloc] initWithRootViewController:detailController];
    [self presentViewController:navC animated:YES completion:nil];
    
}
- (IBAction)instagramClicked:(id)sender {
    UIStoryboard *stroryB = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SocialViewController *detailController   = [stroryB instantiateViewControllerWithIdentifier:@"SocialViewController"];
    detailController.webURL               = [NSURL URLWithString:INSTAGRAM_URL];
    UINavigationController *navC = [[UINavigationController alloc] initWithRootViewController:detailController];
    [self presentViewController:navC animated:YES completion:nil];
}


@end
